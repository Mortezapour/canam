#!/bin/bash

dircfg=$(pwd)

### make sure machine names are correctly defined
FRONTEND=${TRUE_HOST}
[[ $TRUE_HOST == underhill ]] && FRONTEND=ppp5
[[ $TRUE_HOST == robert   ]] && FRONTEND=ppp6
BACKEND=${TRUE_HOST}
[[ $TRUE_HOST == *ppp5 ]] && BACKEND=underhill
[[ $TRUE_HOST == *ppp6 ]] && BACKEND=robert

if [[ $FRONTEND == ppp* ]]; then
  sed -i -e "s/FRONTEND=.*/FRONTEND=$FRONTEND/" -e "s/BACKEND=.*/BACKEND=$BACKEND/" -e "s/SEQ_DEFAULT_MACHINE=.*/SEQ_DEFAULT_MACHINE=$FRONTEND/" $dircfg/resources.def
fi

cp canesm.cfg          ${dircfg%/GEM_cfgs*}/maestrofiles/config
cp CanAM_GEM.cfg       ${dircfg%/GEM_cfgs*}/maestrofiles/config/canesm/runcycle
cp CanDIAG.cfg         ${dircfg%/GEM_cfgs*}/maestrofiles/config/canesm/runcycle
cp outcfg.out          ${dircfg%/GEM_cfgs*}/maestrofiles/config/canesm/runcycle/CanAM_GEM
cp gem_settings.nml    ${dircfg%/GEM_cfgs*}/maestrofiles/config/canesm/runcycle/CanAM_GEM
cp physics_input_table ${dircfg%/GEM_cfgs*}/maestrofiles/config/canesm/runcycle/CanAM_GEM
cp resources.def       ${dircfg%/GEM_cfgs*}/maestrofiles/resources
if [[ -e canam_settings ]]; then
  cp canam_settings    ${dircfg%/GEM_cfgs*}/maestrofiles/config/canesm/runcycle/CanAM_GEM/canam_settings
fi

