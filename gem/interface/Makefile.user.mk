ifneq (,$(DEBUGMAKE))
$(info ## ====================================================================)
$(info ## File: Makefile.user.mk)
$(info ## )
endif

# VERBOSE = 1
OPTIL   = 4
OMP     = -openmp
MPI     = -mpi

LIBPHY = libcccmaphy

DEFINE         =     # User pre-processor definitions (e.g. DEFINE = -DLINUX)
INCLUDES       =     # User add on to the include PATH
DEFINE_DEBUG   = -DECCCGEM    #-DDEBUG_OUTPUT
INCLUDES_DEBUG =
RDE_OPTF_MODULE = -module $(BUILDMOD)

ifneq (,$(filter intel%,$(COMP_ARCH))$(filter PrgEnv-intel%,$(COMP_ARCH)))
#FFLAGS       = -I$(ROOT)/../gem/$(CONST_BUILDMOD)
#FFLAGS       = -g -traceback -ftrapuv -warn all -warn nointerfaces -std08
FFLAGS       =
FFLAGS_DEBUG = -g -C -warn all -init=snan -init=arrays
CFLAGS       =
# LFLAGS      =     # User's flags passed to the linker
endif

# COMP_RULES_FILE =
# PROFIL  = -prof   # set to -prof to enable the profiler (default = "")

CCCMAPHY_VERSION = 5.1

CCCMAPHY_SFX       = $(RDE_BUILDDIR_SFX)
CCCMAPHY_LIBS_0    = cccmaphy$(CCCMAPHY_SFX)
CCCMAPHY_LIBS      = $(CCCMAPHY_LIBS_0)

CCCMAPHY_MOD_FILES = $(foreach item,$(FORTRAN_MODULES_cccmaphy),$(item).[Mm][Oo][Dd])

.PHONY: debug cccmaphy cccmaphy_vfiles
CCCMAPHY_VFILES = cccmaphy_version.inc cccmaphy_version.h
cccmaphy_vfiles: $(CCCMAPHY_VFILES)
cccmaphy_version.inc:
	.rdemkversionfile "cccmaphy" "$(CCCMAPHY_VERSION)" . f
cccmaphy_version.h:
	.rdemkversionfile "cccmaphy" "$(CCCMAPHY_VERSION)" . c

debug:
	$(MAKE) OPTIL="0" DEBUG="$(FFLAGS_DEBUG)" MODEL_DEFINE1="$(DEFINE_DEBUG)" cccmaphy

cccmaphy: $(LIBDIR)/lib$(CCCMAPHY_LIBS).a

$(LIBDIR)/lib$(CCCMAPHY_LIBS).a: $(OBJECTS) $(CCCMAPHY_VFILES)
	rm -f $@ $@_$$$$; ar r $@_$$$$ $(OBJECTS); mv $@_$$$$ $@

ifneq (,$(DEBUGMAKE))
$(info ## ==== Makefile.user.mk [END] ========================================)
endif

#.SUFFIXES :

# Add this rule to handle CanAM5 files labelled a ".F"

.F.o:
	$(FC77) $<

