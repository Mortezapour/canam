#!/bin/ksh
#.*****************************************************************************
#.
#.     JOB NAME - send
#.
#.     STATUS - NON-ESSENTIAL
#.
#.     DESCRIPTION - This job sends to '${ARCHIVE_DESTINATION}' the cmcarc file
#.                   contructed by the previous tasks in this module
#.
#.*****************************************************************************

filelist=$(ls ${TASK_INPUT}/file_indir/*.ca 2>/dev/null) || true
filelist_unzip=$(ls ${TASK_INPUT}/file_indir/${ARCHIVE_DATE}/* 2>/dev/null) || true

# Check to see if ARCHIVE_DESTINATION is local and create local variables
local_ca=yes
machine_ca=''   ## Must default to null string for proper output
destination_ca=${ARCHIVE_DESTINATION#*:}
if [[ "${ARCHIVE_DESTINATION}" != /* ]]; then
    machine_ca="${ARCHIVE_DESTINATION%%:*}"
    if [ "${machine_ca}" != "${TRUE_HOST}" ]; then
        ## Si ${inputsfile} ne commence pas par '/' et ne contient pas de ':' alors
        local_ca=no
    fi
fi
# Make sure destination_ca directory exists
if [ "${local_ca}" = "yes" ]; then
    mkdir -p ${destination_ca}
else
    ssh ${machine_ca} mkdir -p ${destination_ca}
fi

# Check to see if ARCHIVE_DESTINATION_unzip is local and create local variables
local_unzip=yes
machine_unzip=''
destination_unzip=${ARCHIVE_DESTINATION_unzip#*:}
if [[ "${ARCHIVE_DESTINATION_unzip}" != /* ]]; then
    machine_unzip="${ARCHIVE_DESTINATION_unzip%%:*}"
    if [ "${machine_unzip}" != "${TRUE_HOST}" ]; then
        ## Si ${inputsfile} ne commence pas par '/' et ne contient pas de ':' alors
        local_unzip=no
    fi
fi
# Make sure destination_unzip directory exists (if files exist) and add date directory + extension if provided
if [ -n "${ARCHIVE_ext}" ] ; then
    destination_unzip=${destination_unzip}/${ARCHIVE_DATE}_${ARCHIVE_ext}
else
    destination_unzip=${destination_unzip}/${ARCHIVE_DATE}
fi
if [ -n "${filelist_unzip}" ]; then
    if [ "${local_unzip}" = "yes" ]; then
        mkdir -p ${destination_unzip}
    else
        ssh ${machine_unzip} mkdir -p ${destination_unzip}
    fi
fi

# Loop through all files in output of build_all
for infile in ${filelist} ${filelist_unzip}; do
    outfile=$(basename ${infile})
    # Determine ca/non-ca specific variables
    if [[ "${infile}" == *.ca ]]; then
        local=${local_ca}
        machine=${machine_ca}
        destination=${destination_ca}
        if [ -n "${ARCHIVE_ext}" ] ; then
            outfile=$(echo $(basename ${infile}) | sed "s/.ca/_${ARCHIVE_ext}.ca/")
        fi
    else
        local=${local_unzip}
        machine=${machine_unzip}
        # For tree output mode, create further directories
        if [ "${ARCHIVE_output_mode}" == "tree" ]; then
            tree_end=$(echo ${outfile} | sed 's|\.|\/|g')
            destination=${destination_unzip}/$(dirname ${tree_end})
            outfile=$(basename ${tree_end})
            if [ "${local}" = "yes" ]; then
                mkdir -p ${destination}
            else
                ssh ${machine_unzip} mkdir -p ${destination}
            fi
        else
            destination=${destination_unzip}
        fi
    fi

    # Link to input file is created in TASK_WORK
    [ -f "${outfile}" ] && rm -f ${outfile}
    ln -s $(${TASK_BIN}/true_path ${infile}) ${outfile}

    if [ "${local}" = "yes" ]; then
        outputfilename=${destination}/${outfile}
	if [ -f ${destination}/${outfile} -a "${ARCHIVE_overwrite_files}" == "no" ]; then
	    $SEQ_BIN/nodelogger -n $SEQ_NODE -s info -m  "The file ${outputfilename} already exists"
	    exit 1
	fi
	cp $(true_path ${outfile}) ${outputfilename}
    else
        outputfilename=${machine}:${destination}/${outfile}
	status=0
	ssh ${machine} test ! -f ${destination}/${outfile} || status=1
	if [ "${status}" -ne 0 -a "${ARCHIVE_overwrite_files}" == "no" ]; then
	    $SEQ_BIN/nodelogger -n $SEQ_NODE -s info -m  "The file ${outputfilename} already exists"
	    exit 1
	fi
	${TASK_BIN}/remote_copy ${outfile} ${outputfilename}
    fi

    if [ "${local}" = "yes" ]; then
        $SEQ_BIN/nodelogger -n $SEQ_NODE -s infox -m  "File ${outfile} sent to ${destination}"
    else
        $SEQ_BIN/nodelogger -n $SEQ_NODE -s infox -m  "File ${outfile} sent to ${machine}:${destination}"
    fi
    if [ "${ARCHIVE_check_corruption}" = 'md5sum' ]; then
	## Check corruption in the transfer
	status=256
	if [ "${local}" = "yes" ]; then
	    md5sum ${outfile} | ( cd ${destination}; md5sum -c - ) && status=0
	else
	    md5sum ${outfile} | ssh ${machine} "cd ${destination}; md5sum -c -" && status=0
	fi
	if [ "${status}" -eq 0 ]; then
	    $SEQ_BIN/nodelogger -n $SEQ_NODE -s infox -m  "Integrity of ${outputfilename} checked with 'md5sum' and OK"
	else
	    $SEQ_BIN/nodelogger -n $SEQ_NODE -s info -m  "The file ${outputfilename} has not been transferred correctly"
	    exit 1
	fi
    elif [ "${ARCHIVE_check_corruption}" = 'ls' ]; then
	taille_initiale=$(ls -lL ${outfile} | awk '{print $5}')
	if [ "${local}" = "yes" ]; then
	    taille_finale=$(ls -Ll ${destination}/${outfile} | awk '{print $5}')
            outputfilenamenodelogger=${destination}/${outfile}
        else
	    taille_finale=$(ssh ${machine} "ls -Ll ${destination}/${outfile} | awk '{print \$5}'")
            outputfilenamenodelogger=${machine}:${destination}/${outfile}
	fi

	if [ "${taille_initiale}" -eq "${taille_finale}" ]; then
	    $SEQ_BIN/nodelogger -n $SEQ_NODE -s infox -m  "File size of ${outputfilename} checked and OK"
	else
	    $SEQ_BIN/nodelogger -n $SEQ_NODE -s info -m  "The file ${outputfilename} has not been transferred correctly"
	    exit 1
	fi
    else
	$SEQ_BIN/nodelogger -n $SEQ_NODE -s infox -m  "Integrity of ${outputfilename} NOT checked (ARCHIVE_check_corruption=${ARCHIVE_check_corruption})"
    fi

done ## Fin de 'for infile in ${filelist}'
