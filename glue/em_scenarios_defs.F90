!>\file
!>\brief Define the em_scenarios section of the input model namelist
!!
!! @author
!
module em_scenarios_defs

   implicit none

   !--- Flag for aerosol run-time options to indicate how values are to be interpolated
   !--- it has the following meaning:
   !---         =   -1     continuously update from time series
   !---         = -YYYYMM  use the data from year "YYYY" (timeslice)
   !---         =    0     use climatology (non-transient)
   !--- em_aerosol_scn_mode is for all aerosols
   !--- em_aerosol_bc_scn_mode is for black carbon aerosols (optional otherwise default to em_aerosol_scn_mode)
   !--- em_aerosol_oc_scn_mode is for organic carbon aerosols (optional otherwise default to em_aerosol_scn_mode)
   !--- em_aerosol_sulf_scn_mode is for sulfate aerosols (optional otherwise default to em_aerosol_scn_mode)
   !---
   !--- The ind_* are used to indicate which arary element is assocociated with which aerosol emission.
   integer :: em_aerosol_scn_mode
   integer :: em_aerosol_bc_scn_mode, ind_bc
   integer :: em_aerosol_oc_scn_mode, ind_oc
   integer :: em_aerosol_sulf_scn_mode, ind_sulf

   !--- Flag to indicate how surface anthropogenic CO2 emissions are to be interpolated
   !--- it has the following meaning:
   !---         =   -1     continuously update surface anthropogenic CO2 emissions from data in the file "em"
   !---         =  YYYYMM  use a constant value from YYYY/MM in the file "em"
   !---         = -YYYYMM  use an annual cycle from year abs(YYYYMM)/100 in the file "em"
   integer :: em_co2_scn_mode

   !--- Flag to represent ozone run-time options
   !--- it has the following meaning:
   !---         =   -1     continuously update from time series
   !---         = -YYYYMM  use the data from year "YYYY" (timeslice)
   !---         =    0     use climatology (non-transient)
   integer :: conc_ozone_scn_mode

   !--- Flag to indicate how stratospheric aerosol values are to be interpolated
   !--- it has the following meaning:
   !---         =   -1     continuously update stratospheric aerosols form data in the file "vtau"
   !---         =  YYYYMM  use a constant value from YYYY/MM in the file "vtau"
   !---         = -YYYYMM  use an annual cycle from year abs(YYYYMM)/100 in the file "vtau"
   integer :: vtau_scn_mode
   ! The vtaufile_type parameter defines the "type" of the stratospheric aerosol forcing file.
   ! vtaufile_type = 1 is a dataset that only contains optical thickness at 550 nm
   !   - for example, sato_volcanic_4_tau_t63_1850_2300_monthly
   ! vtaufile_type = 2 is a dataset that contains optical properties in the CCCma radiation bands
   ! with data on pressure levels
   !  - for example, cmip_canesm_radiation_185001-201412_v1
   ! NOTE: The value of vtaufile_type and vtaufile in the basefile must be consistent or
   !       the run will crash
   integer :: vtaufile_type
   ! The "vtau_offset_year" offset year for the file with the stratospheric aerosols.
   ! vtau_offset_year  =   0 (default) read the forcing data for the same year as the model year
   !                   =  +N read the forcing data for the year ($year + N)
   !                   =  -N read the forcing data for the year ($year - N)
   integer :: vtau_offset_year

   !--- These correspond with parmsub parameters ghg_co2, ghg_ch4,...
   !--- which have the following meaning
   !--- ghg_xxx =   -1     continuously update emission from GHG time series
   !---         = YYYYMMDD use the emission for day "DD" of month "MM" of the year "YYYY"
   !---         =  -12     use first year in GHG time series for a repeating annual cycle
   !---         =    0     use a default value (the value set in radcons2)
   integer :: ghg_co2_scn_mode, ghg_ch4_scn_mode, ghg_n2o_scn_mode
   integer :: ghg_f11_scn_mode, ghg_f12_scn_mode, ghg_f113_scn_mode, ghg_f114_scn_mode
   !--- A year offset that is added to each GHG time array
   real*8 :: ghg_co2_scn_offset, ghg_ch4_scn_offset, ghg_n2o_scn_offset
   real*8 :: ghg_f11_scn_offset, ghg_f12_scn_offset, ghg_f113_scn_offset, ghg_f114_scn_offset

   integer, parameter :: num_em_aero = 3 ! Maximum number of emission scenarios that could be set.
   integer :: irefyra(num_em_aero)       ! Reference years to be used in aerosol emission
                                         ! scenario (if zero, AEROCOMM used).
                                         ! Determined from em_aerosol_*_scn_modes above
   integer :: irefyre                    ! Reference year to be used in surface co2 emissions scenario
   integer :: irefmne                    ! Reference month to be used in surface co2 emissions scenario.
   integer :: irefyro                    ! Reference year to be used in ozone concentration
                                         ! scenario (if zero, climatology used).
   integer :: irefyrv                    ! Reference year to be used in stratospheric aerosol scenario
   integer :: irefmnv                    ! Reference month to be used in stratospheric aerosol scenario
   integer :: ivtau                      ! "ivtau" based on vtaufile_type
   integer :: ioffyrv                    ! Set "ioffyrv"  based on vtau_offset_year

   logical :: interp_ghgts ! Determine if time interpolation of GHG data is required

   save

   !-------------------------------------------------------------
   !--- Set up namelists for CanAM run-time options
   !-------------------------------------------------------------
   namelist /em_scenario_cfgs/ em_aerosol_scn_mode, em_aerosol_bc_scn_mode, em_aerosol_oc_scn_mode, &
                               em_aerosol_sulf_scn_mode, em_co2_scn_mode, conc_ozone_scn_mode,  &
                               vtau_scn_mode, vtaufile_type, vtau_offset_year,       &
                               ghg_co2_scn_mode, ghg_ch4_scn_mode, ghg_n2o_scn_mode, &
                               ghg_f11_scn_mode, ghg_f12_scn_mode, ghg_f113_scn_mode,&
                               ghg_f114_scn_mode, &
                               ghg_co2_scn_offset, ghg_ch4_scn_offset, ghg_n2o_scn_offset, &
                               ghg_f11_scn_offset, ghg_f12_scn_offset, ghg_f113_scn_offset,&
                               ghg_f114_scn_offset

end module em_scenarios_defs
!> \file
