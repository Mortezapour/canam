!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

module solvar
  implicit none
  integer, parameter :: sv_nts_max=5414
  integer, parameter :: sv_subands=12
  real, dimension(sv_subands,sv_nts_max) :: solvts
  real*8, dimension(sv_nts_max) :: solvtimes
  real, save, dimension(sv_subands) :: solv=0.0
  integer, save :: sv_nts
  character(len=4), parameter, dimension(sv_subands) :: &
  SV_BNDNAMES = (/"B101","B102","B103","B104","B105","B106", &
                  "B107","B108","B109","BND2","BND3","BND4"/)
end module solvar
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
