!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine tdcal6 (dqdt,dtdt,dxdt,qg,xg,qfo,xfo,zg,dct,dsng, &
                   dsnfo,zfo,pg,pfo,dp,dz,dzo,qct,xct,mc, &
                   wrk3,tfe,mbg,lmag,lcl,lnb,iskip, &
                   ilev,ilevm,ilgg,msgg,msg,ntrac, &
                   cpres,rrl,grav,dsr,wc,sc,ru, &
                   auto,qlct,rhoa,pcpg,dt,xsrc,xsnk)
  !-----------------------------------------------------------------------
  !     * jun 25/2013 - m.lazare.    new version for gcm17:
  !     *                            - always do loop 950(not just for
  !     *                              isvchem as was previously the case).
  !     * apr 23/2010 - k.vonsalzen. previous version tdcal5 for gcm15i/16:
  !     *                            - addition of calculation of explicit
  !     *                              sources and sink arrays.
  !     * dec 18/2007 - m.lazare/    previous version tdcal4 for gcm15g/h:
  !     *               k.vonsalzen. add calculations for diagnostic fields
  !     *                            wds4rol,wds6rol.
  !     * nov 25/2006 - m.lazare.    previous version tdcal3 for gcm15f.
  !     *                            - selective promotion of variables to
  !     *                              real(8) :: to support 32-bit mode.
  !     * jun 15/2006 - m.lazare.    - "ZERO" data constant added and used
  !     *                              in call to intrinsics.
  !     * dec 13/2005 - k.vonsalzen. previous version for gcm15d/gcm15e:
  !     *                            - define shallow cloud amount (sclf)
  !     *                              and pass out to transc3.
  !     * may 14/2004 - m.lazare/    previous version tdcal for gcm15b/c.
  !     *               k.vonsalzen.
  !
  !     * calculates final tendencies from vertical fluxes and
  !     * cloud properties with an explicit method. updrafts and
  !     * downdrafts are determined for the same environmental
  !     * profiles.
  !-----------------------------------------------------------------------

  implicit none
  real, intent(in) :: cpres  !< Heat capacity of dry air \f$[J kg^{-1} K^{-1}]\f$
  real, intent(in) :: dt
  real, intent(in) :: grav  !< Gravity on Earth \f$[m s^{-2}]\f$
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilevm  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilgg    !< Total number of gathered atmospheric columns with shallow convection \f$[unitless]\f$
  integer, intent(in) :: msg
  integer, intent(in) :: msgg
  integer, intent(in) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  real, intent(in) :: rrl
  !
  !     * multi-level work fields (first set are promoted for consistency
  !     * with rest of shallow).
  !
  real*8, intent(in), dimension(ilgg,ilev) :: dz   !< Layer thickness for thermodynamic layers \f$[m]\f$
  real*8, intent(in), dimension(ilgg,ilev) :: zg !< Variable description\f$[units]\f$
  real*8, intent(in), dimension(ilgg,ilev) :: qct !< Variable description\f$[units]\f$
  real*8, intent(in), dimension(ilgg,ilev) :: wc !< Variable description\f$[units]\f$

  real, intent(in),  dimension(ilgg,ilev) :: mc !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilgg,ilev) :: wrk3 !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilgg,ilev) :: dqdt !< Variable description\f$[units]\f$
  real, intent(in),  dimension(ilgg,ilev) :: pfo !< Variable description\f$[units]\f$
  real, intent(in),  dimension(ilgg,ilev) :: dp !< Variable description\f$[units]\f$
  real, intent(in),  dimension(ilgg,ilev) :: rhoa !< Variable description\f$[units]\f$
  real, intent(in),  dimension(ilgg,ilev) :: qfo !< Variable description\f$[units]\f$
  real, intent(in),  dimension(ilgg,ilev) :: dzo   !< Variable description\f$[units]\f$
  real, intent(in),  dimension(ilgg,ilev) :: qg !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilgg,ilev) :: dtdt !< Variable description\f$[units]\f$
  real, intent(in),  dimension(ilgg,ilev) :: dct !< Variable description\f$[units]\f$
  real, intent(in),  dimension(ilgg,ilev) :: zfo !< Variable description\f$[units]\f$
  real, intent(in),  dimension(ilgg,ilev) :: auto !< Variable description\f$[units]\f$
  real, intent(in),  dimension(ilgg,ilev) :: dsng !< Variable description\f$[units]\f$
  real, intent(in),  dimension(ilgg,ilev) :: pg !< Variable description\f$[units]\f$
  real, intent(in),  dimension(ilgg,ilev) :: tfe !< Variable description\f$[units]\f$
  real, intent(in),  dimension(ilgg,ilev) :: dsnfo !< Variable description\f$[units]\f$
  real, intent(in),  dimension(ilgg,ilev) :: qlct !< Variable description\f$[units]\f$
  real, intent(in),  dimension(ilgg,ilev) :: dsr !< Variable description\f$[units]\f$


  real, intent(in),  dimension(ilgg,ilev,ntrac) :: xct !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilgg,ilev,ntrac) :: dxdt !< Variable description\f$[units]\f$
  real, intent(in),  dimension(ilgg,ilev,ntrac) :: xg !< Variable description\f$[units]\f$
  real, intent(in),  dimension(ilgg,ilev,ntrac) :: xfo !< Variable description\f$[units]\f$
  real, intent(in),  dimension(ilgg,ilev,ntrac) :: sc !< Variable description\f$[units]\f$
  real, intent(in),  dimension(ilgg,ilev,ntrac) :: ru !< Variable description\f$[units]\f$
  !
  !     * single-level work fields.
  !
  real, intent(inout), dimension(ilgg) :: mbg !< Variable description\f$[units]\f$
  real, intent(out),   dimension(ilgg) :: pcpg   !< Precipitation\f$[g/m**2]\f$

  real, intent(out), dimension(ilgg,ntrac) :: xsrc !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilgg,ntrac) :: xsnk !< Variable description\f$[units]\f$

  integer, intent(in), dimension(ilgg) :: lmag !< Variable description\f$[units]\f$
  integer, intent(in), dimension(ilgg) :: lcl !< Variable description\f$[units]\f$
  integer, intent(in), dimension(ilgg) :: lnb !< Variable description\f$[units]\f$
  integer, intent(in), dimension(ilgg) :: iskip !< Variable description\f$[units]\f$
  !
  real :: afrac
  real :: afs
  real :: aintp
  real :: aintt
  real :: arg1
  real :: arg2
  real :: arg3
  real :: arg4
  real :: arg5
  real :: dpldh
  real :: dpldl
  real :: dpst
  real :: dqdtmin
  real :: dtdtmin
  real :: dxdtmin
  integer :: il
  integer :: l
  integer :: n
  real :: zfh
  real :: zfl
  real :: zfm
  !  w  * acor     correction factor for cloud base mass flux, used in
  !                emergency feature in tdcal.
  !  w  * acorx    correction factor for cloud base mass flux, used in
  !                emergency feature in tdcal (for tracers).
  real, dimension(ilgg) :: acor !< Variable description\f$[units]\f$
  real, dimension(ilgg,ntrac) :: acorx !< Variable description\f$[units]\f$
  real, dimension(ilgg,ilev) :: dsrc !< Variable description\f$[units]\f$
  real, dimension(ilgg,ilev) :: wrk1 !< Variable description\f$[units]\f$
  real, dimension(ilgg,ilev) :: wrk2 !< Variable description\f$[units]\f$
  real, dimension(ilgg,ilev,ntrac) :: wrkx !< Variable description\f$[units]\f$
  !
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  real, parameter :: zero = 0.
  !
  !     * internal functions.
  !
  real :: zmean
  zmean(arg1,arg2,arg3,arg4,arg5) = (arg1 + .5 * arg2 * (arg4 - arg3)) &
                                    * (arg4 - arg3)/arg5
  !
  !-----------------------------------------------------------------------
  !
  !     * initializations.
  !
  do l = msg + 1,ilev
    do il = 1,ilgg
      wrk1(il,l) = 0.
      wrk2(il,l) = 0.
      wrk3(il,l) = 0.
      dqdt(il,l) = 0.
      dtdt(il,l) = 0.
      dsrc(il,l) = 0.
    end do
  end do ! loop 50
  wrkx(:,:,:) = 0.
  dxdt(:,:,:) = 0.
  !
  !     * effective fluxes of cloud properties at grid cell
  !     * interfaces below cloud base.
  !
  l = ilev
  do il = 1,ilgg
    if (l <= lmag(il) .and. l > lcl(il) .and. iskip(il) == 0) then
      dpst = mbg(il) * mc(il,l)
      wrk1(il,l) = wrk1(il,l) + qct(il,l) * dpst
      wrk2(il,l) = wrk2(il,l) + dct(il,l) * dpst
    end if
  end do ! loop 100
  do n = 1,ntrac
    do il = 1,ilgg
       if (l <= lmag(il) .and. l > lcl(il) .and. iskip(il) == 0) then
         dpst = mbg(il) * mc(il,l)
         wrkx(il,l,n) = wrkx(il,l,n) + xct(il,l,n) * dpst
       end if
    end do ! loop 120
  end do ! loop 125
  do l = ilevm,msgg + 2, - 1
    do il = 1,ilgg
      if (l <= lmag(il) .and. l > lcl(il) .and. iskip(il) == 0) then
        dpst = mbg(il) * mc(il,l)
        wrk1(il,l) = wrk1(il,l) + qct(il,l + 1) * dpst
        wrk2(il,l) = wrk2(il,l) + dct(il,l + 1) * dpst
      end if
    end do
  end do ! loop 140
  do n = 1,ntrac
    do l = ilevm,msgg + 2, - 1
      do il = 1,ilgg
        if (l <= lmag(il) .and. l > lcl(il) .and. iskip(il) == 0) then
          dpst = mbg(il) * mc(il,l)
          wrkx(il,l,n) = wrkx(il,l,n) + xct(il,l + 1,n) * dpst
        end if
      end do
    end do ! loop 160
  end do ! loop 165
  !
  !     * effective fluxes of cloud properties at grid cell
  !     * interfaces at and above cloud base. the vertical profiles
  !     * of in-cloud properties are identical to those assumed
  !     * in scprp.
  !
  do l = ilevm,msgg + 2, - 1
    do il = 1,ilgg
      if (l <= lcl(il) .and. l >= lnb(il) .and. iskip(il) == 0) then
        wrk1(il,l) = wrk1(il,l) + mbg(il) * mc(il,l) * qct(il,l)
        wrk2(il,l) = wrk2(il,l) + mbg(il) * mc(il,l) * dct(il,l)
      end if
    end do
  end do ! loop 200
  do n = 1,ntrac
    do l = ilevm,msgg + 2, - 1
      do il = 1,ilgg
        if (l <= lcl(il) .and. l >= lnb(il) .and. iskip(il) == 0) then
          wrkx(il,l,n) = wrkx(il,l,n) + mbg(il) * mc(il,l) * xct(il,l,n)
        end if
      end do
    end do ! loop 220
  end do ! loop 225
  !
  !     * intermediate tendencies from effective fluxes.
  !
  l = ilev
  do il = 1,ilgg
    if (l >= (lnb(il) - 1) .and. iskip(il) == 0) then
      dqdt(il,l) = dqdt(il,l) - wrk1(il,l) * grav * dsr(il,l)/dp(il,l)
      dtdt(il,l) = dtdt(il,l) - wrk2(il,l) * grav * dsr(il,l)/dp(il,l) &
                   /cpres
    end if
  end do ! loop 300
  do l = msgg + 2,ilevm
    do il = 1,ilgg
      if (l >= (lnb(il) - 1) .and. iskip(il) == 0) then
        dqdt(il,l) = dqdt(il,l) + ( wrk1(il,l + 1) - wrk1(il,l) ) &
                     * grav * dsr(il,l)/dp(il,l)
        dtdt(il,l) = dtdt(il,l) + ( wrk2(il,l + 1) - wrk2(il,l) ) &
                     * grav * dsr(il,l)/dp(il,l)/cpres
      end if
    end do
  end do ! loop 320
  do n = 1,ntrac
    l = ilev
    do il = 1,ilgg
      if (l >= (lnb(il) - 1) .and. iskip(il) == 0) then
        dxdt(il,l,n) = dxdt(il,l,n) - wrkx(il,l,n) * grav &
                       * dsr(il,l)/dp(il,l)
      end if
    end do ! loop 340
    !
    do l = msgg + 2,ilevm
      do il = 1,ilgg
        if (l >= (lnb(il) - 1) .and. iskip(il) == 0) then
          dxdt(il,l,n) = dxdt(il,l,n) &
                         + (wrkx(il,l + 1,n) - wrkx(il,l,n) ) * grav &
                         * dsr(il,l)/dp(il,l)
        end if
      end do
    end do ! loop 360
  end do ! loop 365
  !
  !     * lower vertical bounds for the calculation of effective fluxes.
  !
  do l = msgg + 2,ilev
    do il = 1,ilgg
      if (iskip(il) == 0 .and. l >= lnb(il) ) then
        wrk3(il,l) = pfo(il,l) - grav * mbg(il) * mc(il,l) * dsr(il,l) * dt
      end if
    end do
  end do ! loop 500
  !
  !     * effective fluxes of large-scale properties at grid cell interfac
  !     * the environmental profiles between the midpoints of the
  !     * grid cells are obtained from linear interpolation as function
  !     * of pressure. discretization according to bott (mwr, 1989).
  !
  do l = msg + 2,ilev
    do il = 1,ilgg
      wrk1(il,l) = 0.
      wrk2(il,l) = 0.
    end do
  end do ! loop 520
  do n = 1,ntrac
    do l = msg + 1,ilev
      do il = 1,ilgg
        wrkx(il,l,n) = 0.
      end do
    end do
  end do ! loop 530
  do l = ilev,msgg + 2, - 1
    do il = 1,ilgg
      if (l <= lmag(il) .and. l >= lnb(il) &
          .and. iskip(il) == 0) then
        zfh = pfo(il,l - 1)
        zfm = 100. * pg(il,l - 1)
        zfl = pfo(il,l)
        !
        if (qg(il,l - 1) > 0. .and. qfo(il,l) > 0. ) then
          dpldl = (qg (il,l - 1) - qfo(il,l))/(zfm - zfl)
          dpldh = (qfo(il,l - 1) - qg (il,l - 1))/(zfh - zfm)
          aintt = zmean(qfo(il,l),dpldl,zfl,zfm,dp(il,l - 1))
          if (wrk3(il,l) <= zfm) then
            aintp = aintt + zmean(qg(il,l - 1),dpldh,zfm,wrk3(il,l), &
                    dp(il,l - 1))
          else
            aintp = zmean(qfo(il,l),dpldl,zfl,wrk3(il,l), &
                    dp(il,l - 1))
          end if
          aintt = aintt + zmean(qg(il,l - 1),dpldh,zfm,zfh,dp(il,l - 1))
          afrac = qg(il,l - 1)/aintt
          wrk1(il,l) = aintp * afrac * dp(il,l - 1)/(dt * grav * dsr(il,l))
        end if
        !
        dpldl = (dsng (il,l - 1) - dsnfo(il,l))/(zfm - zfl)
        dpldh = (dsnfo(il,l - 1) - dsng (il,l - 1))/(zfh - zfm)
        aintt = zmean(dsnfo(il,l),dpldl,zfl,zfm,dp(il,l - 1))
        if (wrk3(il,l) <= zfm) then
          aintp = aintt + zmean(dsng(il,l - 1),dpldh,zfm,wrk3(il,l), &
                  dp(il,l - 1))
        else
          aintp = zmean(dsnfo(il,l),dpldl,zfl,wrk3(il,l), &
                  dp(il,l - 1))
        end if
        aintt = aintt + zmean(dsng(il,l - 1),dpldh,zfm,zfh,dp(il,l - 1))
        afrac = dsng(il,l - 1)/aintt
        wrk2(il,l) = aintp * afrac * dp(il,l - 1)/(dt * grav * dsr(il,l))
      end if
    end do
  end do ! loop 550
  do n = 1,ntrac
    do l = ilev,msgg + 2, - 1
      do il = 1,ilgg
        if (l <= lmag(il) .and. l >= lnb(il) &
            .and. xg(il,l - 1,n) > 0.0 .and. xfo(il,l,n) > 0.0 &
            .and. iskip(il) == 0) then
          zfh = pfo(il,l - 1)
          zfm = 100. * pg(il,l - 1)
          zfl = pfo(il,l)
          dpldl = (xg (il,l - 1,n) - xfo(il,l  ,n))/(zfm - zfl)
          dpldh = (xfo(il,l - 1,n) - xg (il,l - 1,n))/(zfh - zfm)
          aintt = zmean(xfo(il,l,n),dpldl,zfl,zfm, &
                  dp(il,l - 1))
          if (wrk3(il,l) <= zfm) then
            aintp = aintt + zmean(xg(il,l - 1,n),dpldh,zfm, &
                    wrk3(il,l),dp(il,l - 1))
          else
            aintp = zmean(xfo(il,l,n),dpldl,zfl,wrk3(il,l), &
                    dp(il,l - 1))
          end if
          aintt = aintt + zmean(xg(il,l - 1,n),dpldh,zfm,zfh, &
                  dp(il,l - 1))
          afrac = xg(il,l - 1,n)/aintt
          wrkx(il,l,n) = aintp * afrac * dp(il,l - 1) &
                         /(dt * grav * dsr(il,l))
        end if
      end do
    end do ! loop 570
  end do ! loop 575
  !
  !     * final tendencies from effective fluxes.
  !
  l = ilev
  do il = 1,ilgg
    if (l >= (lnb(il) - 1) .and. iskip(il) == 0) then
      dqdt(il,l) = dqdt(il,l) + wrk1(il,l) * grav * dsr(il,l)/dp(il,l)
      dtdt(il,l) = dtdt(il,l) + wrk2(il,l) * grav * dsr(il,l)/dp(il,l) &
                   /cpres
    end if
  end do ! loop 600
  do l = msgg + 2,ilevm
    do il = 1,ilgg
      if (l >= (lnb(il) - 1) .and. iskip(il) == 0) then
        dqdt(il,l) = dqdt(il,l) - ( wrk1(il,l + 1) - wrk1(il,l) ) * grav &
                     * dsr(il,l)/dp(il,l)
        dtdt(il,l) = dtdt(il,l) - ( wrk2(il,l + 1) - wrk2(il,l) ) * grav &
                     * dsr(il,l)/dp(il,l)/cpres
      end if
    end do
  end do ! loop 620
  do n = 1,ntrac
    l = ilev
    do il = 1,ilgg
      if (l >= (lnb(il) - 1) .and. iskip(il) == 0) then
        dxdt(il,l,n) = dxdt(il,l,n) + wrkx(il,l,n) * grav &
                       * dsr(il,l)/dp(il,l)
      end if
    end do ! loop 645
    !
    do l = msgg + 2,ilevm
      do il = 1,ilgg
        if (l >= (lnb(il) - 1) .and. iskip(il) == 0) then
          dxdt(il,l,n) = dxdt(il,l,n) &
                         - (wrkx(il,l + 1,n) - wrkx(il,l,n) ) * grav &
                         * dsr(il,l)/dp(il,l)
        end if
      end do
    end do ! loop 650
  end do ! loop 660
  !
  !     * volume cloud amount per grid cell.
  !
  do il = 1,ilgg
    if (iskip(il) == 0) then
      l = lcl(il)
      wrk3(il,l) = dz(il,l)/dzo(il,l) * mbg(il) &
                   * .5 * (mc(il,l)/(rhoa(il,l) * wc(il,l)) &
                   + mc(il,l + 1)/(rhoa(il,l + 1) * wc(il,l + 1)) )
    end if
  end do ! loop 680
  do l = msgg + 2,ilevm
    do il = 1,ilgg
      if (l < lcl(il) .and. l >= lnb(il) .and. iskip(il) == 0) then
        wrk3(il,l) = .5 * mbg(il) &
                     * (mc(il,l)/(rhoa(il,l) * wc(il,l)) &
                     + mc(il,l + 1)/(rhoa(il,l + 1) * wc(il,l + 1)) )
      end if
    end do
  end do ! loop 700
  do il = 1,ilgg
    if (iskip(il) == 0) then
      l = lnb(il) - 1
      wrk3(il,l) = dz(il,l)/dzo(il,l) * mbg(il) &
                   * .5 * mc(il,l + 1)/(rhoa(il,l + 1) * wc(il,l + 1))
    end if
  end do ! loop 720
  !
  !     * add sinks due to precipitation to moisture.
  !
  do l = msgg + 2,ilevm
    do il = 1,ilgg
      if (l <= lcl(il) .and. l >= (lnb(il) - 1) .and. iskip(il) == 0) then
        dqdt(il,l) = dqdt(il,l) - auto(il,l) * wrk3(il,l)
      end if
    end do
  end do ! loop 730
  !
  !     * add sources/sinks due to condensation/evaporation
  !     * and precipitation to temperature. it assumed that
  !     * the cloud is in a clear-sky environment. the sources/sinks
  !     * are diagnosed from large-scale liquid water continuity
  !     * under steady conditions.
  !
  do l = msg + 2,ilev
    do il = 1,ilgg
      wrk1(il,l) = 0.
    end do
  end do ! loop 731
  do l = msgg + 2,ilevm
    do il = 1,ilgg
      if (l <= lcl(il) .and. l >= lnb(il) .and. iskip(il) == 0) then
        dpst = mbg(il) * mc(il,l)
        wrk1(il,l) = qlct(il,l) * dpst
      end if
    end do
  end do ! loop 732
  do l = msgg + 2,ilevm
    do il = 1,ilgg
      if (l <= lcl(il) .and. l >= (lnb(il) - 1) .and. iskip(il) == 0) then
        dsrc(il,l) = rrl * (( wrk1(il,l) - wrk1(il,l + 1) ) * grav &
                     * dsr(il,l)/dp(il,l) &
                     + auto(il,l) * wrk3(il,l))
      end if
    end do
  end do ! loop 733
  do l = msgg + 2,ilevm
    do il = 1,ilgg
      if (l <= lcl(il) .and. l >= (lnb(il) - 1) .and. iskip(il) == 0) then
        dtdt(il,l) = dtdt(il,l) + dsrc(il,l)/cpres
      end if
    end do
  end do ! loop 735
  !
  !     * include tracer sinks/sources.
  !
  do n = 1,ntrac
    do l = msgg + 2,ilevm
      do il = 1,ilgg
        if (l <= lcl(il) .and. l >= (lnb(il) - 1) .and. iskip(il) == 0) then
          dxdt(il,l,n) = dxdt(il,l,n) + &
                         (sc(il,l,n) - ru(il,l,n)) * wrk3(il,l)
        end if
      end do
    end do ! loop 740
  end do ! loop 745
  !
  !     * safety feature. the cloud base mass flux is reduced if
  !     * excessive tendencies lead to negative values of the in-cloud
  !     * properties due to the explicity of the numerical scheme
  !     * used in the tendency calculations. the following limits should
  !     * not be invoked under regular operational conditions and caution
  !     * is adviced if this is however the case.
  !
  afs = 1. - 1.e-7
  do il = 1,ilgg
    acor (il) = 1.
  end do ! loop 800
  do l = msgg + 2,ilev
    do il = 1,ilgg
      dqdtmin = - afs * qg  (il,l)/dt
      if (dqdt(il,l) < dqdtmin .and. iskip(il) == 0) then
        acor(il) = max(min( - afs * qg  (il,l)/(dqdt(il,l) * dt), &
                   acor(il)),zero)
      end if
      dtdtmin = - afs * dsng(il,l)/(cpres * dt)
      if (dtdt(il,l) < dtdtmin .and. iskip(il) == 0) then
        acor(il) = max(min( - afs * dsng(il,l)/(cpres * dtdt(il,l) * dt), &
                   acor(il)),zero)
      end if
    end do
  end do ! loop 820
  acorx(:,:) = 1.
  do n = 1,ntrac
    do l = msgg + 2,ilev
      do il = 1,ilgg
        dxdtmin = - afs * xg(il,l,n)/dt
        if (dxdt(il,l,n) < dxdtmin .and. iskip(il) == 0 .and. dxdt(il,l,n) /= 0.0) then
          acorx(il,n) = max(min( - afs * xg(il,l,n) &
                        /(dxdt(il,l,n) * dt),acorx(il,n)),zero)
        end if
      end do
    end do ! loop 840
  end do ! loop 845
  do il = 1,ilgg
    mbg(il) = mbg(il) * acor(il)
  end do ! loop 860
  do l = msgg + 2,ilev
    do il = 1,ilgg
      if (acor(il) < 1. .and. iskip(il) == 0) then
        dqdt(il,l) = dqdt(il,l) * acor(il)
        dtdt(il,l) = dtdt(il,l) * acor(il)
      end if
    end do
  end do ! loop 880
  !
  !     * precipitation.
  !
  pcpg = 0.0
  do l = msgg + 2,ilevm
    do il = 1,ilgg
      if (l <= lcl(il) .and. l >= (lnb(il) - 1) .and. iskip(il) == 0) then
        pcpg(il) = pcpg(il) + &
                    dt * auto(il,l) * wrk3(il,l) * dp(il,l)/dsr(il,l) * &
                    acor(il)/grav/1.e+03
      end if
    end do
  end do ! loop 930
  !
  !     * corrected tracer tendencies.
  !
  do n = 1,ntrac
    do l = msgg + 2,ilev
      do il = 1,ilgg
        if (acorx(il,n) < 1. .and. iskip(il) == 0) then
          dxdt(il,l,n) = dxdt(il,l,n) * acorx(il,n)
        end if
      end do
    end do ! loop 890
  end do ! loop 895
  !
  !        * tracer sources and sinks (vertical integrals).
  !
  xsrc(1:ilgg,:) = 0.
  xsnk(1:ilgg,:) = 0.
  do n = 1,ntrac
    do l = msgg + 2,ilevm
      do il = 1,ilgg
        if (l <= lcl(il) .and. l >= (lnb(il) - 1) &
            .and. iskip(il) == 0) then
          xsrc(il,n) = xsrc(il,n) + wrk3(il,l) * sc(il,l,n) &
                       * acorx(il,n) * dp(il,l)/grav/dsr(il,l)
          xsnk(il,n) = xsnk(il,n) + wrk3(il,l) * ru(il,l,n) &
                       * acorx(il,n) * dp(il,l)/grav/dsr(il,l)
        end if
      end do
    end do
  end do ! loop 950
  !
  return
end subroutine tdcal6
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

