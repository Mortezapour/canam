!>\file
!>\brief Purpose: Main driver for gas-phase chemistry
!!
!! @author David Plummer

module chem3_main_mod

implicit none ; private

public :: chem3

contains

  subroutine chem3 (xrow, qrow, chltrox, chlsrox, chdgrox, wflxrox, &
                    acemphs, csadrow, sfemrow, vddprow, vdsfrow,    &
                    sfsaphs, clifrow, heatch, csalrol, ssadphs,     &
                    pressg, shj, shtj, throw, tfrow, cszchem,       &
                    delt, dtadv, iday, kount, il1, il2, msg, nbs)

  ! Main point of entry for gas-phase chemistry.
  ! Called by gas-phase chemistry interface gas_chem.F90 (which is called by physici.F90).
  ! Calls photolysis2.f, jayno.f, mamchmf2.f, chemheat.f.
  ! ---------------------------------------------------------------------------

  use msizes, only          : ilg, ilev, ntrac
  use tracers_info_mod, only: itox, itx1, itx2, itx3, ito3, itn1, itt6, itbga1
  use chem_photo_mod, only  : photolysis2, jayno, cldcorr
  use chemistry_mod, only   : chemheat
  use mamchmf2_mod, only    : mamchmf2
  use chem_params_mod, only : jpntjv, nsfcem, nvdep, nchdg, nchl, nwflx, jpnrks, &
                              vpavno, vpugc, vpbltz, vppi, vpcv, rc
  use phys_consts, only : grav, rgas, mma_g

  implicit none

  integer, intent(in) :: iday, kount, il1, il2, msg, nbs

  real, intent(in) :: delt, dtadv
  real, dimension(ilg), intent(in) :: cszchem, pressg
  real, dimension(ilg), intent(in) :: vdsfrow  !< surface (2-D) emission field (molecules/cm^2/sec)

  real, dimension(ilg,ilev),   intent(in) :: acemphs, clifrow, sfsaphs
  real, dimension(ilg,ilev),   intent(in) :: shj, ssadphs, throw
  real, dimension(ilg,ilev+1), intent(in) :: shtj, tfrow
  real, dimension(ilg,nsfcem), intent(in) :: sfemrow
  real, dimension(ilg,nvdep) , intent(in) :: vddprow
  real, dimension(ilg,nbs)   , intent(in) :: csalrol

  real, dimension(ilg,ilev), intent(inout)  :: qrow
  real, dimension(ilg,ilev,ntrac), intent(inout) :: xrow
  real(kind=rc), dimension(ilg,nwflx), intent(out) :: wflxrox
  real(kind=rc), dimension(ilg,nchl),  intent(out) :: chltrox, chlsrox
  real(kind=rc), dimension(ilg,ilev,nchdg), intent(out) :: chdgrox

  real, dimension(ilg,ilev), intent(out)  :: heatch
  real, dimension(ilg,ilev,2), intent(out) :: csadrow

  ! Locally defined workspace
  real, dimension(ilev) :: dtdz
  real(kind=rc), dimension(ilg,ilev) :: apm, airrow, delm, qtemp
  real(kind=rc), dimension(ilg,ilev) :: nocrow, ozcrow, zpos, oxlfr
  real, dimension(ilg,ilev) :: emisln
  real(kind=rc), dimension(ilg,7)           :: dep_wrk
  real(kind=rc), dimension(jpntjv,ilg,ilev) :: arj
  real(kind=rc), dimension(jpnrks,ilg,ilev) :: ark
  integer, dimension(ilg) :: indxtrp, indxlst

  integer :: i, k, n, idx, intwv, kst1, kst2
  real          :: prx1, prx2, pre90em, sf6bc, delzi
  real(kind=rc) :: qconst
  real(kind=rc) :: dtadv_rc
  real(kind=rc), parameter :: eps = 1.0e-35_rc

  ! For debugging
  integer*4 :: mynode
  common /mpinfo/ mynode

! -----------------------------------------------------------------------------
! Convert water vapour from specific humidity to VMR
  intwv = 1

  do k = 1, ilev
    do i = il1, il2
      xrow(i,k,itt6) = (qrow(i,k) / (1.0 - qrow(i,k)))/vpcv
    enddo
  enddo

  ! Calculate pressure (Pa) at mid-layers
  do k = 1, ilev
    do i = il1, il2
      apm(i,k) = real((shj(i,k) * pressg(i)), kind=rc)
    enddo
  enddo
  !          !- - - - - - - - PM(2)
  !          !....(ETC).......
  !          !----------------PI(JPLEV)
  !          !- - - - - - - - PM(JPLEV) ,Z(JPLEV)
  !          !________________PRESSG
  !          !////////////////
  !
  ! Calculate the number density (molecules / cm^3)
  do k = 1, ilev
    do i = il1, il2
      qtemp(i,k)  = real(throw(i,k), kind=rc)
      airrow(i,k) = apm(i,k) * 1.e-06_rc / (vpbltz * qtemp(i,k))
    enddo
  enddo

  ! CALCULATE THE LAYER INDEX OF THE TROPOPAUSE
  !
  kst1 = 0
  kst2 = 0
  do k = ilev, 1, -1
    if (shj(1,k) > 0.7)  kst1 = k
    if (shj(1,k) > 0.01) kst2 = k
  enddo

  ! Find the current thermal tropopause using the WMO definition
  do i = il1, il2
    indxtrp(i) = kst2
  enddo

  prx1 = -0.001 * vpugc / (mma_g*grav)
  do i = il1, il2
    ! Using dtdz as a workspace, calculate the temperature lapse rate dT/dZ
    ! in units of k/km.
    do k = kst2-2, kst1
      dtdz(k) = (throw(i,k) - throw(i,k-1)) / ( prx1*0.5*(throw(i,k)+throw(i,k-1)) &
                *log(shj(i,k)/shj(i,k-1)) )
    enddo

    do k = kst1, kst2, -1
      if (dtdz(k) >= -2.0 .and. 0.5*(dtdz(k-1) + dtdz(k-2)) >= -2.0 &
                            .and. indxtrp(i) == kst2) indxtrp(i)=k
    enddo
  enddo

  ! Set index for first model level below 100 hPa
  do i = il1, il2
    indxlst(i) = 1
  enddo

  do i = il1, il2
    do k = kst2, ilev
      if (apm(i,k) > 1.0e+04_rc .and. indxlst(i) == 1) indxlst(i)=k
    enddo
  enddo

  ! For the chemistry routines, calculate the thickness of each model layer
  ! in molecules/cm^2.
  prx1 = 1.0e-4 * vpavno / (grav*mma_g)
  do k = 1, ilev
    do i = il1, il2
      delm(i,k) = real((prx1*(shtj(i,k+1) - shtj(i,k))*pressg(i)), kind=rc)
    enddo
  enddo

  ! For dry deposition, calculate the inverse of the geometric thickness of
  ! the bottom layer (in units of m-1)
  ! And the deposition rate work array
  ! (See vdep1 subroutine for the composition of vddprow.)
  prx1 = rgas / grav
  do i = il1, il2
    delzi = 1.0 / ( prx1*throw(i,ilev)*log(1.0/shtj(i,ilev)) )
    dep_wrk(i,1) = real(exp(-0.5 * dtadv * vddprow(i,1) * delzi), kind=rc)
    dep_wrk(i,2) = real(exp(-0.5 * dtadv * vddprow(i,2) * delzi), kind=rc)
    dep_wrk(i,3) = real(exp(-0.5 * dtadv * vddprow(i,3) * delzi), kind=rc)
    dep_wrk(i,4) = real(exp(-0.5 * dtadv * vddprow(i,4) * delzi), kind=rc)
    dep_wrk(i,5) = real(exp(-0.5 * dtadv * vddprow(i,5) * delzi), kind=rc)
    dep_wrk(i,6) = real(exp(-0.5 * dtadv * vddprow(i,6) * delzi), kind=rc)
    dep_wrk(i,7) = real(exp(-0.5 * dtadv * vdsfrow(i) * delzi)  , kind=rc)
  enddo

  !
  ! COLUMN OZONE CALCULATION FOR J-VALUE CALCULATION.
  !
  ! Column-integrated in units of molecules/cm^2
  qconst = 1.0e-4_rc * real(vpavno / (grav*mma_g), kind=rc)

  do i = il1, il2
    ozcrow(i,1) = qconst * real(xrow(i,1,ito3), kind=rc) * apm(i,1)
  enddo
  do k = 2, ilev
    do i = il1, il2
      ozcrow(i,k) = ozcrow(i,k-1) + 0.5_rc * qconst * real((xrow(i,k-1,ito3)  &
                  + xrow(i,k,ito3)), kind=rc) * (apm(i,k) - apm(i,k-1))
    enddo
  enddo

  ! Calculate NO column at each point for use in J(NO) calculation
  ! (calculated in units of molecules/cm^2)
  do i = il1, il2
    nocrow(i,1) = qconst * real(xrow(i,1,itn1), kind=rc) * apm(i,1)
  enddo

  do k = 2, ilev
    do i = il1, il2
      nocrow(i,k) = nocrow(i,k-1) + 0.5_rc * qconst * real((xrow(i,k-1,itn1)   &
                  + xrow(i,k,itn1)), kind=rc) * (apm(i,k) - apm(i,k-1))
    enddo
  enddo

  ! Initialize instantaneous gas-phase chemistry diagnostic fields
  chdgrox = 0.0_rc
  chlsrox = 0.0_rc
  chltrox = 0.0_rc
  wflxrox = 0.0_rc
  !
  ! J-VALUE CALCULATION
  !
  ! Clear-sky rates interpolated from the look-up table
  call photolysis2 (arj, zpos, apm, ozcrow, cszchem, csalrol, il1, il2, nbs)

  ! In-line calculation of J(NO) (D.Plummer, fall 2004)
  call jayno (arj, apm, airrow, cszchem, ozcrow, nocrow, qconst, il1, il2)

  ! Save clear-sky photolysis rates for diagnostics, targetting O3 -> (O1D)
  ! and NO2 -> NO + O
  do k = 1, ilev
    do i = il1, il2
      chdgrox(i,k,5) = arj(2,i,k)
      chdgrox(i,k,6) = arj(5,i,k)
    enddo
  enddo

  ! Correction of clear-sky rates for clouds  BW: a work in progress sez Dave
  ! call cldcorr(arj, csalrol, cszchem, cmtx, tac, cg, omega, zpos,  &
  !              shj, shtj, vppi, lon, il1, il2, ilg, ilev, &
  !              lev, ilevcs, ilevcf, ilevc, nintsw)

  ! Save photolysis rates after cloud correction for diagnostics,
  ! targetting O3 -> (O1D) and NO2 -> NO + O
  do k = 1, ilev
    do i = il1, il2
      chdgrox(i,k,7) = arj(2,i,k)
      chdgrox(i,k,8) = arj(5,i,k)
    enddo
  enddo

  ! Copy the incoming stratospheric aerosol field (SSAD) onto the old BGA1 field
  ! for the chemistry solver and output
  do k = 1, ilev
    do i = il1, il2
      xrow(i,k,itbga1) = ssadphs(i,k)
    enddo
  enddo

  ! 1004: there are no lightning emissions yet, so set emisln to zero.
  emisln = 0.0

  dtadv_rc = real(dtadv, kind=rc)
  call mamchmf2 (arj, ark, chltrox, chlsrox, chdgrox, csadrow, &
                 wflxrox, emisln, acemphs, sfemrow, sfsaphs,   &
                 qtemp, airrow, delm, dep_wrk, oxlfr, indxtrp, &
                 indxlst, dtadv_rc, kount, il1, il2, msg, xrow)

  ! Chemical heating is calculated to correct the solar heating. If there is no
  ! chemical heating, use chemical potential (see switch CHHSWTCH in RADNEW6).
  call chemheat (heatch, xrow, ark, il1, il2, ntrac )

  ! Diagnostic tracers
  !
  ! Stratospheric Ox: set to Ox in the stratosphere; decays as Ox and
  ! deposits as ozone in the troposphere
  do i = il1, il2
    do k = 1, indxtrp(i)-1
      xrow(i,k,itx1) = xrow(i,k,itox)
    enddo
  enddo

  ! Half-step of dry deposition
   do i = il1, il2
     xrow(i,ilev,itx1) = xrow(i,ilev,itx1) * real(dep_wrk(i,1))
   enddo

  ! Full step of chemistry in the troposphere
  do i = il1, il2
    do k = indxtrp(i), ilev
      xrow(i,k,itx1) = xrow(i,k,itx1) * real(exp(-1.0_rc * dtadv_rc * oxlfr(i,k)))
    enddo
  enddo

  ! Half-step of dry deposition
  do i = il1, il2
    xrow(i,ilev,itx1) =xrow(i,ilev,itx1) * real(dep_wrk(i,1))
  enddo
  !
  ! The E90 tracer
  !
  ! This follows the UCI specification of aiming for a 100 ppb average
  ! concentration, assuming the same molecular weight as dry air.
  ! The calculation uses the GCM average surface pressure of 98480.197 Pa, a
  ! global average 100 ppb mixing ratio and a 90-day lifetime.
  ! A global average emission of 2077.56 Tg/year is required to maintain the
  ! steady-state burden. This is equal to a flux of 2.6847e+11 molecules/cm^2/s
  pre90em = 2.68470243e+11

  ! Half-step of emissions into the bottom two layers
  prx1 = 0.25 * pre90em * dtadv
  do k = ilev-1, ilev
    do i = il1, il2
      xrow(i,k,itx2) = xrow(i,k,itx2) + prx1 / real(delm(i,k))
    enddo
  enddo

  ! Full step of chemistry
  prx2 = exp( -1.0*dtadv/(90.0*86400.0) )
  do k = 1, ilev
    do i = il1, il2
      if(xrow(i,k,itx2) > 1.0e-16) xrow(i,k,itx2) = prx2*xrow(i,k,itx2)
    enddo
  enddo

  ! Half-step of emissions into the bottom two layers
  prx1 = 0.25 * pre90em * dtadv
  do k = ilev-1, ilev
    do i = il1, il2
      xrow(i,k,itx2) = xrow(i,k,itx2) + prx1 / real(delm(i,k))
    enddo
  enddo

  ! Modified age of air tracer: now using inert SF6-like tracer with a
  ! linearly increasing concentration at the surface.
  sf6bc = 2.0e-12 + 2.0e-12*float(kount)*delt/(365.0*86400.0)
  do k = ilev-6, ilev
    do i = il1, il2
      xrow(i,k,itx3) = sf6bc
    enddo
  enddo

  ! Return updated chemical water to model water
  if (intwv == 1) then
    do k = 1, ilev
      do i = il1, il2
        qrow(i,k) = vpcv * xrow(i,k,itt6) / (1.0 + vpcv*xrow(i,k,itt6))
      enddo
    enddo
  endif

  chdgrox = max(chdgrox, eps)
  chlsrox = max(chlsrox, eps)
  chltrox = max(chltrox, eps)
  wflxrox = max(wflxrox, eps)

  return
  end subroutine chem3

end module chem3_main_mod
