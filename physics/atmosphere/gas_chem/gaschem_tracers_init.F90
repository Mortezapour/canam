!>\file
!>\brief Initialization function for gas-phase chemistry species (tracers).
!!
!! @author Deji Akingunola, Barbara Winter

! This function is called by phys_tracers_init.F90 when switch_gas_chem=.true.

integer function gaschem_tracers_init (start_idx)

  use msizes,           only: ilev
  use tracers_info_mod, only: ntracgas, tracer_add, init_agcm_tracers, &
                              itox, itnx, itc9, itbx, itn5, ithx, itn7, itt5, &
                              ithb, itmm, itco, itha, itmh, ithd, itc7, itc4, &
                              itf1, itf2, itf3, itf4, itf5, itf6, itcb, itbf, &
                              itdb, itny, itcy, itcz, itbz, itta, ittb, itx1, &
                              itx2, itx3, ito1, itoo, ito3, itn1, itn2, itn3, &
                              itn4, itc1, itc6, itoc, itc3, itc5, itbr, itbo, &
                              itob, itbc, itbn, itt6, ith0, ithh, ith1, ith2, &
                              itm2, itc2, itbga1, itshno, itsh2o
  use chem_params_mod,  only: irnpos
  use agcm_types_mod,   only: phys_options
  use phys_consts,      only: mma_g

  implicit none

  integer, intent(in) :: start_idx
  integer :: ii, idx
  ! --------------------------------------------------------------------------

  gaschem_tracers_init = -1

  ! Total number of gas phase chemistry species
  ntracgas = 61

  idx = start_idx

  ! Set gas-phase tracer names and indices as they follow the default tracer
  ! indices. There are 16 default tracers; see tracers_info_mod.F90.
  idx = idx + 1; itox = idx; init_agcm_tracers(idx) % name = "  OX"   !   1/17
  idx = idx + 1; itnx = idx; init_agcm_tracers(idx) % name = "  NX"   !   2/18
  idx = idx + 1; itc9 = idx; init_agcm_tracers(idx) % name = "  C9"   !   3/19
  idx = idx + 1; itbx = idx; init_agcm_tracers(idx) % name = "  BX"   !   4/20
  idx = idx + 1; itn5 = idx; init_agcm_tracers(idx) % name = "  N5"   !   5/21
  idx = idx + 1; ithx = idx; init_agcm_tracers(idx) % name = "  HX"   !   6/22
  idx = idx + 1; itn7 = idx; init_agcm_tracers(idx) % name = "  N7"   !   7/23
  idx = idx + 1; itt5 = idx; init_agcm_tracers(idx) % name = "  T5"   !   8/24
  idx = idx + 1; ithb = idx; init_agcm_tracers(idx) % name = "  HB"   !   9/25
  idx = idx + 1; itmm = idx; init_agcm_tracers(idx) % name = "  MM"   !  10/26
  idx = idx + 1; itco = idx; init_agcm_tracers(idx) % name = "  CO"   !  11/27
  idx = idx + 1; itha = idx; init_agcm_tracers(idx) % name = "  HA"   !  12/28
  idx = idx + 1; itmh = idx; init_agcm_tracers(idx) % name = "  MH"   !  13/29
  idx = idx + 1; ithd = idx; init_agcm_tracers(idx) % name = "  HD"   !  14/30
  idx = idx + 1; itc7 = idx; init_agcm_tracers(idx) % name = "  C7"   !  15/31
  idx = idx + 1; itc4 = idx; init_agcm_tracers(idx) % name = "  C4"   !  16/32
  idx = idx + 1; itf1 = idx; init_agcm_tracers(idx) % name = "  F1"   !  17/33
  idx = idx + 1; itf2 = idx; init_agcm_tracers(idx) % name = "  F2"   !  18/34
  idx = idx + 1; itf3 = idx; init_agcm_tracers(idx) % name = "  F3"   !  19/35
  idx = idx + 1; itf4 = idx; init_agcm_tracers(idx) % name = "  F4"   !  20/36
  idx = idx + 1; itf5 = idx; init_agcm_tracers(idx) % name = "  F5"   !  21/37
  idx = idx + 1; itf6 = idx; init_agcm_tracers(idx) % name = "  F6"   !  22/38
  idx = idx + 1; itcb = idx; init_agcm_tracers(idx) % name = "  CB"   !  23/39
  idx = idx + 1; itbf = idx; init_agcm_tracers(idx) % name = "  BF"   !  24/40
  idx = idx + 1; itdb = idx; init_agcm_tracers(idx) % name = "  DB"   !  25/41
  idx = idx + 1; itny = idx; init_agcm_tracers(idx) % name = "  NY"   !  26/42
  idx = idx + 1; itcy = idx; init_agcm_tracers(idx) % name = "  CY"   !  27/43
  idx = idx + 1; itcz = idx; init_agcm_tracers(idx) % name = "  CZ"   !  28/44
  idx = idx + 1; itbz = idx; init_agcm_tracers(idx) % name = "  BZ"   !  29/45
  idx = idx + 1; itta = idx; init_agcm_tracers(idx) % name = "  TA"   !  30/46
  idx = idx + 1; ittb = idx; init_agcm_tracers(idx) % name = "  TB"   !  31/47
  idx = idx + 1; itx1 = idx; init_agcm_tracers(idx) % name = "  X1"   !  32/48
  idx = idx + 1; itx2 = idx; init_agcm_tracers(idx) % name = "  X2"   !  33/49
  idx = idx + 1; itx3 = idx; init_agcm_tracers(idx) % name = "  X3"   !  34/50
  idx = idx + 1; ito1 = idx; init_agcm_tracers(idx) % name = "  O1"   !  35/51
  idx = idx + 1; itoo = idx; init_agcm_tracers(idx) % name = "  OO"   !  36/52
  idx = idx + 1; ito3 = idx; init_agcm_tracers(idx) % name = "  O3"   !  37/53
  idx = idx + 1; itn1 = idx; init_agcm_tracers(idx) % name = "  N1"   !  38/54
  idx = idx + 1; itn2 = idx; init_agcm_tracers(idx) % name = "  N2"   !  39/55
  idx = idx + 1; itn3 = idx; init_agcm_tracers(idx) % name = "  N3"   !  40/56
  idx = idx + 1; itn4 = idx; init_agcm_tracers(idx) % name = "  N4"   !  41/57
  idx = idx + 1; itc1 = idx; init_agcm_tracers(idx) % name = "  C1"   !  42/58
  idx = idx + 1; itc6 = idx; init_agcm_tracers(idx) % name = "  C6"   !  43/59
  idx = idx + 1; itoc = idx; init_agcm_tracers(idx) % name = "  OC"   !  44/60
  idx = idx + 1; itc3 = idx; init_agcm_tracers(idx) % name = "  C3"   !  45/61
  idx = idx + 1; itc5 = idx; init_agcm_tracers(idx) % name = "  C5"   !  46/62
  idx = idx + 1; itbr = idx; init_agcm_tracers(idx) % name = "  BR"   !  47/63
  idx = idx + 1; itbo = idx; init_agcm_tracers(idx) % name = "  BO"   !  48/64
  idx = idx + 1; itob = idx; init_agcm_tracers(idx) % name = "  OB"   !  49/65
  idx = idx + 1; itbc = idx; init_agcm_tracers(idx) % name = "  BC"   !  50/66
  idx = idx + 1; itbn = idx; init_agcm_tracers(idx) % name = "  BN"   !  51/67
  idx = idx + 1; itt6 = idx; init_agcm_tracers(idx) % name = "  T6"   !  52/68
  idx = idx + 1; ith0 = idx; init_agcm_tracers(idx) % name = "  H0"   !  53/69
  idx = idx + 1; ithh = idx; init_agcm_tracers(idx) % name = "  HH"   !  54/70
  idx = idx + 1; ith1 = idx; init_agcm_tracers(idx) % name = "  H1"   !  55/71
  idx = idx + 1; ith2 = idx; init_agcm_tracers(idx) % name = "  H2"   !  56/72
  idx = idx + 1; itm2 = idx; init_agcm_tracers(idx) % name = "  M2"   !  57/73
  idx = idx + 1; itc2 = idx; init_agcm_tracers(idx) % name = "  C2"   !  58/74
  idx = idx + 1; itbga1 = idx; init_agcm_tracers(idx) % name = "BGA1" !  59/75
  idx = idx + 1; itshno = idx; init_agcm_tracers(idx) % name = "SHNO" !  60/76
  idx = idx + 1; itsh2o = idx; init_agcm_tracers(idx) % name = "SH2O" !  61/77
  !
  ! * Set default attributes of gas-phase species. Overrides for individual
  ! * species are set in a subsequent loop.
  !
  do ii = 1, ntracgas
    idx = start_idx + ii
    init_agcm_tracers(idx) % lng_name = "GAS PHASE SPECIES"//init_agcm_tracers(idx) % name
    init_agcm_tracers(idx) % adv      = 1  ! Only for advected tracers
    init_agcm_tracers(idx) % phs      = 1  ! Only for advected tracers
    init_agcm_tracers(idx) % srf      = 0
    init_agcm_tracers(idx) % ic       = 1  ! Only for advected tracers
    init_agcm_tracers(idx) % cch      = 0
    init_agcm_tracers(idx) % dry      = 0
    init_agcm_tracers(idx) % wet      = 0
    init_agcm_tracers(idx) % xref     = 0.0   ! Reference concentration
    init_agcm_tracers(idx) % xpow     = 1.0
    init_agcm_tracers(idx) % bndc     = 0.0   ! Background concentration

    ! When running with chemistry, set molecular weights for advected gas
    ! tracers. The MW values are used to convert between mass and volume mixing
    ! ratios. Aerosol quantities are generally given as mass mixing ratios, but
    ! this does not make sense for gas tracers. Dave Plummer therefore worked
    ! around it by setting all advected gas-phase species to have the same
    ! molecular weight as dry air (28.9647 g/mol), so that mass and volume
    ! mixing ratios are numerically identical.
    ! This value now comes in from phys_consts.
    init_agcm_tracers(idx) % mw = mma_g  ! Only for advected tracers
  enddo

  ! Set overrides for gas-phase species
  do ii = 1, ntracgas
    idx = start_idx + ii

    select case (init_agcm_tracers(idx)%name)
    case ("  OX")
      init_agcm_tracers(idx)%srf  = -2
      if (phys_options%mam) then
        init_agcm_tracers(idx)%bndc = 1.0000E-02  ! MAM setting
      else
        init_agcm_tracers(idx)%bndc = 1.0000E-06  ! low lid setting
      endif
    case ("  NX")
      init_agcm_tracers(idx)%srf  = -2
      if (phys_options%mam) then
        init_agcm_tracers(idx)%bndc = 1.0000E-06  ! MAM setting
      else
        init_agcm_tracers(idx)%bndc = 1.0000E-08  ! low lid setting
      endif
    case ("  T5")
      init_agcm_tracers(idx)%srf  = -1
      init_agcm_tracers(idx)%bndc = 3.1585E-07  ! N2O - year 2000
    case ("  MM")
      init_agcm_tracers(idx)%srf  = -1
      init_agcm_tracers(idx)%bndc = 1.7510E-06  ! CH4 - year 2000
    case ("  HD")
      init_agcm_tracers(idx)%srf  = -1
      init_agcm_tracers(idx)%bndc = 5.0000E-07  ! H2
    case ("  F1")
      init_agcm_tracers(idx)%srf  = -1
      init_agcm_tracers(idx)%bndc = 2.6345E-10  ! CFC-11 - year 2000
    case ("  F2")
      init_agcm_tracers(idx)%srf  = -1
      init_agcm_tracers(idx)%bndc = 6.6082E-10  ! CFC-12 - year 2000
    case ("  F3")
      init_agcm_tracers(idx)%srf  = -1
      init_agcm_tracers(idx)%bndc = 9.9370E-11  ! CCl4 - year 2000
    case ("  F4")
      init_agcm_tracers(idx)%srf  = -1
      init_agcm_tracers(idx)%bndc = 5.6148E-11  ! CH3CCl3 - year 2000
    case ("  F5")
      init_agcm_tracers(idx)%srf  = -1
      init_agcm_tracers(idx)%bndc = 2.1337E-10  ! HCFC-22 - year 2000
    case ("  F6")
      init_agcm_tracers(idx)%srf  = -1
      init_agcm_tracers(idx)%bndc = 5.5001E-10  ! CH3Cl - year 2000
    case ("  CB")
      init_agcm_tracers(idx)%srf  = -1
      init_agcm_tracers(idx)%bndc = 1.6653E-11  ! CH3Br - year 2000
    case ("  BF")
      init_agcm_tracers(idx)%srf  = -3
      init_agcm_tracers(idx)%bndc = 1.2000E-12  ! CHBr3
    case ("  DB")
      init_agcm_tracers(idx)%srf  = -3
      init_agcm_tracers(idx)%bndc = 1.2000E-12  ! CH2Br2
    case("  TA")
      init_agcm_tracers(idx)%xref = 1.50E-08    ! tropospheric NOx
    case("  TB")
      init_agcm_tracers(idx)%xref = 5.00E-09    ! tropospheric HNO3
    ! The non-advected tracers:
    case ("  O1","  OO","  O3","  N1","  N2","  N3","  N4","  C1","  C6", &
          "  OC","  C3","  C5","  BR","  BO","  OB","  BC","  BN","  T6", &
          "  H0","  HH","  H1","  H2","  M2","  C2","BGA1","SHNO","SH2O")
      init_agcm_tracers(idx) % adv = 0
      init_agcm_tracers(idx) % phs = 0
      init_agcm_tracers(idx) % ic  = 0
      init_agcm_tracers(idx) % mw  = 0.0
    end select
  enddo

! Set positional indices for species that are rained out (wet deposition).
  irnpos(1) = ithx    !HOx - to get H2O2
  irnpos(2) = itn7    !HNO4
  irnpos(3) = itha    !HCHO
  irnpos(4) = itmh    !CH3OOH
  irnpos(5) = itn5    !HNO3
  irnpos(6) = ithb    !HBr
  irnpos(7) = itc7    !HCl

  gaschem_tracers_init = start_idx + ntracgas

  return
end function gaschem_tracers_init
