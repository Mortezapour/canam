!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine hines_wavnum2(m_alpha,sigma_alpha,sigsqh_alpha,sigma_t, &
                         ak_alpha,vel_u,vel_v,ubot,vbot,visc_mol, &
                         density,densb,bvfreq,bvfb, &
                         rms_wind,v_alpha,&
                         dragil,drag,m_min,kstar,slope, &
                         f1,f2,f3,naz,levbot,levtop,il1,il2, &
                         nlons,nlevs,nazmth,n_over_m,sigfac)
  !
  !  This routine calculates the cutoff vertical wavenumber and velocity
  !  variances on a longitude by altitude grid needed for the Hines' Doppler
  !  spread gravity wave drag parameterization scheme.
  !
  !  aug. 10/95 - c. mclandress
  !
  !  modifications
  !  -------------
  !  aug. 8/99 - c. mclandress (nonzero m_min for all other values of slope)
  !  feb. 2/96 - c. mclandress (added: minimum cutoff wavenumber m_min
  !                             12 and 16 azimuths; inclusion of introwaves
  !                             by ubot and vbot; diffusion and heating
  !                             calculated at half level; logical :: flags
  !                             new print routine)
  !
  !  output arguments:
  !  ------------------
  !
  !     * m_alpha      = cutoff wavenumber at each azimuth (1/m).
  !     * sigma_alpha  = total rms wind in each azimuth (m/s).
  !     * sigsqh_alpha = portion of wind variance from waves having wave
  !     *                normals in the alpha azimuth (m/s).
  !     * sigma_t      = total rms horizontal wind (m/s).
  !     * ak_alpha     = spectral amplitude factor at each azimuth
  !     *                (i.e.,{ajkj}) in m^4/s^2.
  !     * dragil       = logical :: flag indicating longitudes and levels where
  !     *                calculations to be performed.
  !
  !  input arguements:
  !  -----------------
  !
  !     * vel_u    = background zonal wind component (m/s).
  !     * vel_v    = background meridional wind component (m/s).
  !     * ubot     = background zonal wind component at bottom level.
  !     * vbot     = background meridional wind component at bottom level.
  !     * visc_mol = molecular viscosity (m^2/s)
  !     * density  = background density (kg/m^3).
  !     * densb    = background density at model bottom (kg/m^3).
  !     * bvfreq   = background brunt vassala frequency (radians/sec).
  !     * bvfb     = background brunt vassala frequency at model bottom.
  !     * rms_wind = root mean square gravity wave wind at lowest level (m/s).
  !     * drag     = logical :: flag indicating longitudes where calculations
  !     *            to be performed.
  !     * kstar    = typical gravity wave horizontal wavenumber (1/m).
  !     * m_min    = minimum allowable cutoff vertical wavenumber.
  !     * slope    = slope of incident vertical wavenumber spectrum.
  !     * F1,F2,F3 = Hines's fudge factors.
  !     * naz      = number of horizontal azimuths used (4, 8, 12 or 16).
  !     * levbot   = index of lowest vertical level.
  !     * levtop   = index of highest vertical level.
  !     * il1      = first longitudinal index to use (il1 >= 1).
  !     * il2      = last longitudinal index to use (il1 <= il2 <= nlons).
  !     * nlons    = number of longitudes.
  !     * nlevs    = number of vertical levels.
  !     * nazmth   = azimuthal array dimension (nazmth >= naz).
  !
  !  input work arrays:
  !  -----------------
  !
  !     * v_alpha    = wind component at each azimuth (m/s).
  !
  !  Local work arrays:
  !  -----------------
  !
  !     * do_alpha   = logical :: flag indicating azimuths and longitudes
  !     *              where calculations to be performed.
  !     * I_ALPHA    = Hines' integral at a single level.
  !     * mmin_alpha = minimum value of cutoff wavenumber.
  !
  implicit none
  integer, intent(in)  :: naz !< Variable description\f$[units]\f$
  integer, intent(in)  :: levbot   !< Variable description\f$[units]\f$
  integer, intent(in)  :: levtop   !< Variable description\f$[units]\f$
  integer, intent(in)  :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in)  :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in)  :: nlons !< Variable description\f$[units]\f$
  integer, intent(in)  :: nlevs !< Variable description\f$[units]\f$
  integer, intent(in)  :: nazmth !< Variable description\f$[units]\f$
  ! ccc      logical :: drag(nlons), do_alpha(nlons,nazmth), dragil(nlons,nlevs)
  integer, intent(in), dimension(nlons) :: drag !< Variable description\f$[units]\f$
  integer, intent(out), dimension(nlons,nlevs) :: dragil !< Variable description\f$[units]\f$
  real, intent(in)    :: slope !< Variable description\f$[units]\f$
  real, intent(in)    :: m_min !< Variable description\f$[units]\f$
  real, intent(in)    :: kstar !< Variable description\f$[units]\f$
  real, intent(in)    :: f1 !< Variable description\f$[units]\f$
  real, intent(in)    :: f2 !< Variable description\f$[units]\f$
  real, intent(in)    :: f3 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons,nlevs,nazmth) :: m_alpha !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons,nlevs,nazmth) :: sigma_alpha !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons,nlevs,nazmth) :: sigsqh_alpha !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons,nlevs) :: sigma_t !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons,nazmth) :: ak_alpha !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs) :: visc_mol !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs) :: vel_u !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs) :: vel_v !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons) :: ubot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons) :: vbot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs) :: density !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons) :: densb !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs) :: bvfreq !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons) :: bvfb !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons) :: rms_wind !< Variable description\f$[units]\f$
  real, intent(out), dimension(nlons,nazmth) :: v_alpha !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons) :: n_over_m
  real, intent(inout), dimension(nlons) :: sigfac
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  real, parameter :: visc_min = 1.e-10
  !==================================================================

  !
  ! internal variables.
  ! -------------------
  !
  ! ccc      integer  :: ibig
  ! ccc      parameter  ( ibig = 1000)
  ! ccc      real  :: n_over_m(ibig), sigfac(ibig)
  integer  :: i
  integer  :: l
  integer  :: n
  integer  :: lstart
  integer  :: lincr
  integer  :: lbelow
  integer  :: icount
  real    :: m_sub_m_turb
  real    :: m_sub_m_mol
  real    :: m_trial
  real    :: mmsq
  real    :: mmsp1
  real    :: visc
  real    :: azfac
  real    :: sp1
  integer, dimension(nlons,nazmth) :: do_alpha
  real,    dimension(nlons,nazmth) :: i_alpha !< Variable description\f$[units]\f$
  real,    dimension(nlons,nazmth) :: mmin_alpha
  !-----------------------------------------------------------------------
  !
  !  quit if work arrays not big enough.
  !
  ! ccc      if (ibig<nlons)  then
  ! ccc        write (6,*)
  ! ccc        WRITE (6,*) ' HINES_WAVNUM2: increase IBIG'
  ! ccc        stop
  ! ccc      end if
  !
  sp1 = slope + 1.
  mmsq = m_min ** 2
  mmsp1 = m_min ** sp1
  !
  !  indices of levels to process.
  !
  if (levbot > levtop) then
    lstart = levbot - 1
    lincr  = - 1
  else
    lstart = levbot + 1
    lincr  = 1
  end if
  !
  !  initialize logical :: flags and determine number of longitudes
  !  at bottom where calculations to be done.
  !
  do l = 1,nlevs
    do i = il1,il2
      ! ccc        dragil(i,l) = .false.
      dragil(i,l) = 0
    end do
  end do ! loop 5
  icount = 0
  do i = il1,il2
    if (drag(i) == 1)  icount = icount + 1
    dragil(i,levbot) = drag(i)
  end do ! loop 10
  do  n = 1,nazmth
    do  i = il1,il2
      do_alpha(i,n) = drag(i)
    end do
  end do ! loop 15
  !
  !  use horizontal isotropy to calculate azimuthal variances at bottom level.
  !
  azfac = 1. / float(naz)
  do n = 1,naz
    do i = il1,il2
      if (drag(i) == 1) then
        sigsqh_alpha(i,levbot,n) = azfac * rms_wind(i) ** 2
      end if
    end do
  end do ! loop 20
  !
  !  velocity variances at bottom level.
  !
  call hines_sigma (sigma_t, sigma_alpha, &
                    sigsqh_alpha, drag, naz, levbot, &
                    il1, il2, nlons, nlevs, nazmth)
  !
  !  calculate cutoff wavenumber and spectral amplitude factor
  !  at bottom level where it is assumed that background winds vanish
  !  and also initialize minimum value of cutoff wavnumber.
  !
  if (slope == 1.) then
    do n = 1,naz
      do i = il1,il2
        if (drag(i) == 1) then
          m_alpha(i,levbot,n) =  bvfb(i) / &
                                (f1 * sigma_alpha(i,levbot,n) &
                                + f2 * sigma_t(i,levbot) )
          ak_alpha(i,n) = 2. * sigsqh_alpha(i,levbot,n) / &
                          (m_alpha(i,levbot,n) ** 2 - mmsq)
          mmin_alpha(i,n) = m_alpha(i,levbot,n)
          ! ccc              if (ak_alpha(i,n)<0.) then
          ! ccc                 write(6,*) "####### AK_ALPHA(i,n) lesss thn zero "
          ! ccc             write(6,*) "####","i,n,levbot,f1,f2"
          ! ccc             write(6,*) "####",i,n,levbot,f1,f2
          ! ccc             write(6,*) "####","SIGSQH_ALPHA(I,LEVBOT,N),bvfb(i)"
          ! ccc             write(6,*) "####",sigsqh_alpha(i,levbot,n),bvfb(i)
          ! ccc           write(6,*) "####","SIGMA_ALPHA(I,LEVBOT,N),SIGMA_T(I,LEVBOT)"
          ! ccc             write(6,*) "####",sigma_alpha(i,levbot,n),sigma_t(i,levbot)
          ! ccc             write(6,*) "####","M_ALPHA(I,LEVBOT,N)**2 - MMSQ"
          ! ccc             write(6,*) "####",m_alpha(i,levbot,n)**2 - mmsq
          ! ccc             write(6,*) "####","M_ALPHA(I,LEVBOT,N)**2,MMSQ"
          ! ccc             write(6,*) "####",m_alpha(i,levbot,n)**2,mmsq
          ! ccc              end if
        end if
      end do
    end do ! loop 30
  else
    do n = 1,naz
      do i = il1,il2
        if (drag(i) == 1) then
          m_alpha(i,levbot,n) =  bvfb(i) / &
                                (f1 * sigma_alpha(i,levbot,n) &
                                + f2 * sigma_t(i,levbot) )
          ak_alpha(i,n) = sp1 * sigsqh_alpha(i,levbot,n) / &
                          (m_alpha(i,levbot,n) ** sp1 - mmsp1)
          mmin_alpha(i,n) = m_alpha(i,levbot,n)
        end if
      end do
    end do ! loop 40
  end if
  !
  !  calculate quantities from the bottom upwards,
  !  starting one level above bottom.
  !
  do l = lstart,levtop,lincr
    !
    !  return to calling program if no more longitudes left to do.
    !
    if (icount == 0)  return
    !
    !  level beneath present level.
    !
    lbelow = l - lincr
    !
    !  compute azimuthal wind components from zonal and meridional winds.
    !
    call hines_wind (v_alpha, &
                     vel_u(1,l), vel_v(1,l), ubot, vbot, &
                     dragil(1,lbelow), naz, il1, il2, &
                     nlons, nazmth)
    !
    !  calculate n/m_m where m_m is maximum permissible value of the vertical
    !  wavenumber (i.e., m > m_m are obliterated) and n is buoyancy frequency.
    !  m_m is taken as the smaller of the instability-induced
    !  wavenumber (m_sub_m_turb) and that imposed by molecular viscosity
    !  (m_sub_m_mol). since variance at this level is not yet known
    !  use value at level below.
    !
    do i = il1,il2
      if (dragil(i,lbelow) == 1) then
        visc = max ( visc_mol(i,l), visc_min)
        m_sub_m_turb = bvfreq(i,l) / ( f2 * sigma_t(i,lbelow) )
        m_sub_m_mol  = (bvfreq(i,l) * kstar/visc) ** 0.33333333/f3
        if (m_sub_m_turb < m_sub_m_mol) then
          n_over_m(i) = f2 * sigma_t(i,lbelow)
        else
          n_over_m(i) = bvfreq(i,l) / m_sub_m_mol
        end if
      end if
    end do ! loop 50
    !
    !  calculate cutoff wavenumber at this level.
    !
    do n = 1,naz
      do i = il1,il2
        !
        if (do_alpha(i,n) == 1) then
          !
          !  calculate trial value (since variance at this level is not yet known
          !  use value at level below). if trial value is negative or if it exceeds
          !  minimum value found at lower levels (not permitted) then set it
          !  to this minimum value.
          !
          m_trial = bvfb(i) / ( f1 * sigma_alpha(i,lbelow,n) &
                    + n_over_m(i) + v_alpha(i,n) )
          if (m_trial <= 0. .or. m_trial > mmin_alpha(i,n)) then
            m_trial = mmin_alpha(i,n)
          end if
          m_alpha(i,l,n) = m_trial
          !
          !  do not permit cutoff wavenumber to be less than minimum allowable value.
          !
          if (m_alpha(i,l,n) < m_min)  m_alpha(i,l,n) = m_min
          !
          !  reset minimum value of cutoff wavenumber if necessary.
          !
          if (m_alpha(i,l,n) < mmin_alpha(i,n)) then
            mmin_alpha(i,n) = m_alpha(i,l,n)
          end if
          !
        else
          !
          m_alpha(i,l,n) = m_min
          !
        end if
        !
      end do
    end do ! loop 60
    !
    !  calculate the hines integral at this level.
    !
    call hines_intgrl2 (i_alpha, &
                        v_alpha, m_alpha, bvfb, &
                        dragil(1,l), do_alpha, m_min, slope, naz, &
                        l, il1, il2, nlons, nlevs, nazmth)
    !
    !  calculate the velocity variances at this level.
    !
    do i = il1,il2
      if (dragil(i,l) == 1) then
        sigfac(i) = densb(i) / density(i,l) &
                    * bvfreq(i,l) / bvfb(i)
        ! ccc            if (sigfac(i)<0.) then
        ! ccc               write(6,*) "##### SIGFAC(I)<0."
        ! ccc               write(6,*) "####","i,l,SIGFAC(I) ",i,l,sigfac(i)
        ! ccc               write(6,*) "####","DENSB(I),DENSITY(I,L)"
        ! ccc               write(6,*) "####",densb(i),density(i,l)
        ! ccc               write(6,*) "####","BVFREQ(I,L),BVFB(I)"
        ! ccc               write(6,*) "####",bvfreq(i,l),bvfb(i)
        ! ccc            end if
      end if
    end do ! loop 80
    !
    !  calculate the velocity variances at this level.
    !
    do n = 1,naz
      do i = il1,il2
        if (dragil(i,l) == 1) then
          sigsqh_alpha(i,l,n) = sigfac(i) * ak_alpha(i,n) &
                                * i_alpha(i,n)
          ! ccc            if (sigsqh_alpha(i,l,n)<0.) then
          ! ccc               write(6,*) "###### SIGSQH_ALPHA(I,L,N) is lt 0."
          ! ccc               write(6,*) "####","i,l,n ",i,l,n
          ! ccc               write(6,*) "####","SIGFAC(I),AK_ALPHA(I,N),I_ALPHA(I,N)"
          ! ccc               write(6,*) "####",sigfac(i),ak_alpha(i,n),i_alpha(i,n)
          ! ccc            end if
        end if
      end do
    end do ! loop 90
    call hines_sigma (sigma_t, sigma_alpha, &
                      sigsqh_alpha, dragil(1,l), naz, l, &
                      il1, il2, nlons, nlevs, nazmth)
    !
    !  if total rms wind zero (no more drag) then set drag to false
    !  and update longitude counter.
    !
    do i = il1,il2
      if (sigma_t(i,l) == 0.) then
        dragil(i,l) = 0
        icount = icount - 1
      end if
    end do ! loop 110
    !
    !  end of level loop.
    !
  end do ! loop 150
  !
  return
  !-----------------------------------------------------------------------
end subroutine hines_wavnum2
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
