!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine hines_wind (v_alpha,vel_u,vel_v,ubot,vbot,drag, &
                       naz,il1,il2,nlons,nazmth)
  !
  !  this routine calculates the azimuthal horizontal background wind components
  !  on a longitude at a single altitude for the case of 4, 8, 12 or 16 equally
  !  spaced azimuths needed for the Hines' Doppler spread GWD parameterization
  !  scheme.
  !
  !  aug. 7/95 - c. mclandress
  !
  !  modifications:
  !  --------------
  !  feb. 2/96 - c. mclandress (added: 12 and 16 azimuths; logical :: flags
  !                             only single level calculation; removed umin)

  implicit none

  !
  !  output arguement:
  !  ----------------
  !
  !     * v_alpha = background wind component at each azimuth (m/s).
  !     *           (note: first azimuth is in eastward direction
  !     *            and rotate in counterclockwise direction.)
  !
  !  input arguements:
  !  ----------------
  !
  !     * vel_u  = background zonal wind component (m/s).
  !     * vel_v  = background meridional wind component (m/s).
  !     * ubot   = background zonal wind component at bottom level.
  !     * vbot   = background meridional wind component at bottom level.
  !     * drag   = logical :: flag indicating longitudes where calculations
  !     *          to be performed.
  !     * naz    = number of horizontal azimuths used (4, 8, 12 or 16).
  !     * il1    = first longitudinal index to use (il1 >= 1).
  !     * il2    = last longitudinal index to use (il1 <= il2 <= nlons).
  !     * nlons  = number of longitudes.
  !     * nazmth = azimuthal array dimension (nazmth >= naz).
  !
  !  constants in data statements.
  !  ----------------------------
  !
  !     * cos45 = cosine of 45 degrees.
  !     * cos30 = cosine of 30 degrees.
  !     * sin30 = sine of 30 degrees.
  !     * cos22 = cosine of 22.5 degrees.
  !     * sin22 = sine of 22.5 degrees.
  !
  !  subroutine arguements.
  !  ---------------------
  !
  integer, intent(in)  :: naz !< Variable description\f$[units]\f$
  integer, intent(in)  :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in)  :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in)  :: nlons !< Variable description\f$[units]\f$
  integer, intent(in)  :: nazmth !< Variable description\f$[units]\f$
  integer, intent(in), dimension(nlons) :: drag !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons,nazmth) :: v_alpha !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons) :: vel_u !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons) :: vel_v !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons) :: ubot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons) :: vbot !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  !  internal variables.
  !  -------------------
  !
  integer  :: i
  real   :: u
  real   :: v
  real   :: cos45
  real   :: cos30
  real   :: sin30
  real   :: cos22
  real   :: sin22
  !
  data  cos45 / 0.7071068 /
  data  cos30 / 0.8660254 /, sin30 / 0.5       /
  data  cos22 / 0.9238795 /, sin22 / 0.3826834 /
  !-----------------------------------------------------------------------
  !
  !  case with 4 azimuths.
  !
  if (naz == 4) then
    do i = il1,il2
      if (drag(i) == 1) then
        u = vel_u(i) - ubot(i)
        v = vel_v(i) - vbot(i)
        v_alpha(i,1) = u
        v_alpha(i,2) = v
        v_alpha(i,3) = - u
        v_alpha(i,4) = - v
      end if
    end do ! loop 20
  end if
  !
  !  case with 8 azimuths.
  !
  if (naz == 8) then
    do i = il1,il2
      if (drag(i) == 1) then
        u = vel_u(i) - ubot(i)
        v = vel_v(i) - vbot(i)
        v_alpha(i,1) = u
        v_alpha(i,2) = cos45 * ( v + u)
        v_alpha(i,3) = v
        v_alpha(i,4) = cos45 * ( v - u)
        v_alpha(i,5) = - u
        v_alpha(i,6) = - v_alpha(i,2)
        v_alpha(i,7) = - v
        v_alpha(i,8) = - v_alpha(i,4)
      end if
    end do ! loop 30
  end if
  !
  !  case with 12 azimuths.
  !
  if (naz == 12) then
    do i = il1,il2
      if (drag(i) == 1) then
        u = vel_u(i) - ubot(i)
        v = vel_v(i) - vbot(i)
        v_alpha(i,1)  = u
        v_alpha(i,2)  = cos30 * u + sin30 * v
        v_alpha(i,3)  = sin30 * u + cos30 * v
        v_alpha(i,4)  = v
        v_alpha(i,5)  = - sin30 * u + cos30 * v
        v_alpha(i,6)  = - cos30 * u + sin30 * v
        v_alpha(i,7)  = - u
        v_alpha(i,8)  = - v_alpha(i,2)
        v_alpha(i,9)  = - v_alpha(i,3)
        v_alpha(i,10) = - v
        v_alpha(i,11) = - v_alpha(i,5)
        v_alpha(i,12) = - v_alpha(i,6)
      end if
    end do ! loop 40
  end if
  !
  !  case with 16 azimuths.
  !
  if (naz == 16) then
    do i = il1,il2
      if (drag(i) == 1) then
        u = vel_u(i) - ubot(i)
        v = vel_v(i) - vbot(i)
        v_alpha(i,1)  = u
        v_alpha(i,2)  = cos22 * u + sin22 * v
        v_alpha(i,3)  = cos45 * ( u + v)
        v_alpha(i,4)  = cos22 * v + sin22 * u
        v_alpha(i,5)  = v
        v_alpha(i,6)  = cos22 * v - sin22 * u
        v_alpha(i,7)  = cos45 * ( v - u)
        v_alpha(i,8)  = - cos22 * u + sin22 * v
        v_alpha(i,9)  = - u
        v_alpha(i,10) = - v_alpha(i,2)
        v_alpha(i,11) = - v_alpha(i,3)
        v_alpha(i,12) = - v_alpha(i,4)
        v_alpha(i,13) = - v
        v_alpha(i,14) = - v_alpha(i,6)
        v_alpha(i,15) = - v_alpha(i,7)
        v_alpha(i,16) = - v_alpha(i,8)
      end if
    end do ! loop 50
  end if
  !
  return
  !-----------------------------------------------------------------------
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
