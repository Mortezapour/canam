!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine clouds18(ccld, cld, rh, clw, cic, &
                    wcdw, wcdi, radeqvw, radeqvi, cldcdd, eta, &
                    shj,  dz,   qlwc, qiwc, t, h, &
                    zclf, zcdn, sclf, scdn, slwc, psfc, bcyload, &
                    il1, il2, ilg, lay, lev, switch_pla)
  !
  !     * apr 26/2018 - j.cole removed calculation of clwt, cict, cldt
  !     * jun 25/2013 - k.vonsalzen/new version for gcm17:
  !     *               j.li.       - implement pam option.
  !     *                           - implement semi-direct
  !     *                             effect updates. eta calculated
  !     *                             and passed out, rho promoted to
  !     *                             an internal array.
  !     * feb 12/2009 - jason cole.  previous version clouds17 for
  !     *                            gcm15h/gcm15i/gcm16:
  !     *                            - hard-coded value of 75.46 for
  !     *                              liquid equivilent radius is
  !     *                              now properly decomposed into
  !     *                              pifac=62.035 and the "TUNABLE"
  !     *                              factor beta (increased to 1.3
  !     *                             to give better agreement with
  !     *                             observations of liquid equivilent
  !     *                             radius).
  !     * oct 15/2007 - m.lazare.    previous version clouds16 for gcm15g:
  !     *                            - dz now input array from physics
  !     *                              and not calculated inside, so
  !     *                              shtj not required and thus removed.
  !     *                            - wcdw and wcdi are now physics
  !     *                              work arrays and are thus passed
  !     *                              out. we use instead wclw and
  !     *                              wcli work arrays to calculate
  !     *                              total cloud at the end.
  !     *                            - code related to cloud optical
  !     *                              properties removed.
  !     *                            - cosmetic re-organization of input/
  !     *                              output fields.
  !     *                            - removal of unused parameters.
  !     * jan 13/2007 - m.lazare.    previous version clouds15 for gcm15f.
  !
  use phys_consts, only : rgas, actfrc
  implicit none
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: lay  !< Number of vertical layers \f$[unitless]\f$
  integer, intent(in) :: lev  !< Number of vertical levels plus 1 \f$[unitless]\f$
  logical, intent(in) :: switch_pla !< Switch to use PAM or bulk aerosol scheme \f$[unitless]\f$
  !
  !     * output arrays...
  !

  real, intent(inout), dimension(ilg,lay) :: ccld !< Variable description \f$[units]\f$
  real, intent(inout), dimension(ilg,lay) :: cld !< Variable description \f$[units]\f$
  real, intent(inout), dimension(ilg,lay) :: rh !< Variable description \f$[units]\f$
  real, intent(inout), dimension(ilg,lay) :: clw !< Variable description \f$[units]\f$
  real, intent(inout), dimension(ilg,lay) :: cic !< Variable description \f$[units]\f$
  real, intent(inout), dimension(ilg,lay) :: wcdw !< Variable description \f$[units]\f$
  real, intent(inout), dimension(ilg,lay) :: wcdi !< Variable description \f$[units]\f$
  real, intent(inout), dimension(ilg,lay) :: radeqvw !< Variable description \f$[units]\f$
  real, intent(inout), dimension(ilg,lay) :: radeqvi !< Variable description \f$[units]\f$
  real, intent(inout), dimension(ilg,lay) :: cldcdd !< Variable description \f$[units]\f$
  real, intent(inout), dimension(ilg,lay) :: eta !< Variable description \f$[units]\f$

  !     * input arrays...

  real, intent(in), dimension(ilg,lay) :: shj   !< Eta-level for mid-point of thermodynamic layer \f$[unitless]\f$
  real, intent(in), dimension(ilg,lay) :: dz   !< Layer thickness for thermodynamic layers \f$[m]\f$
  real, intent(in), dimension(ilg,lay) :: qlwc !< Variable description \f$[units]\f$
  real, intent(in), dimension(ilg,lay) :: qiwc !< Variable description \f$[units]\f$
  real, intent(in), dimension(ilg,lay) :: t !< Variable description \f$[units]\f$
  real, intent(in), dimension(ilg,lay) :: h !< Variable description \f$[units]\f$
  real, intent(in), dimension(ilg,lay) :: zclf !< Variable description \f$[units]\f$
  real, intent(in), dimension(ilg,lay) :: zcdn !< Variable description \f$[units]\f$
  real, intent(in), dimension(ilg,lay) :: bcyload !< Variable description \f$[units]\f$
  real, intent(in), dimension(ilg,lay) :: sclf !< Variable description \f$[units]\f$
  real, intent(in), dimension(ilg,lay) :: scdn !< Variable description \f$[units]\f$
  real, intent(in), dimension(ilg,lay) :: slwc !< Variable description \f$[units]\f$

  real, intent(in), dimension(ilg) :: psfc !< Variable description \f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  real, parameter :: wcmin = 0.00001, cut = 0.001
  real, parameter :: zcdnmin = 1.0, zcdnmax =500.0
  real, parameter :: third = 0.3333333333333
  real, parameter :: cldmin = 1.e-9
  real, parameter :: rieffmax = 50.0
  real, parameter :: rweffmin = 2.0, rweffmax = 30.0
  !
  !     * coefficients for liquid equivilent radius.
  !     * note that these values are defined in cld_particle_size_cld_gen2.
  !     * make sure they are consistent !
  !
  real, parameter :: pifac = 62.035
  real, parameter :: beta  = 1.3
  !==================================================================

  real :: betas
  real :: cdd
  real :: cvol
  integer :: j
  integer :: jp1
  integer :: k
  integer :: lev1
  real :: p
  real :: pcdnc
  real :: tclf
  real :: wcfac
  real :: zrieff
  !
  !     * internal work arrays...
  !
  real, dimension(ilg,lay) :: wcl
  real, dimension(ilg,lay) :: wcd
  real, dimension(ilg,lay) :: wclw
  real, dimension(ilg,lay) :: wcli
  real, dimension(ilg,lay) :: rho
  !
  real, parameter :: zero = 0.0, one = 1.0
  !
  !     * there must be no cloud within first "LEV1" layers for radiation
  !     * to work properly. this is defined in the "TOPLW" subroutine
  !     * called at the beginning of the model.
  !
  common /itoplw/ lev1
  !
  !=======================================================================
  !     * initialize arrays and calculate layer thickness.
  !
  do j = 1, lay
    do k = il1, il2
      wcdw(k,j) = 0.
      wcdi(k,j) = 0.
      ccld(k,j) = 0.
      cld (k,j) = 0.
      eta (k,j) = 0.
    end do
  end do ! loop 80

  !----------------------------------------------------------------------c
  !     * cloud amount and water/ice contents.
  !----------------------------------------------------------------------c
  do j = 1, lay
    jp1 = j + 1
    do k = il1, il2
      p = psfc(k) * shj(k,j)
      rho(k,j) = p / (rgas * t(k,j))
      !
      if (switch_pla) then
        tclf = min(max(zclf(k,j),zero),one)
      else
        tclf = min(max(zclf(k,j),sclf(k,j),zero),one)
      end if
      if (tclf >= cut) then
        if (switch_pla) then
          wcdw(k,j) = qlwc(k,j) * rho(k,j) * 1000./tclf
        else
          wcdw(k,j) = (qlwc(k,j) + slwc(k,j)) * rho(k,j) * 1000./tclf
        end if
        wcdi(k,j) = qiwc(k,j) * rho(k,j) * 1000. / tclf
      else
        wcdw(k,j) = 0.
        wcdi(k,j) = 0.
      end if
      !
      !         * generate local clouds.
      !
      cvol = tclf
      if (j > lev1) ccld(k,j) = min(cvol,one)

      !
      !         * assign ccld and h to cld and rh
      !
      cld(k,j)  =  ccld(k,j)
      rh (k,j)  =  h(k,j)

    end do ! loop 85
  end do ! loop 95

  !----------------------------------------------------------------------c
  !     * cloud optical properties.
  !----------------------------------------------------------------------c
  do j = 1, lay
    jp1 = j + 1
    do k = il1, il2
      !
      !         * define water/ice paths, optical depth and emmissivity.
      !
      wcd(k,j)  = wcdw(k,j) + wcdi(k,j)
      clw(k,j)  = qlwc(k,j)
      if (.not. switch_pla) clw(k,j) = clw(k,j) + slwc(k,j)
      cic(k,j)  = qiwc(k,j)
      wclw(k,j) = wcdw(k,j) * dz(k,j)
      wcli(k,j) = wcdi(k,j) * dz(k,j)
      wcl(k,j)  = wclw(k,j)
      !
      !         * calculate equivalent radius (microns) for both phases,
      !         * assuming gamma size distribution.
      !         * note that cloud droplet density now includes effect of
      !         * shallow clouds !
      !
      if (switch_pla) then
        pcdnc = (zclf(k,j) * zcdn(k,j))/max(ccld(k,j),cldmin)
      else
        pcdnc = (zclf(k,j) * zcdn(k,j) + sclf(k,j) * scdn(k,j)) &
                /max(ccld(k,j),cldmin)
      end if
      cdd = max(min(zcdnmax,pcdnc * 1.e-6),zcdnmin)
      cldcdd(k,j) = cdd
      !
      if (switch_pla) then
        !
        !           * dispersion effect (peng and lohmann, 2003).
        !
        betas = (1.22 + 0.00084 * cdd)
      else
        betas = beta
      end if
      radeqvw(k,j) = betas * pifac * (wcdw(k,j)/cdd) ** third
      radeqvw(k,j) = max(min(radeqvw(k,j),rweffmax),rweffmin)
      !
      wcfac = max(wcdi(k,j),wcmin)
      zrieff = 83.8 * wcfac ** 0.216
      radeqvi(k,j) = min(zrieff,rieffmax)
      !
      !----------------------------------------------------------------------c
      !     eta (volume ratio of bc in cloud droplet) 0.9 = bc density       c
      !     factor 1000, since wcdw in units g/m^3, wcdw is the grid mean    c
      !     results, wcdw/ccld is the cloud located area mean                c
      !----------------------------------------------------------------------c
      !
      if (wcdw(k,j) > 1.e-05) then
        eta(k,j) =  1000. * actfrc * rho(k,j) * ccld(k,j) * &
                   bcyload(k,j) / (0.9 * wcdw(k,j))
      else
        eta(k,j) =  0.0
      end if
    end do ! loop 150
  end do ! loop 190
  !
  return
end subroutine clouds18
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
