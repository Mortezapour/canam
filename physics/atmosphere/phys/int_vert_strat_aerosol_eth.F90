!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine int_vert_strat_aerosol_eth(sw_ext_sa_gcm, & ! output
                                      sw_ssa_sa_gcm, &
                                      sw_g_sa_gcm, &
                                      lw_abs_sa_gcm, &
                                      w055_vtau_sa_gcm, &
                                      w110_vtau_sa_gcm, &
                                      w055_ext_sa_gcm, &
                                      w110_ext_sa_gcm, &
                                      sw_ext_sa_in, & ! input
                                      sw_ssa_sa_in, &
                                      sw_g_sa_in, &
                                      lw_ext_sa_in, &
                                      lw_ssa_sa_in, &
                                      w055_ext_sa_in, &
                                      w110_ext_sa_in, &
                                      pres_sa_in, &
                                      shtj, &
                                      shj, &
                                      dz, &
                                      trop_pres, &
                                      pres_sfc, &
                                      ilg, &
                                      il1, &
                                      il2, &
                                      nlev_gcm, &
                                      nlay_gcm, &
                                      nlev_sa, &
                                      nbs, &
                                      nbl)


  !
  !     * jan 10/17 - j. cole
  !
  !     * interpolate stratospheric aerosol data from ethz vertical
  !     * grid (pressures) onto the current gcm vertical levels and
  !     * generate some diagnostic output.
  !
  !     method:
  !
  !     interpolate data from input vertical grid onto gcm vertical grid.
  !     the stratospheric aerosol dataset extends rather low down into the atmosphere,
  !     so we follow the ethz advice and only use the aerosol optical properties above
  !     the local tropopause.
  !
  !     for diagnostics we save the vertically integrated optical thickness above
  !     the local tropopause for 0.55 and 11 microns.
  !
  !     note that the gcm pressure goes from top to bottom while ethz pressures
  !     go from bottom to top.

  implicit none

  ! parameters

  real, parameter :: r_zero = 0.0
  real, parameter :: r_one  = 1.0
  real, parameter :: m2km   = 0.001

  ! input

  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: nlev_gcm !< Variable description\f$[units]\f$
  integer, intent(in) :: nlay_gcm !< Variable description\f$[units]\f$
  integer, intent(in) :: nlev_sa !< Variable description\f$[units]\f$
  integer, intent(in) :: nbs   !< Number of wavelength intervals for solar radiative transfer \f$[unitless]\f$
  integer, intent(in) :: nbl   !< Number of wavelength intervals for thermal radiative transfer \f$[unitless]\f$

  real, intent(in) , dimension(ilg,nlev_sa,nbs) :: sw_ext_sa_in !< (1/km)\f$[units]\f$
  real, intent(in) , dimension(ilg,nlev_sa,nbs) :: sw_ssa_sa_in !< (unitless)\f$[units]\f$
  real, intent(in) , dimension(ilg,nlev_sa,nbs) :: sw_g_sa_in !< (unitless)\f$[units]\f$
  real, intent(in) , dimension(ilg,nlev_sa,nbl) :: lw_ext_sa_in !< (1/km)\f$[units]\f$
  real, intent(in) , dimension(ilg,nlev_sa,nbl) :: lw_ssa_sa_in !< (unitless)\f$[units]\f$
  real, intent(in) , dimension(ilg,nlev_sa) :: w055_ext_sa_in !< (1/km)\f$[units]\f$
  real, intent(in) , dimension(ilg,nlev_sa) :: w110_ext_sa_in !< (1/km)\f$[units]\f$
  real, intent(in) , dimension(ilg,nlev_sa) :: pres_sa_in !< (Pa)\f$[units]\f$
  real, intent(in) , dimension(ilg) :: trop_pres !< (hPa)\f$[units]\f$
  real, intent(in) , dimension(ilg,nlev_gcm) :: shtj   !< Eta-level for top of thermodynamic layer, plus eta-level for surface (base of lowest layer) \f$[unitless]\f$
  real, intent(in) , dimension(ilg,nlay_gcm) :: shj   !< Eta-level for mid-point of thermodynamic layer \f$[unitless]\f$
  real, intent(in) , dimension(ilg) :: pres_sfc !< (hPa)\f$[units]\f$
  real, intent(in) , dimension(ilg,nlay_gcm) :: dz   !< Layer thickness for thermodynamic layers \f$[m]\f$

  ! output
  real, intent(out) , dimension(ilg,nlay_gcm,nbs) :: sw_ext_sa_gcm !< (1/km)\f$[units]\f$
  real, intent(out) , dimension(ilg,nlay_gcm,nbs) :: sw_ssa_sa_gcm !< (unitless)\f$[units]\f$
  real, intent(out) , dimension(ilg,nlay_gcm,nbs) :: sw_g_sa_gcm !< (unitless)\f$[units]\f$
  real, intent(out) , dimension(ilg,nlay_gcm,nbl) :: lw_abs_sa_gcm !< (1/km)\f$[units]\f$
  real, intent(out) , dimension(ilg) :: w055_vtau_sa_gcm !< (unitless)\f$[units]\f$
  real, intent(out) , dimension(ilg) :: w110_vtau_sa_gcm !< (unitless)\f$[units]\f$
  real, intent(out) , dimension(ilg,nlay_gcm) :: w055_ext_sa_gcm !< (unitless)\f$[units]\f$
  real, intent(out) , dimension(ilg,nlay_gcm) :: w110_ext_sa_gcm !< (unitless)\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  ! local

  integer :: ilay_sa
  integer :: ilay_gcm
  integer :: ilev_gcm
  integer :: il
  integer :: ib
  integer :: ip
  integer :: nlev_sa_m1

  real :: pres_lev1
  real :: pres_lev2
  real :: sum_wgt
  real :: w055_ext
  real :: w110_ext
  real :: dz_loc

  integer, dimension(ilg) :: lay_ind_trop
  integer, dimension(2) :: lp

  real, dimension(ilg,nlay_gcm) :: pres_lay
  real, dimension(2) :: w
  real, dimension(nbs) :: sw_ext
  real, dimension(nbs) :: sw_ssa
  real, dimension(nbs) :: sw_g
  real, dimension(nbl) :: lw_abs

  nlev_sa_m1 = nlev_sa - 1

  !
  ! zero out the output, values are only added if above the tropopause.
  !

  sw_ext_sa_gcm(:,:,:) = r_zero
  sw_ssa_sa_gcm(:,:,:) = r_zero
  sw_g_sa_gcm(:,:,:)   = r_zero
  lw_abs_sa_gcm(:,:,:) = r_zero
  w055_vtau_sa_gcm(:)  = r_zero
  w110_vtau_sa_gcm(:)  = r_zero
  w055_ext_sa_gcm(:,:) = r_zero
  w110_ext_sa_gcm(:,:) = r_zero

  !
  ! compute the pressure of the gcm layers.
  !

  do ilay_gcm = 1, nlay_gcm
    do il = il1, il2
      pres_lay(il,ilay_gcm) = pres_sfc(il) * shj(il,ilay_gcm)
    end do ! il
  end do ! ilay_gcm

  !
  ! find the local model layer that contains the tropopause.
  !

  do ilay_gcm = 1, nlay_gcm
    do il = il1, il2
      pres_lev1 = pres_sfc(il) * shtj(il,ilay_gcm)
      pres_lev2 = pres_sfc(il) * shtj(il,ilay_gcm + 1)

      if (trop_pres(il) > pres_lev1 .and. &
          trop_pres(il) <= pres_lev2) then ! tropopause is somewhere in this layer
        lay_ind_trop(il) = ilay_gcm
      end if

    end do ! il
  end do ! ilay_gcm

  !
  ! loop over the model levels and interpolate the stratospheric aerosols
  ! from the ethz levels to the gcm levels.  this is only done for levels
  ! that are above the tropopause.
  !

  do ilay_gcm = 1, nlay_gcm
    do il = il1, il2
      if (ilay_gcm < lay_ind_trop(il)) then
        pres_lev1 =  pres_lay(il,ilay_gcm)

        sw_ext(:) = r_zero
        sw_ssa(:) = r_zero
        sw_g(:)   = r_zero
        lw_abs(:) = r_zero
        w055_ext  = r_zero
        w110_ext  = r_zero
        sum_wgt   = r_one

        do ilay_sa = 1, nlev_sa_m1

          if (pres_sa_in(il,ilay_sa)   >= pres_lev1 .and. &
              pres_sa_in(il,ilay_sa + 1) < pres_lev1) then

            w(1)    = pres_lev1 - pres_sa_in(il,ilay_sa + 1)
            w(2)    = pres_sa_in(il,ilay_sa) - pres_lev1
            lp(1)   = ilay_sa + 1
            lp(2)   = ilay_sa
            sum_wgt = sum(w)

            do ip = 1, 2
              sw_ext(:) = sw_ext(:) &
                          + sw_ext_sa_in(il,lp(ip),:) * w(ip)

              sw_ssa(:) = sw_ssa(:) &
                          + sw_ssa_sa_in(il,lp(ip),:) &
                          * sw_ext_sa_in(il,lp(ip),:) * w(ip)

              sw_g(:)   = sw_g(:) &
                          + sw_ssa_sa_in(il,lp(ip),:) &
                          * sw_ext_sa_in(il,lp(ip),:) &
                          * sw_g_sa_in(il,lp(ip),:) * w(ip)

              lw_abs(:) = lw_abs(:) &
                          + lw_ext_sa_in(il,lp(ip),:) &
                          * (r_one - lw_ssa_sa_in(il,lp(ip),:)) &
                          * w(ip)

              w055_ext  = w055_ext &
                          + w055_ext_sa_in(il,lp(ip)) * w(ip)

              w110_ext  = w110_ext &
                          + w110_ext_sa_in(il,lp(ip)) * w(ip)

            end do ! ip
          end if ! pres_sa_in
        end do ! ilay_sa

        if (sum(sw_ext) > r_zero) then
          sw_g_sa_gcm(il,ilay_gcm,:)   = sw_g(:)/sw_ssa(:)
          sw_ssa_sa_gcm(il,ilay_gcm,:) = sw_ssa(:)/sw_ext(:)
          sw_ext_sa_gcm(il,ilay_gcm,:) = sw_ext(:)/sum_wgt
        else
          sw_g_sa_gcm(il,ilay_gcm,:)   = r_zero
          sw_ssa_sa_gcm(il,ilay_gcm,:) = r_zero
          sw_ext_sa_gcm(il,ilay_gcm,:) = r_zero
        end if

        if (sum(lw_abs) > r_zero) then
          lw_abs_sa_gcm(il,ilay_gcm,:) = lw_abs(:)/sum_wgt
        else
          lw_abs_sa_gcm(il,ilay_gcm,:) = r_zero
        end if

        if (w055_ext > r_zero) then
          w055_ext_sa_gcm(il,ilay_gcm) = w055_ext/sum_wgt
        else
          w055_ext_sa_gcm(il,ilay_gcm) = r_zero
        end if

        if (w110_ext > r_zero) then
          w110_ext_sa_gcm(il,ilay_gcm) = w110_ext/sum_wgt
        else
          w110_ext_sa_gcm(il,ilay_gcm) = r_zero
        end if

        !
        ! diagnose the vertically integerate optical thickness above the local
        ! tropopause.
        !
        dz_loc = dz(il,ilay_gcm) * m2km ! layer thickness in km

        w055_vtau_sa_gcm(il) = w055_vtau_sa_gcm(il) &
                               + (w055_ext/sum_wgt) * dz_loc

        w110_vtau_sa_gcm(il) = w110_vtau_sa_gcm(il) &
                               + (w110_ext/sum_wgt) * dz_loc

      end if ! trop
    end do ! il
  end do ! ilay_gcm


  return

end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
