!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine soleff(heat,qof,s1,xvf,pvf,effh,kmx,kmxp,lmx,il1,il2)

  !***********************************************************************
  !*                                                                     *
  !*              subroutine soleff                                      *
  !*                                                                     *
  !***********************************************************************

  ! to take into account efficiency of the solar heating in the o3 hartley band
  ! by method of mlynczack &solomon (jgr, vol 98, p 10517, 1993)
  ! to evaluate the eficiency, it's supposed that 74% of the total solar
  ! heating (without the o2 heating) in the mesosphere is due to the "net"
  ! (i.e. without chemical potential energy) heating in the hartley band.
  ! the efficiency is calculated in thermdat subroutine and passed through
  ! common block thersh (array: effh).
  ! as well, the o3 heating rate above x=10 is made to be proportional to o3 vmr
  !
  !                                                 v. fomichev, november, 1997
  ! called by mamrad
  ! calls nothing


  implicit none
  integer :: il
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer :: k
  integer :: kk
  integer :: kko3
  integer, intent(in) :: kmx
  integer, intent(in) :: kmxp
  integer :: ko3
  integer, intent(in) :: lmx
  real :: p

  real, intent(inout), dimension(lmx,kmx) :: heat !< Variable description\f$[units]\f$
  real, intent(in), dimension(lmx,kmxp) :: s1 !< Variable description\f$[units]\f$
  real, intent(in), dimension(lmx,kmx) :: qof !< Variable description\f$[units]\f$
  real, intent(in), dimension(kmx) :: xvf !< Variable description\f$[units]\f$
  real, intent(in), dimension(kmx) :: pvf !< Variable description\f$[units]\f$
  real, intent(in), dimension(kmx) :: effh !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================


  !========================================================================
  !     * to make heating rate above x=10 to be proportional to o3
  !     * mixing ratio:

  do k=1,kmx
    if (xvf(k)<10) then
      ko3 = k
      kko3 = kmx+1-k
    end if
  end do ! loop 1

  do k=1,kmx
    if (xvf(k)>10) then
      kk = kmx+1-k
      do il = il1,il2
        heat(il,k) = heat(il,ko3)/qof(il,ko3)*qof(il,k)* &
                         (s1(1,kko3+1)-s1(1,kko3))/(s1(1,kk+1)-s1(1,kk))
      end do ! loop 2
    end if
  end do ! loop 3

  !     * to account for the efficiency in o3 ha band:

  do k=1,kmx
    p = pvf(k)
    if (p>1.) then
      cycle
    else
      do il = il1,il2
        heat(il,k) = heat(il,k)*effh(k)
      end do ! loop 10
    end if
  end do ! loop 20

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
