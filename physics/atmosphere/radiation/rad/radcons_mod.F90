!> \file radcons_mod.F90
!>\brief Physical constants used in radiative transfer
!!
!! @author Jiangnan Li and Deji Akingunola
!
module radcons_mod
  !
  !     * apr 30/2012 - m.lazare. new version for gcm16:
  !     *                         solar_c reduced from 1365 to 1361 in
  !     *                         line with newer sattellite obs.
  !     * feb 13/2009 - m.lazare. previous version radcons3 for gcm15h/i:
  !     *                         remove cfc-13 and cfc-14 effect since
  !     *                         uncertain.
  !     * apr 18/2007 - l.solheim. previous version radcons2 for gcm15g:
  !     *                          - mmr of trace gases now defined
  !     *                            in separate routine set_mmr (also
  !     *                            called else where in code). thus
  !     *                            "trace" common block explicit
  !     *                            in this routine removed (now in
  !     *                            set_mmr).
  !     * apr  3/2003 - m.lazare/ previous version radcons for gcm15f
  !     *               j.li.     and earlier.
  !
  !     *                        defines constants in common blocks
  !     *                        at start of model, used in li radiation.
  !
  !     * original common block containin ppm constants.
  !
  implicit none
  !==================================================================
  !      physical parameters for radiation
  !      SOLAR_C  solar constant in units W/n^2
  !      CH4_PPM  mixing ratio of CH4 in ppm
  !      N2O_PPM  mixing ratio of N2O in ppm
  !      F11_PPM  mixing ratio of CFC11 in ppm
  !      F12_PPM  mixing ratio of CFC12 in ppm
  !      F113_PPM mixing ratio of CFC113 in ppm
  !      F114_PPM mixing ratio of CFC114 in ppm
  !     * define ppm values (actually, these are concentrations
  !     * expressed as a ppm value multiplied by 1.e-6).
  !NOTE: The specified below are initial background values, which depending on the model
  !      setting can be adjusted with time-varying values (at specified times).
  real :: ch4_ppm  = 1.650   * 1.e-6
  real :: co2_ppm  = 348.0   * 1.e-6
  real :: f113_ppm = 0.0  ! 0.05e-3 * 1.e-6
  real :: f114_ppm = 0.0  ! 0.03e-3 * 1.e-6
  real :: f11_ppm  = 0.18e-3 * 1.e-6
  real :: f12_ppm  = 0.28e-3 * 1.e-6
  real :: n2o_ppm = 0.306   * 1.e-6

  real :: solar_c
  !
  !     * absorber mixing ratios (set in set_mmr).
  !
  real :: rmco2
  real :: rmch4
  real :: rmn2o
  real :: rmo2
  real :: rmf11
  real :: rmf12
  real :: rmf113
  real :: rmf114

 contains

  !
  !     * set corresponding mass mixing ratios.
  !
  subroutine set_mmr()
  !=======================================================================
  ! set mass mixing ratios for trace species concentrations.
  !
    use phys_parm_defs, only : pp_solar_const
    implicit none
  !----------------------------------------------------------------------
  !     trace gas concentrations in unit ppmv,
  !     parts per million by volume, transform to mass mixing ratio.
  !     the same as water vapor and ozone.
  !     1.5188126 = 44.    / 28.97
  !     0.5522955 = 16.    / 28.97
  !     1.5188126 = 44.    / 28.97
  !     o2 input as a constant, unit mixing rato by mass
  !     4.7418019 = 137.37 / 28.97
  !     4.1736279 = 120.91 / 28.97
  !     6.4704867 = 187.45 / 28.97
  !     5.6920953 = 164.90 / 28.97
  !     28.97 molecular weight of air, e-06 per million
  !----------------------------------------------------------------------
  !
     rmco2  =  co2_ppm  * 1.5188126
     rmch4  =  ch4_ppm  * 0.5522955
     rmn2o  =  n2o_ppm  * 1.5188126
     rmo2   =  0.2315
     rmf11  =  f11_ppm  * 4.7418019
     rmf12  =  f12_ppm  * 4.1736279
     rmf113 =  f113_ppm * 6.4704867
     rmf114 =  f114_ppm * 5.6920953

  !-------------------------------------------------------------
     solar_c  = pp_solar_const

     return
  end subroutine set_mmr

end module radcons_mod
!> \file
!> Radiative constants used in radiatived transfer.
