subroutine xtchempam(xrow,th,qv, &
                           sgpp,dshj,shj,cszrow,zdayl,pressg, &
                           sfrc,ohrow,h2o2row,o3row,no3row,lsvchem, &
                           dox4row,doxdrow,noxdrow,zclf,zmratep,zfsnow, &
                           zfrain,zmlwc,zfevap,clrfr,clrfs,zfsubl,atau, &
                           kount,ztmst,ntrac,ilg,il1,il2,ilev,lev)
  !
  !     * feb 04, 2015 - k.vonsalzen. new version for gcm18:
  !     *                             - zzoh scaled by tuning factor "ATUNE".

  !     * jun 10, 2014 - k.vonsalzen  subgrid tracer perturbation concentration.
  !     * june 2, 2013 - k.vonsalzen. pla version of xtchemie.
  !
  use phys_consts, only : grav, rgas, rgasv
  use tracers_info_mod, only: modl_tracers, iso2, idms, ihpo, iaindt, ntracp
  implicit none
  integer, intent(in) :: il1
  integer, intent(in) :: il2
  integer, intent(in) :: ilev
  integer, intent(in) :: ilg
  integer, intent(in) :: kount
  integer, intent(in) :: lev
  integer, intent(in) :: ntrac
  logical, intent(in) :: lsvchem
  real, intent(in) :: ztmst
  !
  real, dimension(ilg,ilev,ntrac), intent(inout) :: xrow !<
  real, dimension(ilg,ilev,ntrac), intent(inout) :: sfrc !<
  !
  real, dimension(ilg,ilev), intent(in)    :: th   !<
  real, dimension(ilg,ilev), intent(in)    :: qv   !<
  real, dimension(ilg,ilev), intent(in)    :: dshj !<
  real, dimension(ilg,ilev), intent(in)    :: shj  !<
  !
  real, dimension(ilg),      intent(in)    :: cszrow !<
  real, dimension(ilg),      intent(in)    :: zdayl !<
  real, dimension(ilg),      intent(in)    :: pressg !<
  real, dimension(ilg),      intent(inout) :: atau !<
  !
  !     * species passed in vmr (dimensionless).
  !
  real, dimension(ilg,ilev), intent(in)    :: ohrow   !<
  real, dimension(ilg,ilev), intent(in)    :: h2o2row !<
  real, dimension(ilg,ilev), intent(in)    :: o3row   !<
  real, dimension(ilg,ilev), intent(in)    :: no3row  !<
  !
  real, dimension(ilg),      intent(out)   :: dox4row !<
  real, dimension(ilg),      intent(out)   :: doxdrow !<
  real, dimension(ilg),      intent(out)   :: noxdrow !<
  !
  !     * arrays shared with cond in physics.
  !
  real, dimension(ilg,ilev), intent(in)    :: zclf !<
  real, dimension(ilg,ilev), intent(inout) :: zmratep !<
  real, dimension(ilg,ilev), intent(in)    :: zfrain !<
  real, dimension(ilg,ilev), intent(in)    :: clrfr !<
  real, dimension(ilg,ilev), intent(in)    :: zfsnow !<
  real, dimension(ilg,ilev), intent(inout) :: zmlwc !<
  real, dimension(ilg,ilev), intent(in)    :: zfevap !<
  real, dimension(ilg,ilev), intent(in)    :: clrfs !<
  real, dimension(ilg,ilev), intent(in)    :: zfsubl !<
  !
  real, dimension(ilg,ilev), intent(out)   :: sgpp !<
  !
  real :: asrphob
  real :: asrso4
  real :: factcon
  integer :: il
  integer :: iso4
  integer :: jk
  integer :: jt
  real :: pcons2
  real :: pqtmst
  real :: scalf
  real :: t
  real :: x
  real :: y
  real :: ysphr
  real :: zavo
  real :: zdms
  real :: zexp
  real :: zhil
  real :: zk2
  real :: zk2f
  real :: zk2i
  real :: zk3
  real :: zm
  real :: zmin
  real :: zmolgair
  real :: zmolgs
  real :: znamair
  real :: zso2
  real :: ztk1
  real :: ztk2
  real :: ztk23b
  real :: ztk3
  real :: ztka
  real :: ztkb
  real :: zxtp1dms
  real :: zxtp1so2
  !
  !     * internal work arrays:
  !
  !     * general work arrays for xtchemie5
  !
  real  ,  dimension(ilg,ilev,ntrac)  :: zxte !<
  real  ,  dimension(ilg,ilev,ntrac)  :: xcld !<
  real  ,  dimension(ilg,ilev,ntrac)  :: xclr !<
  real  ,  dimension(ilg,ilev)  :: zxtp10 !<
  real  ,  dimension(ilg,ilev)  :: zxtp1c !<
  real  ,  dimension(ilg,ilev)  :: zhenry !<
  real  ,  dimension(ilg,ilev)  :: zso4 !<
  real  ,  dimension(ilg,ilev)  :: zzoh !<
  real  ,  dimension(ilg,ilev)  :: zzh2o2 !<
  real  ,  dimension(ilg,ilev)  :: zzo3 !<
  real  ,  dimension(ilg,ilev)  :: zzo2 !<
  real  ,  dimension(ilg,ilev)  :: zzno3 !<
  real  ,  dimension(ilg,ilev)  :: agthpo !<
  real  ,  dimension(ilg,ilev)  :: zrho0 !<
  real  ,  dimension(ilg,ilev)  :: zrhctox !<
  real  ,  dimension(ilg,ilev)  :: zrhxtoc !<
  real  ,  dimension(ilg,ilev)  :: zmtof
  real  ,  dimension(ilg,ilev)  :: zdep3d !<
  real  ,  dimension(ilg,ilev)  :: pdclr !<
  real  ,  dimension(ilg,ilev)  :: pdcld !<
  real  ,  dimension(ilg,ilev)  :: tmpscl !<
  real  ,  dimension(ilg,ilev)  :: xarow !<
  real  ,  dimension(ilg,ilev)  :: xcrow !<
  real  ,  dimension(ilg,ilev)  :: dxcdt !<
  real  ,  dimension(ilg,ilev)  :: dxadt !<
  real  ,  dimension(ilg,ilev)  :: dxdt !<
  real  ,  dimension(ilg)  :: za21 !<
  real  ,  dimension(ilg)  :: za22 !<
  real  ,  dimension(ilg)  :: zdt !<
  real  ,  dimension(ilg)  :: ze3 !<
  real  ,  dimension(ilg)  :: zfac1 !<
  real  ,  dimension(ilg)  :: zf_o3 !<
  real  ,  dimension(ilg)  :: zh2o2m !<
  real  ,  dimension(ilg)  :: zso2m !<
  real  ,  dimension(ilg)  :: zso4m !<
  real  ,  dimension(ilg)  :: zsumh2o2 !<
  real  ,  dimension(ilg)  :: zsumo3 !<
  real  ,  dimension(ilg)  :: zxtp1 !<
  real  ,  dimension(ilg)  :: zza !<
  !
  !     * work arrays used in oxistr3 only.
  !
  integer, parameter :: nage = 1
  integer, parameter :: neqp = 13
  logical, dimension(ilg,ilev)  :: kcalc !<
  real  ,  dimension(ilg,ilev,neqp)  :: achpa !<
  real  ,  dimension(ilg,ilev)  :: roarow !<
  real  ,  dimension(ilg,ilev)  :: thg !<
  real  ,  dimension(ilg,ilev)  :: agtso2 !<
  real  ,  dimension(ilg,ilev)  :: agtso4 !<
  real  ,  dimension(ilg,ilev)  :: agto3 !<
  real  ,  dimension(ilg,ilev)  :: agtco2 !<
  real  ,  dimension(ilg,ilev)  :: agtho2 !<
  real  ,  dimension(ilg,ilev)  :: agthno3 !<
  real  ,  dimension(ilg,ilev)  :: agtnh3 !<
  real  ,  dimension(ilg,ilev)  :: antso2 !<
  real  ,  dimension(ilg,ilev)  :: antho2 !<
  real  ,  dimension(ilg,ilev)  :: antso4 !<
  real  ,  dimension(ilg,ilev)  :: aoh2o2 !<
  real  ,  dimension(ilg,ilev)  :: aresid !<
  real  ,  dimension(ilg,ilev)  :: wrk1 !<
  real  ,  dimension(ilg,ilev)  :: wrk2 !<
  real  ,  dimension(ilg,ilev)  :: wrk3 !<
  integer, dimension(ilg)       :: ilwcp !<
  integer, dimension(ilg)       :: ichem !<
  !
  real, parameter :: atune=0.7 !<
  !
  real, parameter :: ytau = 1800.
  real, parameter :: zcthr = .99
  real, parameter :: ysmall = 9.e-30
  !
  real, parameter :: one = 1.
  real, parameter :: plarge = 1.e+02
  real, parameter :: zero = 0.
  real, parameter :: zfarr1 = 8.e+04
  real, parameter :: zfarr2 = -3650.
  real, parameter :: zfarr3 = 9.7e+04
  real, parameter :: zfarr4 = 6600.
  !=======================================================================
  !     * define function for changing the units
  !     * from mass-mixing ratio to molecules per cm**3 and vice versa
  !
  real :: xtoc
  real :: ctox
  xtoc(x,y)=x*6.022e+20/y
  ctox(x,y)=y/(6.022e+20*x)
  !
  !     * x = density of air, y = mol weight in gramm
  !-----------------------------------------------------------------------
  !
  !     * constants.
  !
  pcons2=1./(ztmst*grav)
  pqtmst=1./ztmst
  zmin=1.e-20
  asrso4 = 0.9
  asrphob= 0.
  zk2i=2.0e-12
  zk2=4.0e-31
  zk2f=0.45
  zk3=1.9e-13
  zmolgs=32.064
  zmolgair=28.84
  zavo=6.022e+23
  znamair=1.e-03*zavo/zmolgair
  ysphr=3600.
  !
  !     * define constant 2-d slices.
  !
  do jk=1,ilev
    do il=il1,il2
      zrho0(il,jk)=shj(il,jk)*pressg(il)/(rgas*th(il,jk) &
               *(1.+(rgasv/rgas-1.)*qv(il,jk)))
      zrhxtoc(il,jk)=xtoc(zrho0(il,jk),zmolgs)
      zrhctox(il,jk)=ctox(zrho0(il,jk),zmolgs)
      zmtof(il,jk) = pcons2 * dshj(il,jk) * pressg(il)
    end do
  end do ! loop 100
  !
  !     * oxidant concentrations.
  !
  do jk=1,ilev
    do il=il1,il2
      !
      !       * convert to molecules/cm3 from vmr
      !
      factcon = zrho0(il,jk)*1.e-03*zavo/zmolgair
      zzoh  (il,jk)=ohrow  (il,jk)*factcon*atune  ! tuning to reduce so4
      zzh2o2(il,jk)=h2o2row(il,jk)*factcon
      zzo3  (il,jk)=o3row  (il,jk)*factcon
      zzno3 (il,jk)=no3row (il,jk)*factcon
      !
      !       * oxygen concentration in molecules/cm3
      !
      zzo2 (il,jk)=0.21*1.e-06*factcon
    end do
  end do
  sgpp=0.
  !
  !     * background aging time scale for carbonaceous aerosol.
  !
  if (nage==0) then
    atau=36.*ysphr
  else
    atau=12.*ysphr
  end if
  !-----------------------------------------------------------------
  !     * hydrogen peroxide production and initialization.
  !
  dox4row = 0.0
  doxdrow = 0.0
  noxdrow = 0.0
  !
  if (kount < 2) return
  !
  do jk=1,ilev
    do il=il1,il2
      !
      !---    convert molecules/cm**3 to kg-s/kg
      !
      agthpo(il,jk) = zzh2o2(il,jk) * 1.e+06 * 32.06e-03 &
                        / (6.022045e+23 * zrho0(il,jk))
      zdep3d(il,jk)=0.
    end do
  end do
  !
  !     * initial hydrogen peroxide mixing ratios for clear and cloudy sky.
  !
  tmpscl(il1:il2,:)=1.
  where (zclf(il1:il2,:) < zcthr &
      .and. (1.-zclf(il1:il2,:)) < zcthr)
    where (sfrc(il1:il2,:,ihpo) < -ysmall)
      tmpscl(il1:il2,:)=-xrow(il1:il2,1:ilev,ihpo) &
                        *(1.-zclf(il1:il2,:))/sfrc(il1:il2,:,ihpo)
    else where (sfrc(il1:il2,:,ihpo) > ysmall)
      tmpscl(il1:il2,:)=xrow(il1:il2,1:ilev,ihpo) &
                             *zclf(il1:il2,:)/sfrc(il1:il2,:,ihpo)
    else where
      tmpscl(il1:il2,:)=0.
    end where
  end where
  tmpscl(il1:il2,:)=max(min(tmpscl(il1:il2,:),1.),0.)
  sfrc(il1:il2,:,ihpo)=tmpscl(il1:il2,:)*sfrc(il1:il2,:,ihpo)
  xarow(il1:il2,1:ilev)=xrow(il1:il2,1:ilev,ihpo)
  xcrow(il1:il2,1:ilev)=xrow(il1:il2,1:ilev,ihpo)
  where (zclf(il1:il2,:) < zcthr &
      .and. (1.-zclf(il1:il2,:)) < zcthr)
    xarow(il1:il2,:)=xarow(il1:il2,:) &
                        +sfrc(il1:il2,:,ihpo)/(1.-zclf(il1:il2,:))
    xcrow(il1:il2,:)=xcrow(il1:il2,:) &
                             -sfrc(il1:il2,:,ihpo)/zclf(il1:il2,:)
  end where
  !
  !     * change in concentration in cloudy and clear portions
  !     * of the grid cell by adjusting concentrations towards background
  !     * values.
  !
  dxcdt(il1:il2,:)=-(1./max(ytau,ztmst)) &
                             *(xcrow(il1:il2,:)-agthpo(il1:il2,:))
  dxadt(il1:il2,:)=-(1./max(ytau,ztmst)) &
                             *(xarow(il1:il2,:)-agthpo(il1:il2,:))
  xcrow(il1:il2,:)=xcrow(il1:il2,:)+dxcdt(il1:il2,:)*ztmst
  xarow(il1:il2,:)=xarow(il1:il2,:)+dxadt(il1:il2,:)*ztmst
  !
  !     * all-sky change.
  !
  dxdt(il1:il2,:)=zclf(il1:il2,:)*dxcdt(il1:il2,:) &
                            +(1.-zclf(il1:il2,:))*dxadt(il1:il2,:)
  xrow(il1:il2,1:ilev,ihpo)=xrow(il1:il2,1:ilev,ihpo) &
                                            +dxdt(il1:il2,:)*ztmst
  !
  !     * adjust clear/cloudy concentration difference to account
  !     * for production of h2o2 in cloudy portion of the grid cell.
  !
  sfrc(il1:il2,:,ihpo)=0.
  where (zclf(il1:il2,:) < zcthr &
      .and. (1.-zclf(il1:il2,:)) < zcthr)
    sfrc(il1:il2,:,ihpo)=zclf(il1:il2,:)*(1.-zclf(il1:il2,:)) &
                              *(xarow(il1:il2,:)-xcrow(il1:il2,:))
  end where
  !
  do jt=1,ntrac
    do jk=1,ilev
      do il=il1,il2
        zxte(il,jk,jt)=0.
      end do
    end do
  end do
  !
  do jk=1,ilev
    do il=il1,il2
      zhenry(il,jk)=0.
      if (zclf(il,jk)<1.e-04.or.zmlwc(il,jk)<zmin) then
        zmratep(il,jk)=0.
        zmlwc(il,jk)=0.
      end if
    end do
  end do
  !
  !     * processes which are diferent inside and outside of clouds.
  !
  do jt=1,ntrac
    if (modl_tracers(jt)%wet/=0 .and. jt/=iso2 .and. jt/=ihpo &
        .and. (jt<iaindt .or. jt>=(iaindt+ntracp))) then
      do jk=1,ilev
        do il=il1,il2
          !
          !          * diagnose initial mixing ratios for clear and cloudy sky.
          !
          tmpscl(il,jk)=1.
          if (zclf(il,jk) < zcthr .and. (1.-zclf(il,jk)) < zcthr) then
            if (sfrc(il,jk,jt) < -ysmall) then
              tmpscl(il,jk)=-xrow(il,jk,jt) &
                                  *(1.-zclf(il,jk))/sfrc(il,jk,jt)
            else if (sfrc(il,jk,jt) > ysmall) then
              tmpscl(il,jk)=xrow(il,jk,jt) &
                                       *zclf(il,jk)/sfrc(il,jk,jt)
            else
              tmpscl(il,jk)=0.
            end if
          end if
          tmpscl(il,jk)=max(min(tmpscl(il,jk),1.),0.)
          sfrc(il,jk,jt)=tmpscl(il,jk)*sfrc(il,jk,jt)
          zxtp10(il,jk)=xrow(il,jk,jt)
          zxtp1c(il,jk)=xrow(il,jk,jt)
          if (zclf(il,jk) < zcthr .and. (1.-zclf(il,jk)) < zcthr) then
            zxtp10(il,jk)=zxtp10(il,jk) &
                        +sfrc(il,jk,jt)/(1.-zclf(il,jk))
            zxtp1c(il,jk)=zxtp1c(il,jk) &
                             -sfrc(il,jk,jt)/zclf(il,jk)
          end if
          xcld(il,jk,jt)=zxtp1c(il,jk)
          xclr(il,jk,jt)=zxtp10(il,jk)
          zdep3d(il,jk)=0.
        end do
      end do
      !
      if (modl_tracers(jt)%wet==1) then
        do jk=1,ilev
          do il=il1,il2
            zhenry(il,jk)=asrso4
          end do
        end do
      else
        do jk=1,ilev
          do il=il1,il2
            zhenry(il,jk)=asrphob
          end do
        end do
      end if
      !
      iso4=0
      call wetdep4(ilg,il1,il2,ilev, zxtp10, &
                     zxtp1c,zmtof,zrho0,zmratep,zmlwc, &
                     zfsnow,zfrain,zdep3d,zclf,clrfr,zhenry, &
                     clrfs,zfevap,zfsubl,pdclr,pdcld,jt,iso4)
      do jk=1,ilev
        do il=il1,il2
          zxtp1(il)=(1.-zclf(il,jk))*zxtp10(il,jk)+ &
            zclf(il,jk)*zxtp1c(il,jk)
          zxtp1(il)=zxtp1(il)-zdep3d(il,jk)
          zxte(il,jk,jt)=(zxtp1(il)-xrow(il,jk,jt))*pqtmst
          xcld(il,jk,jt)=zxtp1c(il,jk)-pdcld(il,jk)
          xclr(il,jk,jt)=zxtp10(il,jk)-pdclr(il,jk)
        end do
      end do
      !
    end if
  end do
  !
  jt=iso2
  !
  !     * day-time chemistry.
  !
  do il=il1,il2
    if (cszrow(il)>0. .and. nage==1) atau(il)=1.*ysphr
  end do
  do jk=1,ilev
    do il=il1,il2
      if (cszrow(il)>0.) then
        zxtp1so2=xrow(il,jk,jt)+zxte(il,jk,jt)*ztmst
        if (zxtp1so2<=zmin) then
          zso2=0.
        else
          ztk2=zk2*(th(il,jk)/300.)**(-3.3)
          zm=zrho0(il,jk)*znamair
          zhil=ztk2*zm/zk2i
          zexp=log10(zhil)
          zexp=1./(1.+zexp*zexp)
          ztk23b=ztk2*zm/(1.+zhil)*zk2f**zexp
          zso2=zxtp1so2*zrhxtoc(il,jk)*zzoh(il,jk)*ztk23b*zdayl(il)
          zso2=zso2*zrhctox(il,jk)
          zso2=min(zso2,zxtp1so2*pqtmst)
          zxte(il,jk,jt)=zxte(il,jk,jt)-zso2
        end if
        sgpp(il,jk)=zso2
        !
        zxtp1dms=xrow(il,jk,idms)+ zxte(il,jk,idms)*ztmst
        if (zxtp1dms<=zmin) then
          zdms=0.
        else
          t=th(il,jk)
          !          ztk1=(t*exp(-234./t)+8.46e-10*exp(7230./t)+
          !     1         2.68e-10*exp(7810./t))/(1.04e+11*t+88.1*exp(7460./t))
          !
          !         * dms+oh->so2+mo2+ch2o (jpl 2010)
          !
          ztk1=1.2e-11*exp(-280./t)
          !
          !         * dms+oh+o2->0.75so2+0.25msa+mo2 (jpl 2010)
          !
          !          ztka=8.2e-39*exp(5376./t)
          ztka=exp(5376./t-87.69668)
          ztkb=1.05e-05*exp(3644./t)
          ztk2=ztka*zzo2(il,jk)/(1.0+ztkb*0.2095)
          zdms=zxtp1dms*zrhxtoc(il,jk)*zzoh(il,jk)*(ztk1+ztk2)*zdayl(il)
          zdms=zdms*zrhctox(il,jk)
          if (zdms > zxtp1dms*pqtmst) then
            scalf=zxtp1dms*pqtmst/zdms
            zdms=scalf*zdms
          end if
          zxte(il,jk,idms)=zxte(il,jk,idms)-zdms
          zxte(il,jk,jt)=zxte(il,jk,jt)+0.75*zdms
          sgpp(il,jk)=sgpp(il,jk)+0.25*zdms
        end if
        !
        if (lsvchem) then
          dox4row(il)=dox4row(il)+zso2*dshj(il,jk)*pressg(il)/grav
          doxdrow(il)=doxdrow(il)+zdms*dshj(il,jk)*pressg(il)/grav
        end if
      end if
    end do ! loop 410
  end do ! loop 412
  !
  !     * night-time chemistry.
  !
  do il=il1,il2
    if (cszrow(il)<=0. .and. nage==1) atau(il)=24.*ysphr
  end do
  do jk=1,ilev
    do il=il1,il2
      if (cszrow(il)<=0.) then
        zxtp1dms=xrow(il,jk,idms)+ zxte(il,jk,idms)*ztmst
        if (zxtp1dms<=zmin) then
          zdms=0.
        else
          !
          !          *  dms+no3->so2+hno3+mo2+ch2o (jpl 2010)
          !
          !           ztk3=zk3*exp(520./th(il,jk))
          ztk3=1.9e-13*exp(530./th(il,jk))
          zdms=zxtp1dms*zrhxtoc(il,jk)*zzno3(il,jk)*ztk3
          zdms=zdms*zrhctox(il,jk)
          zdms=min(zdms,zxtp1dms*pqtmst)
          zxte(il,jk,idms)=zxte(il,jk,idms)-zdms
          zxte(il,jk,jt)=zxte(il,jk,jt)+zdms
          if (lsvchem) then
            noxdrow(il)=noxdrow(il)+zdms*dshj(il,jk)*pressg(il)/grav
          end if
        end if
      end if
    end do
  end do
  !
  !     * fraction of tracers in cloudy part of the grid cell.
  !
  do jt=1,ntrac
    do jk=1,ilev
      do il=il1,il2
        if (jt<iaindt .or. jt>=(iaindt+ntracp) ) then
          xrow(il,jk,jt)=xrow(il,jk,jt)+zxte(il,jk,jt)*ztmst
          xrow(il,jk,jt)=max(xrow(il,jk,jt),zero)
          if (jt /= iso2 .and. jt /= ihpo) then
            if (modl_tracers(jt)%wet/=0 .and. xrow(il,jk,jt) > zmin &
                .and. zclf(il,jk) > 1.e-02) then
              sfrc(il,jk,jt)=0.
              if (zclf(il,jk) < zcthr &
                  .and. (1.-zclf(il,jk)) < zcthr) then
                sfrc(il,jk,jt)=zclf(il,jk)*(1.-zclf(il,jk)) &
                              *(xclr(il,jk,jt)-xcld(il,jk,jt))
              end if
            else
              sfrc(il,jk,jt)=0.
            end if
          end if
        end if
      end do
    end do
  end do
  !
  return
end subroutine xtchempam
