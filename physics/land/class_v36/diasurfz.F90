subroutine diasurfz(uz,vz,tz,qz,ni,u,v,tg,qg,z0,z0t,ilmo,za, &
                          h,ue,ftemp,fvap,zu,zt,lat,f,il1,il2,jl)
                          
  use hydcon, only : delta, grav, vkc, cpres, as, asx, angmax, ci, beta, factn, hmin
  implicit none
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ni !<
  integer, intent(in) :: jl !<
  real, intent(in) :: zt(ni) !<
  real, intent(in) :: zu(ni) !<
  real, intent(inout) :: uz(ni) !<
  real, intent(inout) :: vz(ni) !<
  real, intent(inout) :: tz(ni) !<
  real, intent(inout) :: qz(ni) !<
  real, intent(in) :: za(ni) !<
  real, intent(in) :: u(ni) !<
  real, intent(in) :: v(ni) !<
  real, intent(in) :: tg(ni) !<
  real, intent(in) :: qg(ni) !<
  real, intent(in) :: ue(ni) !<
  real, intent(in) :: ftemp(ni) !<
  real, intent(in) :: fvap(ni) !<
  real, intent(in) :: ilmo(ni) !<
  real, intent(in) :: z0t(ni) !<
  real, intent(in) :: z0(ni) !<
  real, intent(in) :: h(ni) !<
  real, intent(in) :: f(ni) !<
  real, intent(in), dimension(ni) :: lat !<
  ! author
  !          yves delage  (aug1990)
  !
  ! revision
  ! 001      g. pellerin(jun94)
  !          adaptation to new surface formulation
  ! 002      b. bilodeau (nov 95) - replace vk by karman
  ! 003      r. sarrazin (jan 96) - prevent problems if zu < za
  ! 004      g. pellerin (feb 96) - rewrite stable formulation
  ! 005      y. delage and b. bilodeau (jul 97) - cleanup
  ! 006      y. delage (feb 98) - addition of hmin
  ! 007      y. delage (sept 00) - change ue2 by ue
  !                              - introduce log-linear profile for near-
  !                                 neutral cases
  ! 008      d. verseghy (nov 02) - remove unused constant clm
  !                                 from common block classd2
  ! 009      m. mackay (nov 04) - change all occurrences of alog
  !                               to log for greater portability.
  ! 010      f. seglenieks (mar 05) - declare lat as real*8 :: for
  !                                   consistency
  ! 011      p.bartlett (mar 06) - set hi to zero for unstable case
  ! 012      e.chan (nov 06) - bracket entire subroutine loop with
  !                            if (f(j)>0.)
  ! 013      d.verseghy (nov 06) - convert lat to regular precision
  ! 014      b.dugas (jan 09) - "Synchronization" with diasurf2
  !
  ! object
  !          to calculate the diagnostic values of u, v, t, q
  !          near the surface (zu and zt)
  !
  ! arguments
  !
  !          - output -
  ! uz       u component of the wind at z=zu
  ! vz       v component of the wind at z=zu
  ! tz       temperature in kelvins at z=zt
  ! qz       specific humidity at z=zt
  !
  !          - input -
  ! ni       number of points to process
  ! u        u component of wind at z=za
  ! v        v component of wind at z=za
  ! tg       temperature at the surface (z=0) in kelvins
  ! qg       specific humidity
  ! ps       surface pressure at the surface
  ! ilmo     inverse of monin-obukhov lenth
  ! h        height of boundary layer
  ! ue       friction velocity
  ! z0       roughness lenth for winds
  ! z0t      roughness lenth for temperature and moisture
  ! ftemp    temperature flux at surface
  ! fvap     vapor flux at surface
  ! za       heights of first model level above ground
  ! zu       heights for computation of wind components
  ! zt       heights for computation of temperature and moisture
  ! lat      latitude
  ! f        fraction of surface type being studied

  real :: ang !<
  real :: angi !<
  real :: vits !<
  real :: lzz0 !<
  real :: lzz0t !<
  real :: ct !<
  real :: dang !<
  real :: cm !<
  real :: xx !<
  real :: xx0 !<
  real :: yy !<
  real :: yy0 !<
  real :: fh !<
  real :: fm !<
  real :: hi !<
  real :: rac3 !<
  integer :: j !<
  !

  rac3=sqrt(3.)

  do j=il1,il2
    if (f(j)>0.0) then

      lzz0t=log((zt(j)+z0(j))/z0t(j))
      lzz0=log(zu(j)/z0(j)+1)
      if (ilmo(j)<=0.) then
        !---------------------------------------------------------------------
        !                      unstable case
        !
        hi=0.
        ! cdir iexpand
        fh=fhi(zt(j)+z0(j),z0t(j),lzz0t,ilmo(j),yy,yy0)
        ! cdir iexpand
        fm=fmi(zu(j)+z0(j),z0 (j),lzz0 ,ilmo(j),xx,xx0)
      else
        !---------------------------------------------------------------------
        !                        stable case
        hi=1/max(hmin,h(j),(za(j)+10*z0(j))*factn,factn/ &
         (4*as*beta*ilmo(j)))
        ! cdir iexpand
        fh=beta*(lzz0t+min( psi(zt(j)+z0(j),hi,ilmo(j)) &
                       -psi(z0t(j),hi,ilmo(j)), &
                        asx*ilmo(j)*(zt(j)+z0(j)-z0t(j)) &
                      ) &
            )
        ! cdir iexpand
        fm=lzz0+min( psi(zu(j)+z0(j),hi,ilmo(j)) &
                -psi(z0(j),hi,ilmo(j)), &
                 asx*ilmo(j)*zu(j) &
               )
      end if
      !---------------------------------------------------------------------
      ct=vkc/fh
      cm=vkc/fm
      tz(j)=tz(j)+f(j)*(tg(j)-ftemp(j)/(ct*ue(j))-grav/cpres*zt(j))
      qz(j)=qz(j)+f(j)*(qg(j)-fvap(j)/(ct*ue(j)))
      vits=ue(j)/cm

      ! calculate wind direction change from top of surface layer
      dang= (za(j)-zu(j))*hi*angmax*sin(lat(j))
      angi=atan2(v(j),sign(abs(u(j))+1.e-05,u(j)))
      if (ilmo(j)>0.) then
        ang=angi+dang
      else
        ang=angi
      end if

      uz(j)=uz(j)+f(j)*vits*cos(ang)
      vz(j)=vz(j)+f(j)*vits*sin(ang)

    end if
  end do

  return
contains

  !   the following code is taken from the rpn/cmc physics library file
  !   /usr/local/env/armnlib/modeles/phy_shared/ops/v_4.5/rcs/stabfunc2.cdk,v

  !   internal function fmi
  !   stability function for momentum in the unstable regime (ilmo<0)
  !   reference: delage y. and girard c. blm 58 (19-31) eq. 19
  !
  real function fmi(z2,z02,lzz02,ilmo2,x,x0)
    implicit none
    !
    real, intent(in) :: z2 !<
    real, intent(in) :: z02 !<
    real, intent(in) :: lzz02 !<
    real, intent(in) :: ilmo2 !<
    real, intent(out) :: x !<
    real, intent(out) :: x0 !<
    !
    x =(1-ci*z2 *beta*ilmo2)**(0.16666666)
    x0=(1-ci*z02*beta*ilmo2)**(0.16666666)
    fmi=lzz02+log((x0+1)**2*sqrt(x0**2-x0+1)*(x0**2+x0+1)**1.5 &
                /((x+1)**2*sqrt(x**2-x+1)*(x**2+x+1)**1.5)) &
               +rac3*atan(rac3*((x**2-1)*x0-(x0**2-1)*x)/ &
               ((x0**2-1)*(x**2-1)+3*x*x0))
    !
    return
  end function fmi
  !
  !   internal function fhi
  !   stability function for heat and moisture in the unstable regime (ilmo<0)
  !   reference: delage y. and girard c. blm 58 (19-31) eq. 17
  !
  real function fhi(z2,z0t2,lzz0t2,ilmo2,y,y0)
    implicit none
    !
    real, intent(in) :: z2 !<
    real, intent(in) :: z0t2 !<
    real, intent(in) :: lzz0t2 !<
    real, intent(in) :: ilmo2 !<
    real, intent(out) :: y !<
    real, intent(out) :: y0 !<
    !
    y =(1-ci*z2  *beta*ilmo2)**(0.33333333)
    y0=(1-ci*z0t2*beta*ilmo2)**(0.33333333)
    fhi=beta*(lzz0t2+1.5*log((y0**2+y0+1)/(y**2+y+1))+rac3* &
         atan(rac3*2*(y-y0)/((2*y0+1)*(2*y+1)+3)))
    !
    return
  end function fhi
  !
  !   internal function psi
  !   stability function for momentum in the stable regime (unsl>0)
  !   reference :  y. delage, blm, 82 (p23-48) (eqs.33-37)
  !
  real function psi(z2,hi2,ilmo2)
    implicit none
    !
    real :: a !<
    real :: b !<
    real :: c !<
    real :: d !<
    real, intent(in) :: ilmo2 !<
    real, intent(in) :: z2 !<
    real, intent(in) :: hi2 !<
    !
    d = 4*as*beta*ilmo2
    c = d*hi2 - hi2**2
    b = d - 2*hi2
    a = sqrt(1 + b*z2 - c*z2**2)
    psi = 0.5 * (a-z2*hi2-log(1+b*z2*0.5+a)- &
            b/(2*sqrt(c))*asin((b-2*c*z2)/d))
    !
    return
  end function psi
end
