subroutine snoadd(albsno,tsnow,rhosno,zsnow,hcpsno,htcs, &
                        fi,s,ts,rhosni,wsnow,ilg,il1,il2,jl)
  !
  !     * nov 17/11 - m.lazare.   change snow albedo refreshment
  !     *                         threshold (snowfall in current
  !     *                         timestep) from 0.005 to 1.e-4 m.
  !     * mar 24/06 - d.verseghy. allow for presence of water in snow.
  !     * sep 10/04 - r.harvey/d.verseghy. increase snow albedo
  !     *                         refreshment threshold; add
  !     *                         "IMPLICIT NONE" command.
  !     * jul 26/02 - d.verseghy. change rhosni from constant to
  !     *                         variable.
  !     * jun 20/97 - d.verseghy. class - version 2.7.
  !     *                         pass in new "CLASS4" common block.
  !     * jan 02/96 - d.verseghy. class - version 2.5.
  !     *                         completion of energy balance
  !     *                         diagnostics.
  !     * jul 30/93 - d.verseghy/m.lazare. class - version 2.2.
  !     *                                  new diagnostic fields.
  !     * apr 24/92 - d.verseghy/m.lazare. class - version 2.1.
  !     *                                  revised and vectorized code
  !     *                                  for model version gcm7.
  !     * aug 12/91 - d.verseghy. code for model version gcm7u -
  !     *                         class version 2.0 (with canopy).
  !     * apr 11/89 - d.verseghy. accumulation of snow on ground.
  !
  use times_mod, only : delt
  use hydcon, only : tfrez, hcpw, hcpice, rhow, rhoice
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: jl !<
  !
  !     * input/output arrays.
  !
  real, intent(inout) :: albsno(ilg) !<
  real, intent(inout) :: tsnow (ilg) !<
  real, intent(inout) :: rhosno(ilg) !<
  real, intent(inout) :: zsnow (ilg) !<
  real, intent(inout) :: hcpsno(ilg) !<
  real, intent(inout) :: htcs  (ilg) !<
  !
  !     * input arrays.
  !
  real, intent(in) :: fi    (ilg) !<
  real, intent(in) :: s     (ilg) !<
  real, intent(in) :: ts    (ilg) !<
  real, intent(in) :: rhosni(ilg) !<
  real, intent(in) :: wsnow (ilg) !<

  !     * temporary variables.
  !
  integer :: i !<
  real :: snofal !<
  real :: hcpsnp !<
  !-----------------------------------------------------------------------
  do i=il1,il2
    if (fi(i)>0. .and. s(i)>0.) then
      htcs  (i)=htcs(i)-fi(i)*hcpsno(i)*(tsnow(i)+tfrez)* &
                   zsnow(i)/delt
      snofal=s(i)*delt
      if (snofal>=1.e-4) then
        albsno(i)=0.84
      else if (.not.(zsnow(i)>0.)) then
        albsno(i)=0.50
      end if
      hcpsnp=hcpice*rhosni(i)/rhoice
      tsnow (i)=((tsnow(i)+tfrez)*zsnow(i)*hcpsno(i) + &
                    (ts   (i)+tfrez)*snofal  *hcpsnp)/ &
                   (zsnow(i)*hcpsno(i) + snofal*hcpsnp) - &
                    tfrez
      rhosno(i)=(zsnow(i)*rhosno(i) + snofal*rhosni(i))/ &
                   (zsnow(i)+snofal)
      zsnow (i)=zsnow(i)+snofal
      hcpsno(i)=hcpice*rhosno(i)/rhoice+hcpw*wsnow(i)/ &
                   (rhow*zsnow(i))
      htcs  (i)=htcs(i)+fi(i)*hcpsno(i)*(tsnow(i)+tfrez)* &
                   zsnow(i)/delt
    end if
  end do ! loop 100
  !
  return
end subroutine snoadd
