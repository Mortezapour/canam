!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine lakez (istep,  ctlstp, ctsstp, wtsstp, &
                  fsgl,   flgl,   hfsl,   hevl,   hmfl,   htcl, &
                  t0,     tlak,   lkiceh, nlak, &
                  fsgs,   flgs,   hfss,   hevs,   hmfn,   htcs, &
                  pcpn,   qfn,    rofn,   sno,    tsnow,  wsnow, &
                  il1,    il2,    ilg,    n,      nlakmax)
  !
  !     * jan 21/17 - d.verseghy. finalized for cslm2. for now, comment
  !     *                         out lake energy balance check; closure
  !     *                         difficult due to large lake mass.
  !     * sep 01/15 - d.verseghy. add htcl to lake energy balance.
  !     * aug 31/15 - d.verseghy. add snow energy and water balance
  !     *                         checks.
  !     * jan 14/13 - m.mackay    energy balance check over lake tiles.
  !
  use times_mod, only : delt
  use hydcon, only : tfrez, delskin, delzlk, hcpw, hcpice, rhow, rhoice
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: istep !< Variable description\f$[units]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: n !< Variable description\f$[units]\f$
  !
  !     * diagnostic arrays.
  !
  real, intent(inout), dimension(ilg) :: ctlstp !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: ctsstp !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: wtsstp !< Variable description\f$[units]\f$
  !
  !     * lake input arrays.
  !
  integer, intent(in) :: nlakmax
  real, intent(in),dimension(ilg,nlakmax) :: tlak !< Variable description\f$[units]\f$
  integer, intent(in),dimension(ilg) :: nlak !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: fsgl !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: flgl !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: hfsl !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: hevl !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: hmfl !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: htcl !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: lkiceh !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: t0 !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: fsgs !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: flgs !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: hfss !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: hevs !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: hmfn !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: htcs !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: pcpn !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: qfn !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: rofn !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: sno !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: tsnow !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: wsnow !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  integer :: i !< Variable description\f$[units]\f$
  integer :: j !< Variable description\f$[units]\f$
  real :: z
  real :: ztop
  real :: zbot
  real :: hcap
  real :: hcap0
  real :: qsuml
  real :: qsums
  real :: wsums
  !
  !----------------------------------------------------------------------
  !
  if (istep == 0) then
    !
    !     * set balance check variable for start of current time step.
    !
    do i = il1,il2
      if (lkiceh(i) >= delskin) then
        hcap0 = hcpice
      else if (lkiceh(i) <= 0.0) then
        hcap0 = hcpw
      else
        hcap0 = (lkiceh(i) * hcpice + (delskin - lkiceh(i)) * hcpw)/delskin
      end if
      if (n == 1) then
        ctlstp(i) = - hcap0 * tlak(i,1) * delskin
      else
        ctlstp(i) = - hcap0 * t0(i) * delskin
      end if
      !
      do j = 1,nlak(i)
        ztop = delskin + delzlk * (j - 1)
        zbot = delskin + delzlk * j
        if (lkiceh(i) >= zbot) then
          hcap = hcpice
        else if (lkiceh(i) <= ztop) then
          hcap = hcpw
        else
          z = lkiceh(i) - ztop
          hcap = (z * hcpice + (delzlk - z) * hcpw)/delzlk
        end if
        ctlstp(i) = ctlstp(i) - hcap * tlak(i,j) * delzlk
      end do ! loop 50
      ctsstp(i) = - tsnow(i) * (hcpice * sno(i)/rhoice + &
                  hcpw * wsnow(i)/rhow)
      wtsstp(i) = - sno(i) - wsnow(i)
    end do ! loop 100
    !
  end if
  !
  if (istep == 1) then
    !
    !     * check energy balance over the current time step.
    !
    do i = il1,il2
      if (lkiceh(i) >= delskin) then
        hcap0 = hcpice
      else if (lkiceh(i) <= 0.0) then
        hcap0 = hcpw
      else
        hcap0 = (lkiceh(i) * hcpice + (delskin - lkiceh(i)) * hcpw)/delskin
      end if
      ctlstp(i) = ctlstp(i) + hcap0 * t0(i) * delskin
      do j = 1,nlak(i)
        ztop = delskin + delzlk * (j - 1)
        zbot = delskin + delzlk * j
        if (lkiceh(i) >= zbot) then
          hcap = hcpice
        else if (lkiceh(i) <= ztop) then
          hcap = hcpw
        else
          z = lkiceh(i) - ztop
          hcap = (z * hcpice + (delzlk - z) * hcpw)/delzlk
        end if
        ctlstp(i) = ctlstp(i) + hcap * tlak(i,j) * delzlk
      end do ! loop 200
      ctlstp(i) = ctlstp(i)/delt
      ctsstp(i) = ctsstp(i) + tsnow(i) * (hcpice * sno(i)/rhoice + &
                  hcpw * wsnow(i)/rhow)
      ctsstp(i) = ctsstp(i)/delt
      wtsstp(i) = wtsstp(i) + sno(i) + wsnow(i)
    end do ! loop 300
    !
    do i = il1,il2
      qsuml = fsgl(i) + flgl(i) - hfsl(i) - hevl(i) - hmfl(i) + htcl(i)
      qsums = fsgs(i) + flgs(i) - hfss(i) - hevs(i) - hmfn(i) + htcs(i)
      wsums = (pcpn(i) - qfn(i) - rofn(i)) * delt

      !          if (abs(ctlstp(i)-qsuml)>= 20.0) then
      !              write(6,6001) n,i,ctlstp(i),qsuml
      ! 6001          FORMAT(2X,'LAKE ENERGY BALANCE  ',2I8,2F20.8)
      !              write(6,6002) nlak(i),fsgl(i),flgl(i),hfsl(i),
      !     1             hevl(i),hmfl(i),htcl(i),lkiceh(i),t0(i)
      ! 6002          format(2x,i6,8f15.6)
      ! c              stop
      !          end if
      if (abs(ctsstp(i) - qsums) > 5.0) then
        write(6,6442) n,i,ctsstp(i),qsums
6442    format(2x,'LAKE SNOW ENERGY BALANCE  ',2i8,2f20.8)
        write(6,6450) fsgs(i),flgs(i),hfss(i), &
             hevs(i),hmfn(i),htcs(i)
        write(6,6450) sno(i),wsnow(i),tsnow(i) - tfrez
        !              stop
      end if
      if (abs(wtsstp(i) - wsums) > 1.0e-2) then
        write(6,6447) n,i,wtsstp(i),wsums
6447    format(2x,'LAKE SNOW WATER BALANCE  ',2i8,2f20.8)
        write(6,6450) pcpn(i) * delt,qfn(i) * delt, &
             rofn(i) * delt
        write(6,6450) sno(i),wsnow(i),tsnow(i) - tfrez
        !              stop
      end if
    end do ! loop 400
6450 format(2x,7f15.6)
    !
  end if
  !
  return
end subroutine lakez
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
