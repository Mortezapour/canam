A example Jupyter notebook to check that the results from for the "rt_full_global_gcm" setup, a global snapshot from CanESM5, are similar to a reference output can be found here:

https://drive.google.com/file/d/1mMtn8CKfOJ91KyoiXjMNjKw7zKC0fjkx/view?usp=sharing

