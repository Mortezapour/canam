!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine getch(ebocpak,ebocpal,ebbcpak,ebbcpal, &
                 eaocpak,eaocpal,eabcpak,eabcpal, &
                 easlpak,easlpal,eashpak,eashpal, &
                 nlon,nlat,incd,irefyr,iday,kount,ijpak,nuan,gg)
  !
  !     * april 17, 2015 - m.lazare.     cosmetic revision (same name kept)
  !     *                                to have "keeptim" common block
  !     *                                definition line in upper case,
  !     *                                so that "myrssti" does not conflict
  !     *                                with the cpp directive (compiler
  !     *                                warnings generated).
  !     * knut von salzen - feb 07,2009. new routine for gcm15h to read in
  !     *                                ebbc,eobc,eabc,eaoc,easl,eash.
  !
  implicit none
  integer, intent(in) :: iday  !< Julian calendar day of year \f$[day]\f$
  integer, intent(in) :: ijpak
  integer, intent(in) :: incd
  integer, intent(in) :: irefyr
  integer, intent(in) :: kount  !< Current model timestep \f$[unitless]\f$
  integer, intent(in) :: nlat
  integer, intent(in) :: nlon
  integer, intent(in) :: nuan
  !
  !     * surface emissions/concentrations.
  !
  real, intent(inout), dimension(ijpak) :: ebocpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: ebocpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: ebbcpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: ebbcpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: eaocpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: eaocpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: eabcpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: eabcpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: easlpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: easlpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: eashpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: eashpal !< Variable description\f$[units]\f$

  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  real :: day1
  real :: day2
  real :: day3
  integer :: i
  integer :: imdh
  integer :: isavdts
  integer :: iyear
  integer :: myrssti
  real :: w1
  real :: w2
  !
  !     * work space
  !
  real, intent(in) :: gg( * ) !< Variable description\f$[units]\f$

  !--- mynode used to control i/o to stdout
  integer*4 :: mynode
  common /mpinfo/ mynode

  !--- keeptime is required for iyear
  common /keeptim/ iyear,imdh,myrssti,isavdts

  !--- local
  integer :: year_prev
  integer :: year_next
  integer :: mon_prev
  integer :: mon_next
  integer :: ib2p
  integer :: ib2n
  integer :: curr_year
  integer, external :: nc4to8

  !--- a list containing mid month days of the year
  integer, parameter, dimension(12) :: mm_doy = (/ 16,46,75,106,136,167,197,228,259,289,320,350 /)

  !--- a list containing mid month days for each month
  integer, parameter, dimension(12) :: mm_dom = (/ 16,14,16,15,16,15,16,16,15,16,15,16 /)
  !----------------------------------------------------------------------
  !
  if (mynode == 0) then
    write(6, * )'    GETCH: NEW CHEMISTRY FORCING REQUIRED'
  end if

  !.....determine the year for which data is required
  if (irefyr > 0) then
    !--- when irefyr > 0 read emissions from the data file for
    !--- a repeated annual cycle of year irefyr
    curr_year = irefyr
  else
    !--- otherwise read time dependent emissions from the data
    !--- file using iyear to determine the current year
    curr_year = iyear
  end if

  !.....check on iday
  if (iday < 1 .or. iday > 365) then
    if (mynode == 0) then
      write(6, * )'GETCH: IDAY is out of range. IDAY = ',iday
    end if
    call xit("GETCH", - 1)
  end if

  !.....determine previous and next year/month, relative to iday
  mon_prev = 0
  if (iday >= 1 .and. iday < mm_doy(1)) then
    year_prev = curr_year - 1
    year_next = curr_year
    mon_prev = 12
    mon_next = 1
  else if (iday >= mm_doy(12) .and. iday <= 365) then
    year_prev = curr_year
    year_next = curr_year + 1
    mon_prev = 12
    mon_next = 1
  else
    do i = 2,12
      if (iday >= mm_doy(i - 1) .and. iday < mm_doy(i)) then
        year_prev = curr_year
        year_next = curr_year
        mon_prev = i - 1
        mon_next = i
        exit
      end if
    end do
  end if

  if (mon_prev == 0) then
    if (mynode == 0) then
      write(6, * )'GETCH: Problem setting year/month on IDAY = ',iday
    end if
    call xit("GETCH", - 2)
  end if

  ! xxx
  if (year_prev == 1849 .and. mon_prev == 12) then
    year_prev = 1850
    mon_prev = 12
  end if
  if (year_next == 2001 .and. mon_next == 1) then
    year_next = 2000
    mon_next = 1
  end if
  ! xxx

  !--- determine time stamps (ibuf2 values) to be used to read
  !--- appropriate data from the file
  ! ib2p=1000000*year_prev+10000*mon_prev+100*mm_dom(mon_prev)
  ! ib2n=1000000*year_next+10000*mon_next+100*mm_dom(mon_next)
  ib2p = year_prev * 100 + mon_prev
  ib2n = year_next * 100 + mon_next

  if (mynode == 0) then
    write(6, * )'GETCH: year_prev,mon_prev,year_next,mon_next: ', &
                     year_prev,mon_prev,year_next,mon_next
    write(6, * )'GETCH: ib2p,ib2n: ',ib2p,ib2n
  end if
  !
  !     * get new boundary fields for next mid-month day.
  !
  if (incd == 0) then
    !
    !       * model is stationary.
    !
    if (kount == 0) then
      !
      !         * start-up time. read-in fields for average of month.
      !         * initialize target fields as well, although not used.
      !
      call getagbx(ebocpak,nc4to8("EBOC"),nuan,nlon,nlat,ib2n,gg)
      call getagbx(ebocpal,nc4to8("EBOC"),nuan,nlon,nlat,ib2n,gg)
      call getagbx(ebbcpak,nc4to8("EBBC"),nuan,nlon,nlat,ib2n,gg)
      call getagbx(ebbcpal,nc4to8("EBBC"),nuan,nlon,nlat,ib2n,gg)
      call getagbx(eaocpak,nc4to8("EAOC"),nuan,nlon,nlat,ib2n,gg)
      call getagbx(eaocpal,nc4to8("EAOC"),nuan,nlon,nlat,ib2n,gg)
      call getagbx(eabcpak,nc4to8("EABC"),nuan,nlon,nlat,ib2n,gg)
      call getagbx(eabcpal,nc4to8("EABC"),nuan,nlon,nlat,ib2n,gg)
      call getagbx(easlpak,nc4to8("EASL"),nuan,nlon,nlat,ib2n,gg)
      call getagbx(easlpal,nc4to8("EASL"),nuan,nlon,nlat,ib2n,gg)
      call getagbx(eashpak,nc4to8("EASH"),nuan,nlon,nlat,ib2n,gg)
      call getagbx(eashpal,nc4to8("EASH"),nuan,nlon,nlat,ib2n,gg)
    end if
    !
  else
    !
    !       * the model is moving.
    !
    if (kount == 0) then
      !
      !         * start-up time. get fields for previous and target mid-month days.
      !
      rewind nuan
      call getggbx(ebocpak,nc4to8("EBOC"),nuan,nlon,nlat,ib2p,1,gg)
      call getggbx(ebbcpak,nc4to8("EBBC"),nuan,nlon,nlat,ib2p,1,gg)
      call getggbx(eaocpak,nc4to8("EAOC"),nuan,nlon,nlat,ib2p,1,gg)
      call getggbx(eabcpak,nc4to8("EABC"),nuan,nlon,nlat,ib2p,1,gg)
      call getggbx(easlpak,nc4to8("EASL"),nuan,nlon,nlat,ib2p,1,gg)
      call getggbx(eashpak,nc4to8("EASH"),nuan,nlon,nlat,ib2p,1,gg)
      !
      rewind nuan
      call getggbx(ebocpal,nc4to8("EBOC"),nuan,nlon,nlat,ib2n,1,gg)
      call getggbx(ebbcpal,nc4to8("EBBC"),nuan,nlon,nlat,ib2n,1,gg)
      call getggbx(eaocpal,nc4to8("EAOC"),nuan,nlon,nlat,ib2n,1,gg)
      call getggbx(eabcpal,nc4to8("EABC"),nuan,nlon,nlat,ib2n,1,gg)
      call getggbx(easlpal,nc4to8("EASL"),nuan,nlon,nlat,ib2n,1,gg)
      call getggbx(eashpal,nc4to8("EASH"),nuan,nlon,nlat,ib2n,1,gg)
      !
      !--- interpolate to current day
      day1 = real(mm_doy(mon_prev))
      day2 = real(iday)
      day3 = real(mm_doy(mon_next))
      if (day2 < day1) day2 = day2 + 365.
      if (day3 < day2) day3 = day3 + 365.
      w1 = (day2 - day1)/(day3 - day1)
      w2 = (day3 - day2)/(day3 - day1)
      if (mynode == 0) then
        write(6, * ) &
         'GETCH: Interpolating at ',curr_year,' day',iday, &
         ' between ',year_prev,' day',mm_doy(mon_prev), &
         ' and ',year_next,' day',mm_doy(mon_next), &
         ' using weights ',w1,w2
      end if
      !
      do i = 1,ijpak
        ebocpak(i) = w1 * ebocpal(i) + w2 * ebocpak(i)
        ebbcpak(i) = w1 * ebbcpal(i) + w2 * ebbcpak(i)
        eaocpak(i) = w1 * eaocpal(i) + w2 * eaocpak(i)
        eabcpak(i) = w1 * eabcpal(i) + w2 * eabcpak(i)
        easlpak(i) = w1 * easlpal(i) + w2 * easlpak(i)
        eashpak(i) = w1 * eashpal(i) + w2 * eashpak(i)
      end do
    else
      !
      !         * this is in the middle of a run.
      !
      rewind nuan
      !
      call getggbx(ebocpal,nc4to8("EBOC"),nuan,nlon,nlat,ib2n,1,gg)
      call getggbx(ebbcpal,nc4to8("EBBC"),nuan,nlon,nlat,ib2n,1,gg)
      call getggbx(eaocpal,nc4to8("EAOC"),nuan,nlon,nlat,ib2n,1,gg)
      call getggbx(eabcpal,nc4to8("EABC"),nuan,nlon,nlat,ib2n,1,gg)
      call getggbx(easlpal,nc4to8("EASL"),nuan,nlon,nlat,ib2n,1,gg)
      call getggbx(eashpal,nc4to8("EASH"),nuan,nlon,nlat,ib2n,1,gg)
    end if
  end if

  return
end subroutine getch
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

