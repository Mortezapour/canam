!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine getnae(nf,escvpak,ehcvpak,esevpak,ehevpak, &
                  ijpak,nlon,nlat,gg)
  !
  !     * knut von salzen - feb 07,2009. new routine for gcm15h to read in
  !     *                                volcanic fields: escv,ehcv,esev,
  !     *                                ehev. (used to be done before in
  !     *                                getchem2).
  !
  implicit none
  integer, intent(in) :: ijpak
  integer, intent(in) :: nf
  integer, intent(in) :: nlat
  integer, intent(in) :: nlon
  integer, external :: nc4to8
  !
  !     * invariant fields:
  !
  real, intent(out), dimension(ijpak) :: escvpak !< Variable description\f$[units]\f$
  real, intent(out), dimension(ijpak) :: ehcvpak !< Variable description\f$[units]\f$
  real, intent(out), dimension(ijpak) :: esevpak !< Variable description\f$[units]\f$
  real, intent(out), dimension(ijpak) :: ehevpak !< Variable description\f$[units]\f$
  !
  !     * work field:
  !
  real, intent(in), dimension(*) :: gg !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================


  !-----------------------------------------------------------------------
  !     * grids read from file nf.

  rewind nf
  !
  call getggbx(escvpak,nc4to8("ESCV"),nf,nlon,nlat,0,1,gg)
  call getggbx(ehcvpak,nc4to8("EHCV"),nf,nlon,nlat,0,1,gg)
  call getggbx(esevpak,nc4to8("ESEV"),nf,nlon,nlat,0,1,gg)
  call getggbx(ehevpak,nc4to8("EHEV"),nf,nlon,nlat,0,1,gg)
  !
  return
end subroutine getnae
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
