!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine getozon2(ozpak,ozpal,name, &
                    lo,nlon,nlat,levoz,incd,iday,mday, &
                    kount,ijpak,nuan,gg)
  !
  !     * jul 31/09 - m.lazare. new routine for gcm15i.
  !     *                       - gets ozone at "MDAY".
  !     *                       - this works for any ozone dataset.
  !     *                       - based on getozon (only difference.
  !     *                         is name and level index passed in.
  !     * jul 29/07 - m.lazare. previous version getoxon for gcm15h.
  !
  !     * read in chemistry ozone fields and interpolate to current iday.
  !
  !     * time label is the julian date of the first of the month
  !     * (nfdm) for which the sst are the monthly average.
  !
  !     * when the model is moving through the year (incd/=0),
  !     * then new fields are read on every mid month day (mmd),
  !     * and interpolation will be done between the two adjacent
  !     * mid month days. "PAK" is the precedent, "PAL" is the target.
  !
  implicit none
  integer, intent(in) :: iday  !< Julian calendar day of year \f$[day]\f$
  integer, intent(in) :: ijpak
  integer, intent(in) :: incd
  integer, intent(in) :: kount  !< Current model timestep \f$[unitless]\f$
  integer, intent(in) :: levoz  !< Number of vertical layers for ozone input data (other than chemistry) \f$[unitless]\f$
  integer, intent(in) :: mday  !< Julian calendar day of next mid-month \f$[day]\f$
  integer, intent(in) :: name
  integer, intent(in) :: nlat
  integer, intent(in) :: nlon
  integer, intent(in) :: nuan
  !
  real, intent(inout), dimension(ijpak,levoz) :: ozpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,levoz) :: ozpal !< Variable description\f$[units]\f$
  !
  real, intent(in) :: gg( * ) !< Variable description\f$[units]\f$
  !
  integer, intent(in), dimension(levoz) :: lo !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  real :: day1
  real :: day2
  real :: day3
  integer :: i
  integer :: jour
  integer :: jour1
  integer :: l
  integer :: lon
  integer :: m
  integer :: mday1
  integer :: n
  integer, external :: nc4to8
  real :: w1
  real :: w2

  integer, dimension(12), parameter :: mmd = [16, 46, 75,106,136,167,197,228,259,289,320,350]
  integer*4 :: mynode
  !
  !     * internal work array.
  !
  integer, dimension(levoz) :: lh
  !
  common /mpinfo/ mynode
  !
  !----------------------------------------------------------------------
  !
  !     * first, establish whether the radiation ozone ("OZ") or chemical
  !     * ozone ("03C") is being used. the values of ibuf(4) are different
  !     * for these, because for chemistry it is the original raw data
  !     * which has the actual pressure level index, while the radiation
  !     * ozone goes through the usual interpolation routine which sets
  !     * ibuf(4)=l.
  !
  if (name == nc4to8("  OZ")) then
    do l = 1,levoz
      lh(l) = l
    end do
  else if (name == nc4to8(" O3C")) then
    do l = 1,levoz
      lh(l) = lo(l)
    end do
  else
    call xit('GETOZON2', - 1)
  end if
  !
  m = 0
  do n = 1,12
    if (mday == mmd(n)) m = n
  end do ! loop 5
  jour = mday
  !
  !     * now, get previous mid-month target for initialization at kount=0.
  !
  l = m - 1
  if (l == 0) l = 12
  mday1 = mmd(l)
  jour1 = mmd(l)
  !
  if (incd == 0) then
    !
    !       * model is stationary.
    !
    if (kount == 0) then
      !
      !         * start-up time. read-in fields for average of month.
      !         * initialize target fields as well, although not used.
      !
      rewind nuan
      do  l = 1,levoz
        call getggbx(ozpak(1,l),name,nuan,nlon,nlat,mday, &
                     lh(l),gg)
      end do ! loop 10
      !
      do l = 1,levoz
        do i = 1,ijpak
          ozpal  (i,l) = ozpak  (i,l)
        end do
      end do ! loop 16

    else
      !
      !         * not a new integration. no new fields required.
      !
    end if

  else
    !
    !       * the model is moving.
    !
    if (kount == 0) then
      !
      !        * start-up time. get fields for previous and target mid-month days.
      !
      rewind nuan
      do  l = 1,levoz
        call getggbx(ozpak(1,l),name,nuan,nlon,nlat,jour1, &
                     lh(l),gg)
      end do ! loop 22
      !
      rewind nuan
      do  l = 1,levoz
        call getggbx(ozpal(1,l),name,nuan,nlon,nlat,jour, &
                     lh(l),gg)
      end do ! loop 32
      !
      lon = nlon - 1
      day1 = real(mday1)
      day2 = real(iday)
      day3 = real(mday)
      if (day2 < day1) day2 = day2 + 365.
      if (day3 < day2) day3 = day3 + 365.
      w1 = (day2 - day1)/(day3 - day1)
      w2 = (day3 - day2)/(day3 - day1)
      if (mynode == 0) write(6,6000) iday,mday1,mday,w1,w2
      !
      do l = 1,levoz
        do i = 1,ijpak
          ozpak  (i,l) = w1 * ozpal  (i,l) + w2 * ozpak  (i,l)
        end do
      end do ! loop 150

    else
      !
      !         * this is in the middle of a run.
      !
      rewind nuan
      !
      do  l = 1,levoz
        call getggbx(ozpal(1,l),name,nuan,nlon,nlat,jour, &
                     lh(l),gg)
      end do ! loop 232
    end if

  end if
  return
  !---------------------------------------------------------------------
6000 format(' INTERPOLATING OZONE FOR', i5, ' BETWEEN', i5, ' AND', &
        i5, ' WITH WEIGHTS = ', 2f7.3)
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}


