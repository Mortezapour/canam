!> \file io_labels.F90
!> \brief Defines and set label values to use in reading/writing I/O files
!!
!! @author Routine author name(s)
!
 module io_labels
  use phys_cosp_itf, only : llidar, lradar

  implicit none

  ! * General spectral index information
  integer, allocatable, dimension(:, :) :: lsr
  integer, allocatable, dimension(:, :) :: lsrtotal
  integer, allocatable, dimension(:)    :: mindex
  !
  integer, allocatable, dimension(:) :: lh !<
  integer, allocatable, dimension(:) :: ls !<
   !
   !     * equivilent "LS","LH" arrays for new saved gwd fields.
   !
  integer, allocatable, dimension(:) :: lsb !<
  integer, allocatable, dimension(:) :: lhb !<

  !
  !     * level information for class, oxidants and ozone.
  !
  integer, allocatable, dimension(:) :: lc !< Label for canopy levels
  integer, allocatable, dimension(:) :: lg !< Label for soil levels
  integer, allocatable, dimension(:) :: lo !< Label values for input ozone
  integer, allocatable, dimension(:) :: lochem !< Level information for randel chemistry ozone
  integer, allocatable, dimension(:) :: lx  !< Label values for input oxidants
  integer, allocatable, dimension(:) :: lct !<
  integer, allocatable, dimension(:) :: lgt !<
  integer, allocatable, dimension(:) :: lctem  !<
  integer, allocatable, dimension(:) :: lctg   !<
  integer, allocatable, dimension(:) :: lctemg !<
  integer, allocatable, dimension(:) :: lsw  !<
  integer, allocatable, dimension(:) :: llw  !<
  integer, allocatable, dimension(:) :: lswt !<
  integer, allocatable, dimension(:) :: llwt !<
  integer, allocatable, dimension(:) :: lf   !< Level information for input emission from wild fire
  integer, allocatable, dimension(:) :: lair !< Level information for input emissions from aircraft
  integer, allocatable, dimension(:) :: lsa  !< Level information for stratospheric aerosols
  integer, allocatable, dimension(:) :: lace !< Level information for aircraft emissions for gas-phase chemistry
  integer, allocatable, dimension(:) :: lsad !< Level information for stratospheric aerosol surface area

  integer, allocatable, dimension(:) :: lrp  !< Level information for radiative forcing diagnostic flux arrays

  contains

  subroutine set_labels(sg, sh, sgb, shb, ptoit)
   use psizes_19,         only : ilev, ican, ignd, ntld, ictem, im, nbl, nbs, &
                                 levair, levox, levoz, levozc, levrf, levsa,  &
                                 levwf, ioztyp, ioxtyp, la, lm, lmtotal
   use agcm_types_mod,    only : phys_options
   use agcm_types_mod,    only : phys_diag
   use em_scenarios_defs, only : ivtau
   use chem_params_mod,   only : levssad, nlvarc, zed
   use cosp_defs,         only : use_vgrid

   implicit none

   real, intent(in) :: ptoit
   real, dimension(ilev), intent(in) :: sg
   real, dimension(ilev), intent(in) :: sgb
   real, dimension(ilev), intent(in) :: sh
   real, dimension(ilev), intent(in) :: shb

   integer :: ii
   integer :: kk
   integer :: ioztyp4

   !
   ! * calculate general spectral index information.
   !
   allocate(lsr(2, lm + 1), lsrtotal(2, lmtotal + 1), mindex(lm))
   call dimgt2(lsrtotal, lmtotal, lsr, la, lm, mindex)

   !
   allocate(ls(ilev), lh(ilev), lsb(ilev), lhb(ilev))
   call siglab2 (ls, lh, sg, sh, ilev)
   call siglab2 (lsb, lhb, sgb, shb, ilev)

   if (phys_diag%switch_cosp_diag) then
      ! If use_vgrid=.true. then grid defined in cosp_sim_driver
      ! If not, need to put "flipped" levels in lh into llidar and lradar
      if (.not. use_vgrid) then
         do ii = 1, ilev
            llidar(ii) = lh(ilev-ii+1)
            lradar(ii) = lh(ilev-ii+1)
         end do
      end if
   end if

  !     * define number of canopy (c) and soil (g) "LEVELS" and assign
  !     * integer :: label values to aid in reading/writing restart file.
  !     * extra canopy level is for "URBAN" special class - the
  !     * lc array is extended to include this.

    allocate(lc(ican+1), lg(ignd), lct(ntld*(ican + 1)), lgt(ntld*ignd), &
             lctem(ntld*(ictem + 1)), lctg(ntld*ignd*ican), lctemg(ntld*ignd*ictem))
    call hydlabt (lc, lg, lct, lgt, lctem, lctg, lctemg, &
                  ican, ican+1, ignd, ictem, ictem+1, ntld)

    allocate(lsw(nbs), llw(nbl), lswt(im*nbs), llwt(im*nbl))
    call radlabt (lsw, llw, lswt, llwt, nbs, nbl, im)

  !
  !     * define integer :: label values for input ozone to aid in reading/
  !     * writing restart file.
  !
    allocate(lo(levoz))
    call ozilab2(lo, levoz, ioztyp)

  !  * now do same for oxidants.
  !  * if oxidants are cmip6 ones on model thermodynamic levels, set lx=lh and
  !  * skip ensuing routine; otherwise call it to set lx to input data
  !  * set pressure levels.
  !    write(6,*)'ioxtyp = ',ioxtyp
    allocate(lx(levox))
    if (ioxtyp == 0) then
       do kk = 1, levox
          lx(kk) = lh(kk)
       end do
    else
       call oxilab(lx, levox, ioxtyp)
    end if
  !
  ! * Define lochem for randel chemistry ozone regardless of whether it is
  ! * being used in physics or not (ie regardless of choice for "IOZCHEM" in
  ! * physics). This is ioztyp=4 and has "LEVOZC" levels irrespective of choice
  ! * for "LEVOZ" which applies to radiative ozone.
  !
    ioztyp4=4
    allocate(lochem(levozc))
    call ozilab2(lochem, levozc, ioztyp4)
  !
  !     * define integer :: label values for stratospheric aerosol to
  !     * aid in reading/writing restart file.
  !
    allocate(lsa(levsa))
    call strat_aerosol_lab(lsa, levsa, ivtau)
  !
  ! * Define integer :: label values for input emission from wild fire to
  ! * aid in reading/writing restart file.
    allocate(lf(levwf))
    call wildflab(lf, levwf)
  !
  ! * Define integer :: label values for input emission from aircraft.
    allocate(lair(levair))
    call airlab(lair, levair)
  !
    if (phys_options%gas_chem) then
    ! * Define level information for specified stratospheric aerosol SAD.
       allocate(lsad(levssad))
       do kk = 1, levssad
          lsad(kk) = kk
       end do

    ! * Define level information for aircraft emissions.
       allocate(lace(nlvarc))
       do kk = 1, nlvarc
          lace(kk) = int(zed(kk))
       end do
    !
    endif

    if (phys_diag%rad_flux_profs .or. phys_diag%radforce) then

      ! * Call routine to get level values for saving radiative forcing
      ! * diagnostic flux arrays.
      !
      allocate(lrp(levrf))
      call radflab(lrp, levrf, ptoit, lh, ilev)

    end if

    return
   end subroutine set_labels

 end module io_labels
!> \file
