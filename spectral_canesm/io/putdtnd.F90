!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine putdtnd(nf, ptd, ctd, ttd, estd, &
                   kount, la, lrlmt, ilev, levs, ls, lh, latotal)

  !     * feb 29/2012 - m.lazare.
  !
  !     * saves global spectral dynamics tendencies on sequential file nf.
  !     * all fields are written unpacked.
  !     * ls,lh = output label values for full,half levels.
  !
  implicit none
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: kount  !< Current model timestep \f$[unitless]\f$
  integer, intent(in) :: la
  integer, intent(in) :: latotal
  integer, intent(in) :: levs  !< Number of moisture levels in the vertical \f$[unitless]\f$
  integer, intent(in) :: lrlmt
  integer, intent(in) :: nf

  complex, intent(inout) :: ttd(la,ilev)          !< Variable description\f$[units]\f$
  complex, intent(inout) :: ptd(la,ilev)          !< Variable description\f$[units]\f$
  complex, intent(inout) :: ctd(la,ilev)          !< Variable description\f$[units]\f$
  complex, intent(inout) :: estd(la,levs)         !< Variable description\f$[units]\f$

  integer, intent(in), dimension(ilev) :: ls !< Variable description\f$[units]\f$
  integer, intent(in), dimension(ilev) :: lh !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  integer :: ibuf2
  integer :: idiv
  integer :: ies
  integer :: ipio
  integer :: isavdts
  integer :: itmp
  integer :: ivort
  integer :: iyear
  integer :: iymdh
  integer :: l
  integer :: myrssti
  integer :: n
  integer, external :: nc4to8
  integer, dimension(8) :: ibuf

  common /ipario/  ipio
  common /keeptim/ iyear,iymdh,myrssti,isavdts
  !--------------------------------------------------------------------
  !
  itmp = nc4to8(" TTD")
  ivort = nc4to8(" PTD")
  idiv = nc4to8(" CTD")
  ies = nc4to8("ESTD")

  !     * determine proper ibuf(2) to use for saved fields, based on
  !     * value of option switch "ISAVDTS".
  !
  if (isavdts /= 0) then
    ibuf2 = iymdh
  else
    ibuf2 = kount
  end if
  !
  call setlab(ibuf,nc4to8("SPEC"),ibuf2, - 1,1, - 1,1,lrlmt,0)
  !
  !     * save temperature tendencies for ilev levels.
  !
  ibuf(3) = itmp
  do l = 1,ilev
    ibuf(4) = lh(l)
    call putspn (nf, ttd(1,l), la,  latotal, ibuf, ipio)
  end do ! loop 310
  !
  !     * save vorticity and divergence tendencies in pairs for each level.
  !
  do l = 1,ilev
    ibuf(4) = ls(l)

    ibuf(3) = ivort
    call putspn (nf, ptd(1,l), la, latotal, ibuf, ipio)

    ibuf(3) = idiv
    call putspn (nf, ctd(1,l), la, latotal, ibuf, ipio)
  end do ! loop 410
  !
  !     * moisture variable tendencies saved for levs levels.
  !
  if (levs == 0) return
  ibuf(3) = ies
  do n = 1,levs
    l = (ilev - levs) + n
    ibuf(4) = lh(l)
    call putspn (nf, estd(1,n), la, latotal, ibuf, ipio)
  end do ! loop 510
  !
  return
  !-----------------------------------------------------------------------
end subroutine putdtnd
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
