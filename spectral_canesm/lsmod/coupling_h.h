!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

!     * nov 14/2003 - d.robitaille

!     define some variable types for mpi communication

  integer*4 :: coupler !<
  integer*4 :: atmos !<
  integer*4 :: ocean !<
  character*8 :: member(0:1000) !<

  common /mpimodules/ coupler, atmos, ocean, member

  integer*4 :: status(mpi_status_size) !<
  integer*4 :: ierr !<
  !
  !     * common block to hold the number of nodes and communicator.
  !
  integer*4 :: nnode !<
  integer*4 :: agcm_commwrld !<
  integer*4 :: my_cmplx_type !<
  integer*4 :: my_real_type !<
  integer*4 :: my_int_type !<
  common /mpicomm/ nnode, agcm_commwrld, my_cmplx_type,my_real_type, &
                  my_int_type

!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
