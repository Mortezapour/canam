!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine name2(nam,ic,integ)

  !     * apr 17/15 - m.lazare/   new version for gcm18+:
  !     *             j.cole/     - rename "INT" as "INTEG"
  !     *             f.majaess.    to avoid conflict with intrinsic
  !     *                           of same name. this is a purely
  !     *                           cosmetic modification so the same
  !     *                           name is kept.
  !     * aug 25/06 - f.majaess - new version for gcm15f:
  !     *                         make it work in 32-bits mode as well.
  !     * nov 04/97 - m.holzer,r.harvey. previous version name.
  !     *                                like "NOM" except writes out in
  !     *                                format "XM01".
  !     * april 26/90 - j. de grandpre (u.q.a.m.)
  !
  !     * subroutine qui definie le nom des champs associe aux differents
  !     * traceurs en fonction du nombre de traceurs.

  implicit none
  integer, intent(in)  :: integ
  integer, intent(out) :: nam
  character(len=1), intent(in) :: ic(2) !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  integer :: n1
  integer :: n2
  integer :: nc4to8

  character(len=1) :: ic_wrk(2)
  character(len=4) :: cnam !<

  !----------------------------------------------------------------------
  n2=integ/10
  n1=integ-n2*10
  ic_wrk = ic
  if (integ>99) call xit('NAME2',-1)
  if (ic_wrk(2)==' ') ic_wrk(2)='0'
  write(cnam,1000) ic_wrk,n2,n1
  nam=nc4to8(cnam)

  1000 format(2a1,2i1)
  return
end subroutine name2
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
