!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine tracd2 (trac,la,ilev,s,grav,kount,iepr,itrac,tracol)
  !
  !     * sep 03/88 - m.lazare.  tracol is now work array instead of
  !     *                        hard-coated dimension.
  !     * mar 03/85 - r.laprise. original routine tracdia.
  !
  !     * compute and print layer and global averages of tracer
  !     * when itrac/=0.
  !
  implicit none
  real, intent(inout) :: grav  !< Gravity on Earth \f$[m s^{-2}]\f$
  integer, intent(inout) :: iepr
  integer, intent(inout) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(inout) :: itrac  !< Switch to indicate use of tracers in CanAM (0 = no, 1 = yes) \f$[unitless]\f$
  integer, intent(inout) :: kount  !< Current model timestep \f$[unitless]\f$
  integer :: l
  integer, intent(inout) :: la
  real :: tractot

  complex, intent(in) :: trac(la,ilev) !< Variable description\f$[units]\f$
  !
  real, intent(inout), dimension(ilev) :: tracol !< Variable description\f$[units]\f$
  real, intent(in), dimension(1) :: s !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !-------------------------------------------------------------------------
  if (itrac==0 .or. iepr==0)return
  if (mod(kount,iepr)/=0)     return
  !
  tractot=0.
  do l=1,ilev
    tracol(l)=real(trac(1,l))/(sqrt(2.)*grav)
    tractot=tractot+tracol(l)*(s(l+1)-s(l))
  end do ! loop 200
  !
  write(6,6000)tractot,(l,tracol(l),l=1,ilev)
  return
  !-------------------------------------------------------------------------
  6000 format('0TOTAL TRACER=',7x,e15.7,/,(i6,2x,e15.7))
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
