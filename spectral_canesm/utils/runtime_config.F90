!> Handle the runtime configurations and dimension sizes for the AGCM
module agcm_config_mod

 implicit none; private

 type, public :: agcm_options_type

   logical :: agcm_river_routing
   logical :: biogeochem
   logical :: cf_sites
   logical :: dynrhs
   logical :: isavdts                ! isavdts defines using "YYYYMMDDHH" date-time stamp
   logical :: myrssti                ! MYRSSTI DEFINES RUNNING WITH MULTI-YEAR SST'S AND SEA-ICE.
   logical :: parallel_io
   logical :: relax
   logical :: timers
   logical :: use_canam_32bit
   logical :: rcm

   contains

   procedure, public :: initialize => initialize_agcm_options
 end type agcm_options_type

!> Contains various dimensions associated with arrays in the AGCM. Default values should NOT be set here
 type, public :: agcm_dims_type

  ! Defined in psizes_19 and msizes modules
   integer :: ilev
   integer :: ilg
   integer :: ilgd
   integer :: levs
   integer :: lmtotal
   integer :: lonp
   integer :: lond
   integer :: lonsl
   integer :: lonsld
   integer :: nlat
   integer :: nlatd
   integer :: nnodex

   contains

   procedure, public :: initialize => initialize_agcm_dims
 end type agcm_dims_type

  !> Contains runtime options and various dimensions used throughout the physics
 type, public :: agcm_config_type
   type(agcm_options_type), pointer :: options !< Runtime configurable options
   type(agcm_dims_type)   , pointer :: dims    !< Dimensions of arrays in the physics

   contains

   procedure, public :: initialize => initialize_config
 end type agcm_config_type

! Module variables
 type(agcm_dims_type),    public, target, save :: agcm_dims
 type(agcm_options_type), public, target, save :: agcm_options

 contains

 !> Read in dimensions associated with all arrays in phys_arrays.
 subroutine initialize_agcm_dims(self, nml_unit, mynode)
   use psizes_19
   implicit none

   class(agcm_dims_type), intent(inout) :: self     !< phys_arrays_dims type to be initialized
   integer,               intent(in)    :: nml_unit !< File unit of an already opened namelist
   integer,               intent(in)    :: mynode   !< MPI CPU tile

   ! Defined in a namelist, set default values here
   ilev          = 49
   ilg           = 131
   ilgd          = 195
   levs          = 49
   lmtotal       = 64
   lonp          = 128
   lond          = 192
   lonsl         = 128
   lonsld        = 192
   nlat          = 64
   nlatd         = 96
   nnodex        = 32

   namelist /agcm_sizes/ ilev, ilg, ilgd, levs, lmtotal, lonp, lond, &
                         lonsl, lonsld, nlat, nlatd, nnodex

   rewind(nml_unit)
   read(nml_unit, agcm_sizes)

   if (mynode == 0) then
      write(6, *) "Final AGCM size settings:"
      write(6, nml = agcm_sizes)
   end if
   !
   !     * physics tile gathered array dimension
   !
   ilgl = ilg * ntld
   ilgk = ilg * ntlk
   ilgw = ilg * ntwt
   ilgm = max(ilgl,ilgk,ilgw)

   ilglc = ilgl*ican !<
   ilgim = ilg*im !<

   !     * number of model levels
   ilevp1 = ilev + 1
   ilevp2 = ilev + 2

   !     * spectral model grid sizes.
   ilgsl = lonsl + 2
 !  lonsz = 1
   ilgsld = lonsld + 2
   ilat = nlat/nnodex
   ilatd = nlatd/nnodex
   nlatj = max(lonp/lonsl,1)
   nlatjd = max(lond/lonsld,1)
   nlatjm = max(nlatj,nlatjd)
   ilgz = lonsz + 1
   ilg_tp = ilgsl * nlatj + 1
   ilg_td = ilgsld * nlatjd + 1
   idlm = max(ilg * ilev,ilgd * ilev,ilg_tp * ilev)
   ip0j = lonsl * ilat + 1
   ip0jz = lonsz * ilat + 1
   dp0j = lonsld * ilatd + 1
   lon_tp = lonsl * nlatj
   lon_td = lonsld * nlatjd
   ntask_p = lonsl * ilat/lonp
   ntask_d = lonsld * ilatd/lond
   ntask_m = max(ntask_p,ntask_d)
   ntask_tp = ilat/nlatj
   ntask_td = ilatd/nlatjd
   ilh = ilgsl/2
   ilhd = ilgsld/2
   latotal = (lmtotal + 1) * lmtotal/2
   iram = latotal + lmtotal
   lm = lmtotal/nnodex
   lmp1 = lm + 1
   la = latotal/nnodex
   rl = la * ilev
   rs = la * levs
   ip0f = max(max(ilgsld * nlatd,2 * latotal),20000) + 8

   !     * size of gll work array
   ngll = max(ilgsld * nlatd,((ilev + 2) ** 2) * nlatd)

 end subroutine initialize_agcm_dims

!> Set runtime configurable parameters affecting phys_arrays. Currently these are being set via CPP keys, but
!! there is hope that they may actually be set from a namelist
 subroutine initialize_agcm_options(self, nml_unit, mynode)
   implicit none

   class(agcm_options_type), intent(inout) :: self !< Contains all configurable parameters in the spectral AGCM
   integer, optional,        intent(in   ) :: nml_unit !< Handle to the namelist file
   integer, optional,        intent(in   ) :: mynode   !< MPI CPU tile

   logical :: agcm_river_routing = .false.
   logical :: biogeochem = .false.
   logical :: cf_sites = .false.
   logical :: dynrhs = .false.
   logical :: isavdts = .false.
   logical :: myrssti = .false.
   logical :: parallel_io = .false.
   logical :: relax = .false.
   logical :: timers = .false.
   logical :: use_canam_32bit = .false.
   logical :: rcm = .false.

   namelist / agcm_options / &
     agcm_river_routing, biogeochem, cf_sites, dynrhs, isavdts, myrssti, &
     parallel_io, relax, timers, use_canam_32bit, rcm

   rewind(nml_unit)
   read(nml_unit, nml=agcm_options)

   if (mynode == 0) then
      write(6, *) "Final model's AGCM options:"
      write(6, nml = agcm_options)
   end if

   self%agcm_river_routing = agcm_river_routing
   self%biogeochem = biogeochem
   self%cf_sites = cf_sites
   self%dynrhs = dynrhs
   self%isavdts = isavdts
   self%myrssti = myrssti
   self%parallel_io = parallel_io
   self%relax = relax
   self%timers = timers
   self%use_canam_32bit = use_canam_32bit
   self%rcm = rcm

 end subroutine initialize_agcm_options

 subroutine initialize_config(self, nml_unit, mynode)
   class(agcm_config_type), intent(inout) :: self
   integer,                 intent(in)    :: nml_unit !< Handle to the opened namelist
   integer,                 intent(in   ) :: mynode   !< MPI CPU tile

   self%options => agcm_options
   call self%options%initialize(nml_unit, mynode)
   self%dims => agcm_dims
   call self%dims%initialize(nml_unit, mynode)

 end subroutine initialize_config

end module agcm_config_mod
